import socialnetwork.config.ApplicationContext;
import socialnetwork.domain.Event;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.*;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.*;
import socialnetwork.service.*;

import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import java.lang.annotation.Documented;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


import java.io.FileOutputStream;

public class TestingFeatures {
    public static void main(String[] args) {

        final String file_password = ApplicationContext.getPROPERTIES().getProperty("datafile.password");

        final String url = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url");
        final String username= ApplicationContext.getPROPERTIES().getProperty("databse.socialnetwork.username");
        final String password= ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.pasword");

      UserDBRepo userDBrepo = new UserDBRepo(url,username, password,   new UserValidator());

        FriendshipDBRepo friendshipDBRepo = new FriendshipDBRepo(url,username,password,new FriendshipValidator());

        MessageDBRepo messageDBRepo = new MessageDBRepo(url,username,password,new MessageValidator());

        FriendRequestsDBRepo requestsDBRepo = new FriendRequestsDBRepo(url,username,password,new FriendRequestValidator());

        EventsDBRepo eventsDBRepo = new EventsDBRepo(url,username,password,new EventValidator());

        EventsService eventsService = new EventsService(eventsDBRepo,userDBrepo);

        ImageProfileDBRepo imageProfileDBRepo = new ImageProfileDBRepo(url,username,password);

        AdminService adminService = new AdminService(userDBrepo,friendshipDBRepo,messageDBRepo,requestsDBRepo);

        UserService userService = new UserService(userDBrepo,friendshipDBRepo,messageDBRepo,requestsDBRepo,imageProfileDBRepo);

        FriendShipService friendShipService = new FriendShipService(userDBrepo,friendshipDBRepo,messageDBRepo,requestsDBRepo);

        MessageService messageService = new MessageService(userDBrepo,friendshipDBRepo,messageDBRepo,requestsDBRepo);

        LocalDateTime localDateTime = LocalDateTime.of(2021,11,9,0,0);
        String event_name = "La filme";

        Event event = new Event(0l,new User((long) 25),localDateTime,event_name,123120l,true);

         eventsService.saveEvent(event);

      /*  eventsService.updateEvent(event);


        Event event1 = eventsDBRepo.findOne(new Event(1l));

        System.out.println(event1.toString());

        System.out.println("PAGES= " +eventsService.getNrOfPagesEvents(""));

        eventsService.getAllEventsPageable(2,"").forEach(System.out::println);*/

/*
        LocalDateTime localDateTime_start = LocalDateTime.of(2020,12,9,0,0);
        LocalDateTime localDateTime_end = LocalDateTime.of(2020,12,9,15,59);

        User user_test = new User(25l);

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
      //  messageService.getReceiveMessagesForAnUserFromAPeriod(user_test,localDateTime_start,localDateTime_end).forEach(f-> System.out.println(dateTimeFormatter.format(f.getDateTime())));


        User user_test1 = new User(24l);

        LocalDate localDate_s = LocalDate.of(2020,1,1);
        LocalDate localDate_f = LocalDate.of(2020,12,3);


        friendShipService.getFriendsFromAPeriod(user_test1,localDate_s,localDate_f).forEach(f-> System.out.println(f.getLocalDate() + " " + f.getEmail()));


        System.out.println("---------------------");
        System.out.println("---------------------");

        System.out.println(friendShipService.getResultForChartDateFriends(user_test1,localDate_s,localDate_f));
*/





    }
}
