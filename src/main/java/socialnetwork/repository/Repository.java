package socialnetwork.repository;

import socialnetwork.domain.Entity;
import socialnetwork.exceptions.UserException;

/**
 * CRUD operations repository interface
 * @param <ID> - type E must have an attribute of type ID
 * @param <E> -  type of entities saved in repository
 */

public interface Repository<ID, E extends Entity<ID>> {

    /**
     *
     * @param entity
     * @return the entity if that entity exist in repository
     *         null - otherwise
     */
    E findOne(E entity);

    /**
     *
     * @return all entities
     */
    Iterable<E> findAll();

    /**
     *
     * @param entity
     *         entity must be not null
     * @return null- if the given entity is saved
     *         otherwise returns the entity (id already exists)
     * @throws UserException
     *            if the entity is not valid
     * @throws IllegalArgumentException
     *             if the given entity is null.     *
     */
    E save(E entity);


    /**
     *
     * @param entity
     * @return the entity which has been removed
     *         null-otherwise
     */
    E delete(E entity);

    /**
     *
     * @param entity
     *          entity must not be null
     * @return null - if the entity is updated,
     *                otherwise  returns the entity  - (e.g id does not exist).
     * @throws IllegalArgumentException
     *             if the given entity is null.
     * @throws UserException
     *             if the entity is not valid.
     */
    E update(E entity);


    int getSize();
}

