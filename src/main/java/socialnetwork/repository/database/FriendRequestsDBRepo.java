package socialnetwork.repository.database;

import socialnetwork.domain.FriendRequest;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.Validator;
import socialnetwork.exceptions.FriendRequestException;
import socialnetwork.repository.Repository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public  class FriendRequestsDBRepo implements Repository<Long, FriendRequest> {

    private String url;
    private String username;
    private String password;
    private Validator<FriendRequest> validator;


    public FriendRequestsDBRepo(String url, String username, String password, Validator<FriendRequest> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    /**
     *
     * @param entity
     * @return a entity of FriendRequest if that request exist in DB
     * return null otherwise
     * @throws  FriendRequestException if entity is null
     */
    @Override
    public FriendRequest findOne(FriendRequest entity) {

        if(entity == null)
            throw new FriendRequestException("Friend request must be not null!");

        FriendRequest friendRequest = null;

        try(Connection connection = DriverManager.getConnection(url,username,password)) {


            PreparedStatement statement = connection.prepareStatement
                    (
                            "SELECT * FROM friendRequests " +
                                    " WHERE transmitter = ? and receiver = ?"

                    );

            statement.setLong(1,entity.getIdFrom());
            statement.setLong(2,entity.getIdTo());


            ResultSet resultSet = statement.executeQuery();

            if(resultSet.next() == false )
                return null;

            Long id = resultSet.getLong(1);
            Long id1 = resultSet.getLong(2);
            Long id2 = resultSet.getLong(3);
            String status = resultSet.getString(4);
            LocalDateTime time = resultSet.getTimestamp(5).toLocalDateTime();

            friendRequest = new FriendRequest(id1,id2,status,time);
            friendRequest.setId(id);

        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }

        return friendRequest;

    }



    @Override
    public Iterable<FriendRequest> findAll() {

        Set<FriendRequest> friendRequestsSet = new HashSet<FriendRequest>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from friendrequests");
             ResultSet resultSet = statement.executeQuery()) {


            while(resultSet.next())
            {
                Long id = resultSet.getLong(1);
                Long idFrom = resultSet.getLong(2);
                Long idTo = resultSet.getLong(3);
                String status = resultSet.getString(4);
                LocalDateTime time = resultSet.getTimestamp(5).toLocalDateTime();


                FriendRequest friendRequest = new FriendRequest(idFrom,idTo,status,time);
                friendRequest.setId(id);

                friendRequestsSet.add(friendRequest);

            }


        } catch (SQLException e) {
            e.printStackTrace();
        }


        return friendRequestsSet;

    }



    @Override
    public FriendRequest save(FriendRequest entity) {

        if(entity == null)
            throw new FriendRequestException("Friend request must be not null!");

        validator.validate(entity);

        if(findOne(entity) != null)
            return entity;


        try(Connection connection = DriverManager.getConnection(url,username,password)) {


            PreparedStatement statement = connection.prepareStatement(" INSERT INTO friendrequests(transmitter,receiver,status,date) " +
                    " VALUES(?,?,?,?)");

            statement.setLong(1,entity.getIdFrom());
            statement.setLong(2,entity.getIdTo());
            statement.setString(3,entity.getStatus());
            statement.setTimestamp(4,Timestamp.valueOf(entity.getLocalDateTime()));

            statement.executeUpdate();


        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }


        return null;


    }

    /**
     *
     * @param entity
     * @return friendRequest , if there exist a friend request between a transmitter and a receiver
     *         null , if there is no friend request between transmitter and receiver
     */
    @Override
    public FriendRequest delete(FriendRequest entity)
    {
        if(entity == null)
            throw new FriendRequestException("Friend request must be not null!");

        if(findOne(entity) == null)
            return null;


        try(Connection connection = DriverManager.getConnection(url,username,password)) {


            PreparedStatement statement = connection.prepareStatement
                    (
                            "DELETE FROM friendrequests" +
                                    " WHERE transmitter = ? and receiver = ? "
                    );

            statement.setLong(1,entity.getIdFrom());
            statement.setLong(2,entity.getIdTo());


            statement.executeUpdate();


        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }

        return entity;

    }


    @Override
    public FriendRequest update(FriendRequest entity) {

        if(entity == null)
            throw new FriendRequestException("Friend request must be not null!");

        if(findOne(entity) == null)
           return entity;

        validator.validate(entity);

        try(Connection connection = DriverManager.getConnection(url,username,password)) {


            PreparedStatement statement = connection.prepareStatement
                    (
                            " update friendrequests " +
                                  " set status = ? "  +
                                     " where transmitter = ? and receiver = ?"
                    );

            statement.setString(1,entity.getStatus());
            statement.setLong(2,entity.getIdFrom());
            statement.setLong(3,entity.getIdTo());

            statement.executeUpdate();


        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }


        return null;
    }

    @Override
    public int getSize() {
        int size = 0;
        try(Connection connection = DriverManager.getConnection(url,username,password)) {

            PreparedStatement statement = connection.prepareStatement("SELECT COUNT(*) FROM friendrequests");

            ResultSet resultSet = statement.executeQuery();

            resultSet.next();

            size = resultSet.getInt(1);


        } catch (SQLException throwable) {
            throwable.printStackTrace();

        }

        return size;
    }


    public int getSizeFriendRequestReceive(User user,String full_name) {
        int size = 0;
        try(Connection connection = DriverManager.getConnection(url,username,password)) {

            PreparedStatement statement = connection.prepareStatement
                    (
                            "SELECT COUNT(*) from friendrequests"+
                                    " WHERE receiver = ? and (SELECT CONCAT(first_name,' ',last_name) from users where id = transmitter) like ?"
                    );

            statement.setLong(1,user.getId());
            statement.setString(2,"%"+full_name+"%");

            ResultSet resultSet = statement.executeQuery();

            resultSet.next();

            size = resultSet.getInt(1);

            resultSet.close();

        } catch (SQLException throwable) {
            throwable.printStackTrace();

        }

        return size;

    }

    public Iterable<FriendRequest> findFriendRequestReceivePaginate(User user,int limit , int offset,String full_name) {

        Set<FriendRequest> friendRequestsSet = new HashSet<FriendRequest>();

        try (Connection connection = DriverManager.getConnection(url, username, password)){

            PreparedStatement statement = connection.prepareStatement
                    (
                            "SELECT * from friendrequests "+
                                    " WHERE receiver = ? and (SELECT CONCAT(first_name,' ',last_name) from users where id = transmitter) like ? "+
                                    " LIMIT ? OFFSET ?"
                    );

            statement.setLong(1,user.getId());
            statement.setString(2,"%"+full_name+"%");
            statement.setInt(3,limit);
            statement.setInt(4,offset);



            ResultSet resultSet = statement.executeQuery();


            while(resultSet.next())
            {
                Long id = resultSet.getLong(1);
                Long idFrom = resultSet.getLong(2);
                Long idTo = resultSet.getLong(3);
                String status = resultSet.getString(4);
                LocalDateTime time = resultSet.getTimestamp(5).toLocalDateTime();


                FriendRequest friendRequest = new FriendRequest(idFrom,idTo,status,time);
                friendRequest.setId(id);

                friendRequestsSet.add(friendRequest);

            }


        } catch (SQLException e) {
            e.printStackTrace();
        }


        return friendRequestsSet;

    }

    public int getSizeFriendRequestSent(User user, String full_name) {
        int size = 0;
        try(Connection connection = DriverManager.getConnection(url,username,password)) {

            PreparedStatement statement = connection.prepareStatement
                    (
                            "SELECT COUNT(*) from friendrequests"+
                                    " WHERE transmitter = ? and (SELECT CONCAT(first_name,' ',last_name) from users where id = receiver ) like ?"
                    );

            statement.setLong(1,user.getId());
            statement.setString(2,"%"+full_name+"%");

            ResultSet resultSet = statement.executeQuery();

            resultSet.next();

            size = resultSet.getInt(1);

            resultSet.close();

        } catch (SQLException throwable) {
            throwable.printStackTrace();

        }

        return size;
    }

    public Object findFriendRequestSentPaginate(User user, int limit, int offset, String full_name) {

        Set<FriendRequest> friendRequestsSet = new HashSet<FriendRequest>();

        try (Connection connection = DriverManager.getConnection(url, username, password)){

            PreparedStatement statement = connection.prepareStatement
                    (
                            "SELECT * from friendrequests "+
                                    " WHERE transmitter = ? and (SELECT CONCAT(first_name,' ',last_name) from users where id = receiver) like ? "+
                                    " LIMIT ? OFFSET ?"
                    );

            statement.setLong(1,user.getId());
            statement.setString(2,"%"+full_name+"%");
            statement.setInt(3,limit);
            statement.setInt(4,offset);



            ResultSet resultSet = statement.executeQuery();


            while(resultSet.next())
            {
                Long id = resultSet.getLong(1);
                Long idFrom = resultSet.getLong(2);
                Long idTo = resultSet.getLong(3);
                String status = resultSet.getString(4);
                LocalDateTime time = resultSet.getTimestamp(5).toLocalDateTime();


                FriendRequest friendRequest = new FriendRequest(idFrom,idTo,status,time);
                friendRequest.setId(id);

                friendRequestsSet.add(friendRequest);

            }


        } catch (SQLException e) {
            e.printStackTrace();
        }


        return friendRequestsSet;
    }
}
