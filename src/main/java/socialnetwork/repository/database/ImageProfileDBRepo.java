package socialnetwork.repository.database;

import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.User;

import java.sql.*;
import java.time.LocalDate;

public class ImageProfileDBRepo {
    private String url;
    private String username;
    private String password;

    public ImageProfileDBRepo(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    public String findOne(User user)
    {
        String path = "";
        try (Connection connection = DriverManager.getConnection(url, username, password)) {

            PreparedStatement statement = connection.prepareStatement
                    (
                            "SELECT P.path from profile_images P where id_user = ?"
                    );

            statement.setLong(1,user.getId());

            ResultSet resultSet = statement.executeQuery();

            if(resultSet != null){
                if(resultSet.next())
                    path = resultSet.getString("path");
            }



        } catch (SQLException e) {
            e.printStackTrace();
        }


        return path;
    }

    public void save(User user,String path )
    {

        try (Connection connection = DriverManager.getConnection(url, username, password)) {

            PreparedStatement statement = connection.prepareStatement
                    (
                            "insert into profile_images(id_user,path) values(?,?)"
                    );

            statement.setLong(1,user.getId());
            statement.setString(2,path);

            statement.executeUpdate();


        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public void update(User user,String path )
    {

        try (Connection connection = DriverManager.getConnection(url, username, password)) {

            PreparedStatement statement = connection.prepareStatement
                    (
                            "update profile_images  set path = ? where id_user = ?"
                    );

            statement.setString(1,path);
            statement.setLong(2,user.getId());

            statement.executeUpdate();


        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}
