package socialnetwork.repository.database;

import socialnetwork.domain.Password;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;


import java.sql.*;
import java.util.*;

public class UserDBRepo implements Repository<Long, User> {
    private String url;
    private String username;
    private String password;
    private Validator<User> validator;


    public UserDBRepo(){}

    public UserDBRepo(String url, String username, String password, Validator<User> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;

    }


    public String getStringFromBytes(byte[] bytes)
    {
        String string_ = "";

        for(int  i = 0 ; i < bytes.length ; i++)
            string_+=bytes[i] +",";

        return string_;
    }


    public byte[] getBytesFromString(String string_)
    {
        List<String> lst = Arrays.asList(string_.split(","));

        byte[] bytes = new byte[lst.size()];

        for(int i = 0 ; i < lst.size() ; i++)
        {
            bytes[i] = Byte.parseByte(lst.get(i));
        }

        return bytes;

    }


    /**
     *
     * @param user_param
     * @return a founded user if an user with an email equal with user_param's email or their id's are equal
     *
     */
    @Override
    public User findOne(User user_param) {

        if (user_param.getId() == null)
            throw new IllegalArgumentException("Id must be not null");

        try(Connection connection = DriverManager.getConnection(url,username,password)) {
            PreparedStatement statement  = connection.prepareStatement
                    (
                            "SELECT * FROM users u " +
                                    " INNER JOIN passwords p on u.email = p.email_user " +
                                        "WHERE u.id = ? or u.email = ?"

                    );



            statement.setLong(1,user_param.getId());

            if(user_param.getEmail() != null)
                statement.setString(2,user_param.getEmail());
            else
                statement.setString(2, "");


            ResultSet resultSet = statement.executeQuery();


            if(!resultSet.next())
                return null;

            Long id1 = resultSet.getLong("id");
            String first_name = resultSet.getString("first_name");
            String last_name = resultSet.getString("last_name");
            String email = resultSet.getString("email");
            String hash_string = resultSet.getString("hash");
            String salt_string = resultSet.getString("salt");

            byte[] salt = getBytesFromString(salt_string);
            byte[] hash = getBytesFromString(hash_string);

            Password password_object = new Password(id1);
            password_object.setSalt(salt);
            password_object.setHash(hash);


            return new User(id1,first_name,last_name,email,password_object);


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


        return null;
    }



    public Iterable<User> getPagingUsers(int limit , int offset) {
        Set<User> users = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password)){

             PreparedStatement statement = connection.prepareStatement
                     (
                             "SELECT * FROM users u " +
                                     " INNER JOIN passwords p on u.email = p.email_user LIMIT ? OFFSET ?"

                     );

            statement.setInt(1,limit);
            statement.setInt(2,offset);
             ResultSet resultSet = statement.executeQuery();


            while (resultSet.next()) {
                Long id1 = resultSet.getLong("id");
                String first_name = resultSet.getString("first_name");
                String last_name = resultSet.getString("last_name");
                String email = resultSet.getString("email");
                Integer group = resultSet.getInt("gr");

                String hash_string = resultSet.getString("hash");
                String salt_string = resultSet.getString("salt");

                byte[] salt = getBytesFromString(salt_string);
                byte[] hash = getBytesFromString(hash_string);

                Password password_object = new Password(id1);
                password_object.setSalt(salt);
                password_object.setHash(hash);


                User user = new User(id1,first_name, last_name,email,password_object);
                user.setGroup(group);

                users.add(user);


            }

            resultSet.close();
            return users;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;

    }

    @Override
    public Iterable<User> findAll() {
        Set<User> users = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement
                     (
                             "SELECT * FROM users u " +
                                     " INNER JOIN passwords p on u.email = p.email_user  "


                     );
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id1 = resultSet.getLong("id");
                String first_name = resultSet.getString("first_name");
                String last_name = resultSet.getString("last_name");
                String email = resultSet.getString("email");
                Integer group = resultSet.getInt("gr");

                String hash_string = resultSet.getString("hash");
                String salt_string = resultSet.getString("salt");

                byte[] salt = getBytesFromString(salt_string);
                byte[] hash = getBytesFromString(hash_string);

                Password password_object = new Password(id1);
                password_object.setSalt(salt);
                password_object.setHash(hash);


                User user = new User(id1,first_name, last_name,email,password_object);
                user.setGroup(group);

                users.add(user);


            }
            resultSet.close();
            return users;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public User save(User entity) {

        if (entity==null)
            throw new IllegalArgumentException("entity must be not null");


        validator.validate(entity);

        if(findOne(entity) != null)
            return entity;

        try(Connection connection = DriverManager.getConnection(url,username,password)) {


            PreparedStatement statement = connection.prepareStatement
                    (" INSERT INTO users(first_name,last_name,email) " +
                            " VALUES(?,?,?)"

                    );

            statement.setString(1,entity.getFirstName());
            statement.setString(2,entity.getLastName());
            statement.setString(3,entity.getEmail());
            statement.executeUpdate();

            entity.getPassword().encodePassword(); // encode the password

            String hash_string = getStringFromBytes(entity.getPassword().getHash());
            String salt_string = getStringFromBytes(entity.getPassword().getSalt());

            PreparedStatement statement1 = connection.prepareStatement
                    (" INSERT INTO passwords(email_user,hash,salt) " +
                            " VALUES(?,?,?)"

                    );

            statement1.setString(1,entity.getEmail());
            statement1.setString(2,getStringFromBytes(entity.getPassword().getHash()));
            statement1.setString(3,getStringFromBytes(entity.getPassword().getSalt()));

            statement1.executeUpdate();



        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }

        return null;
    }

    @Override
    public User delete(User user_param) {

        if (user_param.getId() == null)
            throw new IllegalArgumentException("Id must be not null");

        User user = findOne(user_param);

        if(user == null)
            return null;


        try(Connection connection = DriverManager.getConnection(url,username,password)) {

            PreparedStatement statement = connection.prepareStatement("DELETE FROM users  WHERE users.id = ? or user.email = ?");

            statement.setLong(1,user_param.getId());
            statement.setString(2,user_param.getEmail());

            statement.executeUpdate();



            PreparedStatement statement1 = connection.prepareStatement("DELETE FROM passwords p WHERE p.email_user = ?");
            statement1.setString(1,user_param.getEmail());

            statement1.executeUpdate();




        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }

        return user;

    }

    @Override
    public User update(User entity) {

        validator.validate(entity);

        if (entity==null)
            throw new IllegalArgumentException("Entity must be not null");

        User user = findOne(entity);

        if(user == null)
            return entity;

        try(Connection connection = DriverManager.getConnection(url,username,password)) {

            PreparedStatement statement = connection.prepareStatement
                    (
                            "UPDATE users "
                                    +" SET first_name = ? ,last_name = ? , gr = ? "
                                    + " WHERE users.id = ? "
                    );

            statement.setString(1, entity.getFirstName());
            statement.setString(2, entity.getLastName());
            statement.setInt(3,entity.getGroup());
            statement.setLong(4,entity.getId());

            statement.executeUpdate();




        } catch (SQLException throwable) {
            throwable.printStackTrace();

        }

        return null;

    }


    public Iterable<User> getPagingUsersSearch(int limit , int offset, String full_name) {
        Set<User> users = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password)){

            PreparedStatement statement = connection.prepareStatement
                    (
                            "SELECT * FROM users u " +
                                    " INNER JOIN passwords p on u.email = p.email_user" +
                                    " WHERE (SELECT CONCAT(first_name,' ',last_name))  like ?" +
                                    " LIMIT ? OFFSET ?"

                    );

            statement.setString(1,"%"+full_name+"%");
            statement.setInt(2,limit);
            statement.setInt(3,offset);
            ResultSet resultSet = statement.executeQuery();


            while (resultSet.next()) {
                Long id1 = resultSet.getLong("id");
                String first_name = resultSet.getString("first_name");
                String last_name = resultSet.getString("last_name");
                String email = resultSet.getString("email");
                Integer group = resultSet.getInt("gr");

                String hash_string = resultSet.getString("hash");
                String salt_string = resultSet.getString("salt");

                byte[] salt = getBytesFromString(salt_string);
                byte[] hash = getBytesFromString(hash_string);

                Password password_object = new Password(id1);
                password_object.setSalt(salt);
                password_object.setHash(hash);


                User user = new User(id1,first_name, last_name,email,password_object);
                user.setGroup(group);

                users.add(user);


            }

            resultSet.close();
            return users;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;

    }


    public int getSearchSize(String full_name){
        int size = 0;
        try(Connection connection = DriverManager.getConnection(url,username,password)) {

            PreparedStatement statement = connection.prepareStatement
                    (
                            "SELECT COUNT(*) FROM users "+
                                    "WHERE CONCAT(first_name,' ',last_name)  like ?"

                    );

            statement.setString(1,"%"+full_name+"%");
            ResultSet resultSet = statement.executeQuery();

            resultSet.next();

            size = resultSet.getInt(1);

            resultSet.close();

        } catch (SQLException throwable) {
            throwable.printStackTrace();

        }

        return size;


    }


    @Override
    public int getSize() {

        int size = 0;
        try(Connection connection = DriverManager.getConnection(url,username,password)) {

            PreparedStatement statement = connection.prepareStatement("SELECT COUNT(*) FROM users");

            ResultSet resultSet = statement.executeQuery();

            resultSet.next();

            size = resultSet.getInt(1);

            resultSet.close();

        } catch (SQLException throwable) {
            throwable.printStackTrace();

        }

        return size;

    }


}
