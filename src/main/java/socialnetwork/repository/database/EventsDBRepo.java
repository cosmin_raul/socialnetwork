package socialnetwork.repository.database;

import org.w3c.dom.events.EventException;
import socialnetwork.domain.Event;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.Validator;
import socialnetwork.exceptions.EventsException;
import socialnetwork.repository.Repository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


public class EventsDBRepo   {

    private Validator<Event> eventValidator;
    private String url;
    private String username;
    private String password;

    public EventsDBRepo( String url, String username, String password,Validator<Event> eventValidator) {
        this.eventValidator = eventValidator;
        this.url = url;
        this.username = username;
        this.password = password;
    }

    public List<Event> findAllPageable(User user,int limit, int offset ,String full_name) {

        List<Event> events = new ArrayList<>();

        try (Connection connection = DriverManager.getConnection(url, username, password))
        {
            PreparedStatement statement = connection.prepareStatement
                    (
                            " SELECT * FROM events E " +
                                    " WHERE  E.event_name like ? ORDER BY E.id ASC " +
                                    " LIMIT ? OFFSET ?"
                    );

            statement.setString(1,"%"+full_name+"%");
            statement.setInt(2,limit);
            statement.setInt(3,offset);


            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()){

                Boolean subscribed = false;

                Long id = resultSet.getLong("id");
                Long user_id = resultSet.getLong("user_id");

                PreparedStatement statement1 = connection.prepareStatement
                        (
                                " SELECT COUNT(*) FROM users_events E " +
                                        " WHERE E.id_user = ? and E.id_event = ?"
                        );

                statement1.setLong(1,user.getId());
                statement1.setLong(2,id);

                ResultSet resultSet1 = statement1.executeQuery();
                if(resultSet1.next())
                {
                    if(resultSet1.getInt(1) != 0)
                        subscribed = true;

                }

                String event_name = resultSet.getString("event_name");
                LocalDateTime localDateTime = resultSet.getTimestamp("event_date").toLocalDateTime();
                Long subscribers = resultSet.getLong("subscribers");

                events.add(new Event(id,new User(user_id),localDateTime,event_name,subscribers,subscribed));

            }

            resultSet.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return events;
    }


    public int getSizePageable(String full_name) {
//(SELECT CONCAT(first_name,' ',last_name) from users where id = E.user_id)
        int size = 0;
        try (Connection connection = DriverManager.getConnection(url, username, password))
        {
            PreparedStatement statement = connection.prepareStatement
                    (

                            " SELECT COUNT(*) FROM events E " +
                                    " WHERE  E.event_name like ?"
                    );

            statement.setString(1,"%"+full_name+"%");


            ResultSet resultSet = statement.executeQuery();

            resultSet.next();

            size = resultSet.getInt(1);


            resultSet.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return  size;
    }


    public Event save(Event entity) {
        if(entity == null)
            throw new EventsException("Event is null!");

        if(findOne(entity)!=null)
            throw new EventsException("That event already exist!");


        eventValidator.validate(entity);

        try (Connection connection = DriverManager.getConnection(url, username, password))
        {
                  PreparedStatement statement = connection.prepareStatement
                          (
                                  "INSERT INTO events(user_id,event_name,event_date,subscribers) values(?,?,?,?)"
                          );

                  statement.setLong(1,entity.getUser().getId());
                  statement.setString(2,entity.getEventName());
                  statement.setTimestamp(3,Timestamp.valueOf(entity.getLocalDateTime()));
                  statement.setLong(4,entity.getSubscribers());

                  statement.executeUpdate();

            PreparedStatement statement2 = connection.prepareStatement
                    (
                            "SELECT id from events where user_id = ? and event_name = ? and event_date = ? and subscribers = ? "
                    );
            statement2.setLong(1,entity.getUser().getId());
            statement2.setString(2,entity.getEventName());
            statement2.setTimestamp(3,Timestamp.valueOf(entity.getLocalDateTime()));
            statement2.setLong(4,entity.getSubscribers());

            ResultSet resultSet = statement2.executeQuery();

            resultSet.next();

            Long id_event = resultSet.getLong(1);

            resultSet.close();
            PreparedStatement statement1 = connection.prepareStatement
                    (
                            "INSERT INTO users_events(id_user,id_event) values(?, ?)"
                    );
            statement1.setLong(1,entity.getUser().getId());
            statement1.setLong(2,id_event);

            statement1.executeUpdate();


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


        return null;
    }




    public Event findOne(Event event){


        if(event == null)
            throw new EventsException("Event is null!");


        try (Connection connection = DriverManager.getConnection(url, username, password))
        {
            PreparedStatement statement = connection.prepareStatement
                    (
                            " SELECT * FROM events E " +
                                    " WHERE E.id = ? "
                    );

            statement.setLong(1,event.getId());


            ResultSet resultSet1 = statement.executeQuery();

           if(resultSet1.next()) {

               Long id = resultSet1.getLong("id");
               Long user_id = resultSet1.getLong("user_id");
               String event_name = resultSet1.getString("event_name");
               LocalDateTime localDateTime = resultSet1.getTimestamp("event_date").toLocalDateTime();
               Long subscribers = resultSet1.getLong("subscribers");

               // resultSet1.close();

               return new Event(id, new User(user_id), localDateTime, event_name, subscribers,true);
           }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }


    public Event delete( Event new_entity ) {


       return null;
    }


    public Event update(Event new_entity) {
        eventValidator.validate(new_entity);


        try (Connection connection = DriverManager.getConnection(url, username, password))
        {
            PreparedStatement statement = connection.prepareStatement
                    (
                            "UPDATE events " +
                                    " SET event_name = ?, event_date = ?, subscribers = ?" +
                                    " WHERE id = ? "
                    );

            statement.setString(1,new_entity.getEventName());
            statement.setTimestamp(2,Timestamp.valueOf(new_entity.getLocalDateTime()));
            statement.setLong(3,new_entity.getSubscribers());
            statement.setLong(4, new_entity.getId());

            statement.executeUpdate();


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


        return null;
    }

    public void findSubscriberForEvent(Long id,Long idEvent){
        try (Connection connection = DriverManager.getConnection(url, username, password))
        {
            PreparedStatement statement = connection.prepareStatement
                    (
                            "SELECT * FROM users_events WHERE id_user = ? and id_event = ?"
                    );

            statement.setLong(1,id);
            statement.setLong(2,idEvent);

           ResultSet resultSet =  statement.executeQuery();

           if(resultSet.next())
                throw new EventsException("You already are a subscriber for this event!");


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void subscribe(Long id, Long idEvent) {

        findSubscriberForEvent(id,idEvent);

        try (Connection connection = DriverManager.getConnection(url, username, password))
        {
            PreparedStatement statement = connection.prepareStatement
                    (
                            "INSERT INTO users_events(id_user,id_event) values(?,?)"
                    );

           statement.setLong(1,id);
           statement.setLong(2,idEvent);

            statement.executeUpdate();


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


    }

    public void unSubscribe(Long id, Long idEvent) {
        try{
            findSubscriberForEvent(id,idEvent);
            throw new RuntimeException("You are not a subscriber for this event!");
        }
        catch (EventsException ignored){

        }

        try (Connection connection = DriverManager.getConnection(url, username, password))
        {
            PreparedStatement statement = connection.prepareStatement
                    (
                            "DELETE FROM users_events WHERE  id_user=? and id_event=?"
                    );

            statement.setLong(1,id);
            statement.setLong(2,idEvent);

            statement.executeUpdate();


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }



    }
}
