package socialnetwork.repository.database;

import socialnetwork.domain.Message;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class MessageDBRepo implements Repository<Long,Message> {

    private String url;
    private String username;
    private String password;
    private Validator<Message> validator;

    public MessageDBRepo(String url, String username, String password, Validator validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }




    /**
     *
     * @param msg
     * @param replier
     * @param msgReply
     * behavior : function stores in variable Message msg a reply which comes from user replier whit message msgReply at
     * message send by user from , who is store in variable msg
     */
    public void replyMessage( Message msg,User replier, String msgReply) {

        if(msg == null || replier == null)
            throw new IllegalArgumentException("Msg and replier must be not null");

        List<Long> from = new ArrayList<Long>();
        from.add(msg.getIdFrom());

        Message reply = new Message(replier.getId(),from,msgReply,LocalDateTime.now());

        sendMessage(reply);
        reply.setStatus(true);

        try (Connection connection = DriverManager.getConnection(url, username, password)){
             PreparedStatement statement = connection.prepareStatement
                     (
                             " update receivers " +
                                     " set reply = true " +
                                     " where idmessage = ? and idreceiver = ? "
                     );

             statement.setLong(1,msg.getId());
             statement.setLong(2,replier.getId());

             statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        msg.setReply(reply);

    }



    @Override
    public Message findOne(Message message) {
        if (message.getId() == null)
            throw new IllegalArgumentException("Id must be not null");

        Message msg = null;

        try (Connection connection = DriverManager.getConnection(url, username, password)) {

            PreparedStatement statement = connection.prepareStatement
                    (
                            " select * from messages M" +
                                " inner join receivers R on M.id = R.idmessage "+
                                    " where M.id = ? "

                    );

            statement.setLong(1,message.getId());

            ResultSet resultSet = statement.executeQuery();

            List<Long> lstReceiver = new ArrayList<>();

            Long idMessage = null;
            Long transmitter = null;
            Long receiver = null;
            String strMsg = null;
            LocalDateTime date = null;
            if(resultSet.next())
            {
                 idMessage = resultSet.getLong("id");
                 transmitter = resultSet.getLong("transmitter");
                 strMsg = resultSet.getString("message");
                 date = resultSet.getTimestamp("date").toLocalDateTime();
                 receiver = resultSet.getLong("idreceiver");
                lstReceiver.add(receiver);
            }



            while(resultSet.next())
            {
                receiver = resultSet.getLong("idreceiver");
                lstReceiver.add(receiver);
            }


            if(transmitter == null && receiver == null)
                return null;

            msg = new Message(transmitter,lstReceiver,strMsg,date);
            msg.setId(idMessage);



        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return msg;

    }


    /**
     *
     * @param user
     * @return a iterable list of messages ,this messages are all messages which has been sent by user given as argument
     *
     */

    public Iterable<Message> findMessageFrom(User user) {
        if (user == null)
            throw new IllegalArgumentException("entity must be not null");

        Set<Message> messageSet = new HashSet<Message>();
        try (Connection connection = DriverManager.getConnection(url, username, password)) {

            PreparedStatement statement = connection.prepareStatement
                    (
                            "SELECT * from messages " +
                                    " WHERE transmitter = ? "
                    );
        }
 catch (SQLException throwables) {
            throwables.printStackTrace();
        }


        return messageSet;

    }

    /**
     *
     * @param user
     * @return a iterable list of messages , these messages represent all messages received by user given as argument
     */
    public Iterable<Message> findMessageTo(User user) {

        if (user == null)
            throw new IllegalArgumentException("entity must be not null");

        Set<Message> messageSet = new HashSet<Message>();

        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement
                    (" select * from messages M" +
                                    " inner join receivers R on M.id = R.idmessage "+
                            " where R.idreceiver = ?"
                    );

            statement.setLong(1,user.getId());

            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next())
            {
                Long idMessage = resultSet.getLong("id");
                Long idFrom = resultSet.getLong("transmitter");
                String msg = resultSet.getString("message");
                LocalDateTime date = resultSet.getTimestamp("date").toLocalDateTime();
                Long idReceiver = resultSet.getLong("idreceiver");
                Boolean reply = resultSet.getBoolean("reply");

                List<Long> lst = new ArrayList<Long>();
                lst.add(idReceiver);

                Message message = new Message(idFrom,lst,msg,date);

                message.setId(idMessage);

                message.setStatus(reply);

                messageSet.add(message);

            }


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


        return messageSet;


    }


    /**
     *
     * @param entity
     * @return a object of Message if that message could be send to one or more users
     *       null, otherwise
     */

    public Message sendMessage(Message entity) {

        if (entity == null)
            throw new IllegalArgumentException("entity must be not null");

        validator.validate(entity);

        try (Connection connection = DriverManager.getConnection(url, username, password)) {

            PreparedStatement statement = connection.prepareStatement
                    (
                            "insert into messages(transmitter,message,date) "+
                                    " values (?,?,?)"
                    );

            statement.setLong(1,entity.getIdFrom());
            statement.setString(2,entity.getMessage());
            statement.setTimestamp(3,Timestamp.valueOf(entity.getDateTime()));

            statement.executeUpdate();

            PreparedStatement statement2 = connection.prepareStatement
                    (
                            "select id from messages" +
                                    " where transmitter = ? and message = ? and date=?"
                    );

            statement2.setLong(1,entity.getIdFrom());
            statement2.setString(2,entity.getMessage());
            statement2.setTimestamp(3,Timestamp.valueOf(entity.getDateTime()));

            ResultSet resultSet = statement2.executeQuery();
            resultSet.next();

            Long idMessage = resultSet.getLong(1);


            for(Long idUser : entity.getIdTo())
            {
                PreparedStatement statement1 = connection.prepareStatement
                        (
                                "insert into receivers(idmessage,idreceiver,reply) "+
                                        " values (?,?,?)"
                        );

                statement1.setLong(1,idMessage);
                statement1.setLong(2,idUser);
                statement1.setBoolean(3,false);

                statement1.executeUpdate();

            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return entity;


    }

    /**
     *
     * @param user1
     * @param user2
     * @return a iterable list of messages between user1 and user2
     */
    public Iterable<Message> findMessageFrom2Users(User user1 ,User user2) {
        if (user1 == null || user2 == null)
            throw new IllegalArgumentException("entity must be not null");

        Set<Message> messageSet = new HashSet<Message>();

        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement
                    (" select * from messages m" +
                            " inner join receivers r on m.id = r.idmessage "+
                            " where (m.transmitter = ? or m.transmitter = ? ) and (r.idreceiver = ? or r.idreceiver = ?) "
                    );

            statement.setLong(1,user1.getId());
            statement.setLong(2,user2.getId());

            statement.setLong(3,user1.getId());
            statement.setLong(4,user2.getId());


            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next())
            {
                Long idMessage = resultSet.getLong("id");
                Long idFrom = resultSet.getLong("transmitter");
                String msg = resultSet.getString("message");
                LocalDateTime date = resultSet.getTimestamp("date").toLocalDateTime();
                Long idReceiver = resultSet.getLong("idreceiver");

                List<Long> lst = new ArrayList<Long>();
                lst.add(idReceiver);

                Message message = new Message(idFrom,lst,msg,date);
                message.setId(idMessage);

                messageSet.add(message);


            }


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


        return messageSet;
    }

    @Override
    public Message save(Message entity) {
        return null;
    }

    @Override
    public Message delete(Message message) {
        return null;
    }

    @Override
    public Message update(Message entity) {
        return null;
    }

    @Override
    public int getSize() {
        return 0;
    }

    @Override
    public Iterable<Message> findAll() {return null; }

    public int getSizeMessageTo(User user, String full_name) {
        int size = 0;
        try(Connection connection = DriverManager.getConnection(url,username,password)) {

            PreparedStatement statement = connection.prepareStatement
                    (
                            "SELECT COUNT(*) from messages M " +
                                    " inner join receivers R on M.id = R.idmessage "+
                                    " where R.idreceiver = ? and ((select email from users where id = M.transmitter ) like ? )"
                    );

            statement.setLong(1,user.getId());
            statement.setString(2,"%"+full_name+"%");


            ResultSet resultSet = statement.executeQuery();

            resultSet.next();

            size = resultSet.getInt(1);

            resultSet.close();

        } catch (SQLException throwable) {
            throwable.printStackTrace();

        }

        return size;
    }

    public HashSet<Message> findMessageToPageable(User user,int limit,int offset, String full_name) {

        if (user == null)
            throw new IllegalArgumentException("entity must be not null");

        HashSet<Message> messageSet = new HashSet<Message>();

        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement
                    (
                            "SELECT * from messages M " +
                            " inner join receivers R on M.id = R.idmessage "+
                            " where R.idreceiver = ? and ((select email from users where id = M.transmitter ) like ? )" +
                            " LIMIT ? OFFSET ?"

                    );

            statement.setLong(1,user.getId());
            statement.setString(2,"%"+full_name+"%");
            statement.setInt(3,limit);
            statement.setInt(4,offset);



            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next())
            {
                Long idMessage = resultSet.getLong("id");
                Long idFrom = resultSet.getLong("transmitter");
                String msg = resultSet.getString("message");
                LocalDateTime date = resultSet.getTimestamp("date").toLocalDateTime();
                Long idReceiver = resultSet.getLong("idreceiver");
                Boolean reply = resultSet.getBoolean("reply");

                List<Long> lst = new ArrayList<Long>();
                lst.add(idReceiver);

                Message message = new Message(idFrom,lst,msg,date);

                message.setId(idMessage);

                message.setStatus(reply);

                messageSet.add(message);

            }


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


        return messageSet;
    }
}

