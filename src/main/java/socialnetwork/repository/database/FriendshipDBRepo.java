package socialnetwork.repository.database;

import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;

import javax.xml.transform.Result;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FriendshipDBRepo implements Repository<Tuple<Long,Long>, Friendship> {
    private String url;
    private String username;
    private String password;
    private Validator<Friendship> validator;

    public FriendshipDBRepo(String url, String username, String password, Validator<Friendship> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    public List<Friendship> findAllFriendshipFromPeriodWith(User user , String year, String month)
    {
        if (user == null)
            throw new IllegalArgumentException("User must be not null");

        List<Friendship> friendshipList = new ArrayList<Friendship>();

        try (Connection connection = DriverManager.getConnection(url, username, password)) {


            PreparedStatement statement = connection.prepareStatement
                    (
                            "select * from friendships " +
                                    " where (friendships.id1 = ? or friendships.id2 = ?) and  friendships.data >= ?  and friendships.data <= ?"
                    );

            Date date1 = Date.valueOf(year+"-"+month+"-01");
            Date date2 = Date.valueOf(year+"-"+month+"-31");

            statement.setLong(1,user.getId());
            statement.setLong(2,user.getId());
            statement.setDate(3,date1);
            statement.setDate(4,date2);


            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next())
            {
                Long id1 = resultSet.getLong(1);
                Long id2 = resultSet.getLong(2);
                LocalDate date = resultSet.getTimestamp(3).toLocalDateTime().toLocalDate();

                Friendship friendship = new Friendship(new Tuple<>(id1,id2),date);

                friendshipList.add(friendship);

            }


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return friendshipList;
    }


    public List<Friendship> findAllFriendshipWhit_Pageable(int limit ,int offset,User user,String full_name) {
        if (user == null)
            throw new IllegalArgumentException("User must be not null");

        List<Friendship> friendshipList = new ArrayList<Friendship>();

        try (Connection connection = DriverManager.getConnection(url, username, password)) {

            PreparedStatement statement = connection.prepareStatement
                    (
                            "select * from friendships " +
                                    " where id1 = ? and ((select concat(first_name,' ',last_name) from users where id = id2) like ?)" +
                                    "or id2 = ? and ((select concat(first_name,' ',last_name) from users where id = id1) like ?)" +
                                    " LIMIT ? OFFSET ?"
                    );


            statement.setLong(1,user.getId());
            statement.setString(2,"%"+full_name+"%");
            statement.setLong(3,user.getId());
            statement.setString(4,"%"+full_name+"%");
            statement.setInt(5,limit);
            statement.setInt(6,offset);

            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next())
            {
                Long id1 = resultSet.getLong(1);
                Long id2 = resultSet.getLong(2);
                LocalDate date = resultSet.getTimestamp(3).toLocalDateTime().toLocalDate();

                Friendship friendship = new Friendship(new Tuple<>(id1,id2),date);

                friendshipList.add(friendship);

            }


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return friendshipList;
    }



    public List<Friendship> findAllFriendshipWhit(User user) {
        if (user == null)
            throw new IllegalArgumentException("User must be not null");

        List<Friendship> friendshipList = new ArrayList<Friendship>();

        try (Connection connection = DriverManager.getConnection(url, username, password)) {

            PreparedStatement statement = connection.prepareStatement
                    (
                      "select * from friendships " +
                      " where id1 = ? or id2 = ? "
                    );

            statement.setLong(1,user.getId());
            statement.setLong(2,user.getId());

            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next())
            {
                Long id1 = resultSet.getLong(1);
                Long id2 = resultSet.getLong(2);
                LocalDate date = resultSet.getTimestamp(3).toLocalDateTime().toLocalDate();

                Friendship friendship = new Friendship(new Tuple<>(id1,id2),date);

                friendshipList.add(friendship);

            }


        } catch (SQLException e) {
            e.printStackTrace();
        }

        return friendshipList;
    }

        @Override
    public Friendship findOne(Friendship friendship_param) {
        if (friendship_param.getId() == null)
            throw new IllegalArgumentException("Id must be not null");

        Friendship friendship = null;

        try(Connection connection = DriverManager.getConnection(url,username,password)) {

            PreparedStatement statement = connection.prepareStatement
                    (
                            "select * from friendships fs " +
                                    " where fs.id1 = ? and fs.id2 = ?" +
                                    " or fs.id1 = ? and fs.id2= ?"
                    );

            statement.setLong(1,friendship_param.getId().getLeft());
            statement.setLong(2,friendship_param.getId().getRight());

            statement.setLong(3,friendship_param.getId().getRight());
            statement.setLong(4,friendship_param.getId().getLeft());

            ResultSet resultSet = statement.executeQuery();

            if(resultSet.next() == false)
                return null;

            LocalDate date = LocalDate.parse(resultSet.getString(3));


            friendship = new Friendship(friendship_param.getId(),date);


            return friendship;


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        catch (DateTimeParseException e)
        {
            System.out.println(e.getMessage());


        }

        return friendship;

    }

    @Override
    public Iterable<Friendship> findAll() {
        Set<Friendship> friendships = new HashSet<>();

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from friendships");
             ResultSet resultSet = statement.executeQuery()) {

            while(resultSet.next())
            {
                Long id1 = resultSet.getLong(1);
                Long id2 = resultSet.getLong(2);
                LocalDate date = LocalDate.parse(resultSet.getString(3));

                Friendship friendship = new Friendship(date);
                friendship.setId(new Tuple(id1,id2));

                friendships.add(friendship);

            }



        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


        return friendships;

    }

    @Override
    public Friendship save(Friendship entity) {

        if(entity == null)
            throw new IllegalArgumentException("entity must be not null");

        validator.validate(entity);

        if(findOne(entity) != null)
            return entity;

        try(Connection connection = DriverManager.getConnection(url,username,password)) {

            PreparedStatement statement = connection.prepareStatement
                    (
                            "insert into friendships(id1,id2,data) " +
                                    " values (?,?,?)"
                    );

            statement.setLong(1,entity.getId().getLeft());
            statement.setLong(2,entity.getId().getRight());
            statement.setDate(3, Date.valueOf(String.valueOf(entity.getDate())));


            statement.executeUpdate();


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;


    }

    @Override
    public Friendship delete(Friendship friendship_param) {

        if (friendship_param.getId()==null)
            throw new IllegalArgumentException("Id must be not null");

        Friendship  friendship = findOne(friendship_param);

        if(friendship == null)
            return null;


        try(Connection connection = DriverManager.getConnection(url,username,password)) {

            PreparedStatement statement = connection.prepareStatement
                    (
                            "delete from friendships fs "+
                                    " where fs.id1 = ? and fs.id2 = ? "+
                                    " or fs.id1 = ? and fs.id2 = ?"
                    );

            statement.setLong(1,friendship_param.getId().getLeft());
            statement.setLong(2,friendship_param.getId().getRight());
            statement.setLong(3,friendship_param.getId().getRight());
            statement.setLong(4,friendship_param.getId().getLeft());

            statement.executeUpdate();



        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return friendship;
    }

    @Override
    public Friendship update(Friendship entity) {
        if(entity == null)
            throw new IllegalArgumentException("Entity must be not null");

        Friendship friendship = findOne(entity);

        if(friendship == null)
            return entity;

        try(Connection connection = DriverManager.getConnection(url,username,password)) {

            PreparedStatement statement = connection.prepareStatement
                    (
                      "update friendships "+
                            " set data = ? "+
                              "where id1 = ? and id2 = ? or id1 = ? and id2 = ?"

                    );

            statement.setDate(1, Date.valueOf(String.valueOf(entity.getDate())));
            statement.setLong(2,entity.getId().getLeft());
            statement.setLong(3,entity.getId().getRight());
            statement.setLong(4,entity.getId().getRight());
            statement.setLong(5,entity.getId().getLeft());

            statement.executeUpdate();



        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;

    }

    public int getSizeFriendsWith(User user,String full_name) {
        int size = 0;
        try(Connection connection = DriverManager.getConnection(url,username,password)) {

            PreparedStatement statement = connection.prepareStatement
                    ("SELECT COUNT(*) FROM friendships" +
                            "   where id1 = ? and ((select concat(first_name,' ',last_name) from users where id = id2) like ?)" +
                            "   or id2 = ? and ((select concat(first_name,' ',last_name) from users where id = id1) like ?)"
                    );

            statement.setLong(1,user.getId());
            statement.setString(2,"%"+full_name+"%");
            statement.setLong(3,user.getId());
            statement.setString(4,"%"+full_name+"%");

            ResultSet resultSet = statement.executeQuery();

            resultSet.next();

            size = resultSet.getInt(1);


        } catch (SQLException throwable) {
            throwable.printStackTrace();

        }

        return size;
    }


    @Override
    public int getSize() {
        int size = 0;
        try(Connection connection = DriverManager.getConnection(url,username,password)) {

            PreparedStatement statement = connection.prepareStatement("SELECT COUNT(*) FROM friendships");

            ResultSet resultSet = statement.executeQuery();

            resultSet.next();

            size = resultSet.getInt(1);


        } catch (SQLException throwable) {
            throwable.printStackTrace();

        }

        return size;
    }
}
