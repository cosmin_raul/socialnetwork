package socialnetwork.domain;

import org.graalvm.compiler.serviceprovider.IsolateUtil;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class MessageDto {

    private Long idMessage;
    private String emailFrom;
    private String localDateTime;
    private  DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");

    private String message;
    private Boolean reply;

    public MessageDto(Long idMessage, String emailFrom, String localDateTime, String message, Boolean reply) {
        this.idMessage = idMessage;
        this.emailFrom = emailFrom;
        this.localDateTime = localDateTime;

        this.message = message;
        this.reply = reply;
    }

    public Long getIdMessage() {


        return idMessage;

    }

    public void setIdMessage(Long idMessage) {
        this.idMessage = idMessage;
    }

    public String getEmailFrom() {
        return emailFrom;
    }

    public void setEmailFrom(String emailFrom) {
        this.emailFrom = emailFrom;
    }

    public String getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(String localDateTime) {
        this.localDateTime = localDateTime;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean isReply() {
        return reply;
    }

    public void setReply(Boolean reply) {
        this.reply = reply;
    }
}
