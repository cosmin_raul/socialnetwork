package socialnetwork.domain;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class FriendRequestDto {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String localDateTime;
    private String status;

    public FriendRequestDto(Long id, String firstName, String lastName, String email, String localDateTime, String status) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.localDateTime = localDateTime;
        this.status = status;
    }

    public String getFullStringRequest(){
        return id+" "+ firstName +  " " + lastName + " " + email + " " + localDateTime + " " + status;
    }


    @Override
    public String toString() {
        return "FriendRequestDto{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", localDate=" + localDateTime +
                ", status='" + status + '\'' +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDate(String localDateTime) {
        this.localDateTime = localDateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
