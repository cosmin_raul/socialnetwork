package socialnetwork.domain;

import java.text.DateFormat;
import java.time.LocalDate;

public class FriendShipDto extends User{

    LocalDate localDate;

    public FriendShipDto(User user, LocalDate localDate) {
        super(user.getId(),user.getFirstName(), user.getLastName(), user.getEmail(), user.getPassword());
        this.localDate = localDate;
    }


    public LocalDate getLocalDate() {
        return localDate;
    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }


    @Override
    public String toString() {
        return getId()+";"+ getEmail() + ";"  + localDate;

    }
}


