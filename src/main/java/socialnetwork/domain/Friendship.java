package socialnetwork.domain;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;


public class Friendship extends Entity<Tuple<Long,Long>> {

    LocalDate date;

    User user1,user2;

    public User getUser1() {
        return user1;
    }

    public User getUser2() {
        return user2;
    }

    public void setUser1(User user1) {
        this.user1 = user1;
    }

    public void setUser2(User user2) {
        this.user2 = user2;
    }

    public Friendship() {
        super(new Tuple<>(0l,0l));
        date = LocalDate.now();

    }

    public Friendship(Tuple<Long,Long> tuple){
        super(tuple);
    }
    public Friendship(LocalDate date){
        super(new Tuple<>(0l,0l));
        this.date = date;

    }

    public Friendship(Tuple<Long,Long> tuple,LocalDate date){
        super(tuple);
        this.date = date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Friendship{" +
                "Id1: " + getId().getLeft() + "  " +
                "Id2: " + getId().getRight() + "  " +
                " date=" + date +
                '}';
    }

    /**
     *
     * @return the date when the friendship was created
     */
    public LocalDate getDate() {
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Friendship that = (Friendship) o;
        return Objects.equals(date, that.date) &&
                Objects.equals(user1, that.user1) &&
                Objects.equals(user2, that.user2);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, user1, user2);
    }
}
