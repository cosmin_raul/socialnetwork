package socialnetwork.domain;

import java.time.LocalDateTime;

public class Event extends Entity<Long>{

    private User user;
    private LocalDateTime localDateTime;
    private String eventName;
    private Long subscribers;
    private Boolean subscribed;



    public Event(Long id) {
        super(id);
    }

    public Event(Long aLong, User user, LocalDateTime localDateTime, String eventName, Long subscribers,Boolean subscribed) {
        super(aLong);
        this.user = user;
        this.localDateTime = localDateTime;
        this.eventName = eventName;
        this.subscribers = subscribers;
        this.subscribed = subscribed;
    }
    public Event(Long aLong,  LocalDateTime localDateTime, String eventName, Long subscribers) {
        super(aLong);

        this.localDateTime = localDateTime;
        this.eventName = eventName;
        this.subscribers = subscribers;
        this.subscribed = subscribed;
    }

    public Boolean getSubscribed() {
        return subscribed;
    }

    public void setSubscribed(Boolean subscribed) {
        this.subscribed = subscribed;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public Long getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(Long subscribers) {
        this.subscribers = subscribers;
    }

    @Override
    public String toString() {
        return "Event{" +
                "userFULL NAME =" + user.getFullName() +
                ", localDateTime=" + localDateTime +
                ", eventName='" + eventName + '\'' +
                ", subscribers=" + subscribers +
                '}';
    }
}
