package socialnetwork.domain;


import java.util.Objects;
import java.util.Vector;

public class User extends Entity<Long>{
    private String firstName;
    private String lastName;
    private int group = 0;
    private String email;
    private Password password;



    private Vector<User> friends = new Vector<User>();

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Password getPassword() {
        return password;
    }

    public void setPassword(Password password) {
        this.password = password;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }



    public User(Long id, String firstName, String lastName,String email,Password password) {
        super(id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;


    }

    public User( String firstName, String lastName,String email,Password password) {
        super(0l);
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;


    }

    public User(Long id, String firstName, String lastName) {
        super(id);
        this.firstName = firstName;
        this.lastName = lastName;

    }

    public User( String firstName, String lastName) {
        super(0l);
        this.firstName = firstName;
        this.lastName = lastName;

    }

    public User(Long id)
    {
        super(id);
    }

    public User(){
        this(0l);
    }



    public String getFullName(){
        return firstName+" "+lastName;
    }
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Vector<User> getFriends() {
        return friends;
    }

    public void addFriend(User friend){
        friends.add(friend);
    }

    public void removeFriend(User friend){
        friends.remove(friend);

    }

    public String showFriends()
    {
        String str = "";
        for(User u : friends)
            str = str + " ["+u.getId()+";"+u.firstName + ";" + u.lastName+"] ";

        return str;

    }
    @Override
    public String toString() {
        return "User{ Id: " + getId() + " " +
                "email: " + email + " " +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", friends=" + showFriends();

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return group == user.group &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(email, user.email) &&
                Objects.equals(password, user.password) &&
                Objects.equals(friends, user.friends);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, group, email, password, friends);
    }
}