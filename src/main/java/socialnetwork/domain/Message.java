package socialnetwork.domain;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class Message extends Entity<Long> {

   private Long idFrom;
   private User userFrom = null;
   private String message;
   private List<Long> idTo;
   private List<User> userTo = null;
   private LocalDateTime dateTime;

   private boolean status;

    Message reply = null;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }



    public Message getReply() {
        return reply;
    }

    public void setReply(Message reply) {
        this.reply = reply;
    }

    public Message(Long idFrom, List<Long> idTo, String message, LocalDateTime dateTime) {
        super(0l);
        this.idFrom = idFrom;
        this.message = message;
        this.idTo = idTo;
        this.dateTime = dateTime;
    }

    public Message(Long id ,Long idFrom, List<Long> idTo, String message, LocalDateTime dateTime) {
        super(id);
        this.idFrom = idFrom;
        this.message = message;
        this.idTo = idTo;
        this.dateTime = dateTime;
    }


    public Message(Long id){
        super(id);

    }

    @Override
    public String toString() {
        return "Message{" +
                "idFrom=" + idFrom +
                ", message='" + message + '\'' +
                ", userTo=" + userTo +
                ", dateTime=" + dateTime +
                ", status=" + status +
                '}';
    }

    public User getUserFrom() {
        return userFrom;
    }

    public void setUserFrom(User userFrom) {
        this.userFrom = userFrom;
    }

    public List<User> getUserTo() {
        return userTo;
    }

    public void setUserTo(List<User> userTo) {
        this.userTo = userTo;
    }

    public Long getIdFrom() {
        return idFrom;
    }

    public void setIdFrom(Long idFrom) {
        this.idFrom = idFrom;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Long> getIdTo() {
        return idTo;
    }

    public void setIdTo(List<Long> idTo) {
        this.idTo = idTo;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message1 = (Message) o;
        return status == message1.status &&
                Objects.equals(idFrom, message1.idFrom) &&
                Objects.equals(userFrom, message1.userFrom) &&
                Objects.equals(message, message1.message) &&
                Objects.equals(idTo, message1.idTo) &&
                Objects.equals(userTo, message1.userTo) &&
                Objects.equals(dateTime, message1.dateTime) &&
                Objects.equals(reply, message1.reply);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idFrom, userFrom, message, idTo, userTo, dateTime, status, reply);
    }
}
