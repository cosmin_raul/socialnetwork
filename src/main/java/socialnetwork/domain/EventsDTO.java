package socialnetwork.domain;

public class EventsDTO {
    private Long idEvent;
    private String eventName;
    private String user_name;
    private String date;
    private Long subscribers;
    private Boolean status;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public EventsDTO(Long idEvent, String eventName, String user_name, String date, Long subscribers, Boolean status) {
       this.idEvent = idEvent;
        this.eventName = eventName;
        this.user_name = user_name;
        this.date = date;
        this.subscribers = subscribers;
        this.status = status;
    }

    public Long getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(Long idEvent) {
        this.idEvent = idEvent;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(Long subscribers) {
        this.subscribers = subscribers;
    }
}
