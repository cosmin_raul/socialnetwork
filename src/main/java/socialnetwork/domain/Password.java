package socialnetwork.domain;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;

public class Password  extends  Entity<Long>{

    private String password;
    private byte[] salt = new byte[16];
    private byte[] hash;

    public Password(){
        this(0l);
    }

    public Password(Long aLong) {
        super(aLong);
    }
    public Password(Long aLong,String password) {
        super(aLong);
        this.password = password;
    }

    public void setPassword_String(String password) {
        this.password = password;
    }

    public void setSalt(byte[] salt) {
        this.salt = salt;
    }

    public void setHash(byte[] hash) {
        this.hash = hash;
    }

    public String getPassword_String() {
        return password;
    }

    public byte[] getSalt() {
        return salt;
    }

    public byte[] getHash() {
        return hash;
    }

    /**
     * implementing PBKDF2 encode password algorithm
     */
    public void encodePassword(){

        SecureRandom random = new SecureRandom();
        this.salt = new byte[16];
        random.nextBytes(this.salt);


        KeySpec spec = new PBEKeySpec(password.toCharArray(), this.salt, 65536, 128);

        SecretKeyFactory factory = null;
        try {
            factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            this.hash = factory.generateSecret(spec).getEncoded();

        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }

    public boolean checkEquality(byte[] hash_param,byte[] salt_param){

        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt_param, 65536, 128);

        SecretKeyFactory factory = null;
        try {
            factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            this.hash = factory.generateSecret(spec).getEncoded();

        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        if( Arrays.equals(hash_param, this.hash))
            return true;

        return false;
    }
}
