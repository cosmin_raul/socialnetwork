package socialnetwork.domain.validators;

import socialnetwork.domain.FriendRequest;
import socialnetwork.exceptions.FriendRequestException;
import socialnetwork.exceptions.UserException;

public class FriendRequestValidator implements Validator<FriendRequest> {
    @Override
    public void validate(FriendRequest entity) throws FriendRequestException {


        if(entity.getIdFrom() == entity.getIdTo())
            throw new FriendRequestException("Invalid id for a friend request");


        if(!entity.getStatus().equals("pending") && !entity.getStatus().equals("approved") && !entity.getStatus().equals("rejected"))
            throw new FriendRequestException("Status from friendRequest must be: pending,approved,rejected");



    }
}
