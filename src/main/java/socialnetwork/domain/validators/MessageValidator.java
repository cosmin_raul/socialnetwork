package socialnetwork.domain.validators;

import socialnetwork.domain.Message;
import socialnetwork.exceptions.MessageException;


public class MessageValidator implements Validator<Message> {
    @Override
    public void validate(Message entity) throws MessageException {
            if(entity.getMessage().length() == 0 || entity.getMessage().length() >= 200)
                throw new MessageException("Invalid message!");

    }
}
