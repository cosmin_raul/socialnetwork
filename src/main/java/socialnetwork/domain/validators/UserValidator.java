package socialnetwork.domain.validators;


import socialnetwork.domain.User;
import socialnetwork.exceptions.UserException;

public class UserValidator implements Validator<User> {
    /**
     *
     * @param str
     * @return true , if str can be a name of a person
     *         false, otherwise
     */
    private boolean isCorrect(String str) {

        return str.matches("[A-Z][a-z]+");

    }

    private boolean userName(String str){

        String[] lst = new String[2];
        boolean k = false;

        if (str.contains("-") ){
            k = true;
            lst = str.split("-");
        }

        if(k) {
            return isCorrect(lst[0]) && isCorrect(lst[1]);
        }
        else{
            return isCorrect(str);
        }
    }

    public void validFirstLastName(User entity) throws UserException{
        String errors = "";

        if(entity.getFirstName().length() < 2 || entity.getFirstName().length() > 25 || !userName(entity.getFirstName()))
            errors += "First name is invalid!\n";

        if(entity.getLastName().length() < 2 || entity.getLastName().length() > 25 || !userName(entity.getLastName()))
            errors += "Last name is invalid!\n";

        if(errors.length() > 0)
            throw new UserException(errors);


    }
    /**
     *
     * @param entity : User
     * @throws UserException if user last name or first name are empty
     */
    @Override
    public void validate(User entity) throws UserException {

        String errors = "";

        if(entity.getFirstName().length() < 2 || entity.getFirstName().length() > 25 || !userName(entity.getFirstName()))
            errors += "First name is invalid!\n";

        if(entity.getLastName().length() < 2 || entity.getLastName().length() > 25 || !userName(entity.getLastName()))
           errors += "Last name is invalid!\n";


        if(!check_email_address(entity.getEmail()))
            errors += "Email is invalid(format eg: name@addres.domain)!\n";

        if(entity.getPassword().getPassword_String().length() < 5)
            errors += "Password is to short!\n";

        if(errors.length() > 0)
            throw new UserException(errors);

    }

    private boolean check_email_address(String email) {

        return email.matches("[A-Z_a-z0-9]+@[a-zA-Z]+\\.[a-z]+");
    }
}
