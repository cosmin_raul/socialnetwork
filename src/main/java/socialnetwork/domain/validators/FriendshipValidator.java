package socialnetwork.domain.validators;

import socialnetwork.domain.Friendship;
import socialnetwork.exceptions.FriendshipException;
import socialnetwork.exceptions.UserException;

public class FriendshipValidator implements Validator<Friendship> {
    /**
     *
     * @param entity : Friendship
     * @throws FriendshipException if id1 == id2
     */
    @Override
    public void validate(Friendship entity) throws FriendshipException {
            if(entity.getId().getLeft() == entity.getId().getRight())
                throw new UserException("Id1 must be different from id2!");


    }
}
