package socialnetwork.domain.validators;

import socialnetwork.domain.Event;
import socialnetwork.exceptions.EventsException;
import socialnetwork.exceptions.UserException;

import java.time.LocalDateTime;

public class EventValidator implements Validator<Event>{
    @Override
    public void validate(Event entity) throws EventsException {
        if(entity.getEventName().isEmpty())
            throw new EventsException("Invalid event name!");

        if(entity.getLocalDateTime().isBefore(LocalDateTime.now()))
            throw new EventsException("Invalid date event!");


    }
}
