package socialnetwork.domain.validators;

import socialnetwork.exceptions.UserException;

public interface Validator<T> {
    void validate(T entity) throws UserException;
}