package socialnetwork.domain;

import java.time.LocalDateTime;
import java.util.Objects;

public class FriendRequest extends Entity<Long>{

    private Long idFrom;
    private Long idTo;
    private User userFrom = null;
    private User userTo = null;

    private LocalDateTime localDateTime;

    private String status;

    public FriendRequest(Long id){
        super(id);
    }

    public FriendRequest(Long idFrom, Long idTo) {
        super(0l);
        this.idFrom = idFrom;
        this.idTo = idTo;
    }

    public FriendRequest(Long idFrom, Long idTo, String status,LocalDateTime localDateTime) {
        super(0l);
        this.idFrom = idFrom;
        this.idTo = idTo;
        this.status = status;
        this.localDateTime = localDateTime;

    }

    public FriendRequest(Long id,Long idFrom, Long idTo, String status,LocalDateTime localDateTime) {
        super(id);
        this.idFrom = idFrom;
        this.idTo = idTo;
        this.status = status;
        this.localDateTime = localDateTime;

    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }

    public Long getIdFrom() {
        return idFrom;
    }

    public void setIdFrom(Long idFrom) {
        this.idFrom = idFrom;
    }

    public Long getIdTo() {
        return idTo;
    }

    public void setIdTo(Long idTo) {
        this.idTo = idTo;
    }

    public User getUserFrom() {
        return userFrom;
    }

    public void setUserFrom(User userFrom) {
        this.userFrom = userFrom;
    }

    public User getUserTo() {
        return userTo;
    }

    public void setUserTo(User userTo) {
        this.userTo = userTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FriendRequest that = (FriendRequest) o;
        return Objects.equals(idFrom, that.idFrom) &&
                Objects.equals(idTo, that.idTo) &&
                Objects.equals(userFrom, that.userFrom) &&
                Objects.equals(userTo, that.userTo) &&
                Objects.equals(localDateTime, that.localDateTime) &&
                Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idFrom, idTo, userFrom, userTo, localDateTime, status);
    }
}
