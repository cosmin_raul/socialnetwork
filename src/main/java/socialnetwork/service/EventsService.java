package socialnetwork.service;

import socialnetwork.domain.Event;
import socialnetwork.domain.EventsDTO;
import socialnetwork.domain.User;
import socialnetwork.exceptions.EventsException;
import socialnetwork.repository.database.EventsDBRepo;
import socialnetwork.repository.database.UserDBRepo;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EventsService implements Observable {

    private final int limit = 10;
    private EventsDBRepo repoEvents;
    private UserDBRepo repoUser;

    private List<Observer> observers = new ArrayList<Observer>();

    public EventsService(EventsDBRepo eventsDBRepo, UserDBRepo userDBRepo) {
        this.repoEvents = eventsDBRepo;
        this.repoUser = userDBRepo;
    }


    public void saveEvent(Event event){
        repoEvents.save(event);
        notifyObservers();

    }

    public void updateEvent(Event event){
        repoEvents.update(event);
    }

    public List<Event> getAllEventsPageable(User user ,int page, String full_name){
        int offset = limit * (page - 1);
       List<Event> result = repoEvents.findAllPageable(user, limit,offset,full_name);

      result.forEach(f->f.setUser(repoUser.findOne(f.getUser())));

       return result;
    }

    public int getNrOfPagesEvents(String full_name){

            int nrOfElems = repoEvents.getSizePageable(full_name);

            if(nrOfElems % limit == 0)
                return Math.max(nrOfElems / limit,1);
            else
                return  nrOfElems / limit + 1;


    }


    @Override
    public void addObserver(Observer e) {

        observers.add(e);
    }

    @Override
    public void removeObserver(Observer e) {
        observers.remove(e);

    }

    @Override
    public void notifyObservers() {
        observers.forEach(Observer::update);

    }

    public void subscribeForEvent(User user, EventsDTO event,DateTimeFormatter formatter) {

        if(LocalDateTime.parse(event.getDate(),formatter).isBefore(LocalDateTime.now()))
            throw new EventsException("That event has already started, no changes can be made!");

        repoEvents.subscribe(user.getId(),event.getIdEvent());
        Event event1 = new Event(event.getIdEvent(), LocalDateTime.parse(event.getDate(),formatter),event.getEventName(),event.getSubscribers());
        event1.setSubscribers(event1.getSubscribers() + 1);
        repoEvents.update(event1);

        notifyObservers();



    }

    public void unSubscribeForEvent(User user, EventsDTO event,DateTimeFormatter formatter) {
        if(LocalDateTime.parse(event.getDate(),formatter).isBefore(LocalDateTime.now()))
            throw new EventsException("That event has already started, no changes can be made!");

        repoEvents.unSubscribe(user.getId(),event.getIdEvent());
        Event event1 = new Event(event.getIdEvent(), LocalDateTime.parse(event.getDate(),formatter),event.getEventName(),event.getSubscribers());

        event1.setSubscribers(event1.getSubscribers() - 1);

        repoEvents.update(event1);

        notifyObservers();
    }

    public List<Event> getAllEvents(User user) {

        int nrOfElems = repoEvents.getSizePageable("");

        List<Event> result = repoEvents.findAllPageable(user,nrOfElems ,0,"");

        result.forEach(f->f.setUser(repoUser.findOne(f.getUser())));

        return result;
    }
}
