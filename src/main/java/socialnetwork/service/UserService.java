package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.exceptions.*;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.*;
import socialnetwork.repository.paging.Paginator;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;
import java.util.*;


public class UserService extends AbstractService implements Observable {

    private final int rows = 10;
    private ImageProfileDBRepo imageProfileDBRepo;
    public UserService(UserDBRepo repoUser, FriendshipDBRepo repoFriendship, MessageDBRepo repoMessage, FriendRequestsDBRepo repoRequest,ImageProfileDBRepo imageProfileDBRepo) {
        super(repoUser, repoFriendship, repoMessage, repoRequest);
        this.imageProfileDBRepo = imageProfileDBRepo;
    }




    private List<Observer> observers = new ArrayList<Observer>();



    public Iterable<User> getPagingUsers(int nrPage) {
       return  repoUser.getPagingUsers(rows,(nrPage-1) * rows);
    }

    public int getNrOfPages(){
        int nrOfElems = repoUser.getSize();

        if(nrOfElems % rows == 0)
            return Math.max(1,nrOfElems / rows);
        else
            return  nrOfElems / rows + 1;

    }
    /**
     *
     * @return all users from DB
     */
    public Iterable<User> findAll(){
        return repoUser.findAll();
    }


    /**
     * @param email           - string ,
     * @param password_string - password in string format
     * @return user if in DB exist a user with that email and password
     * @throws IdentificationException - if the identification data is invalid
     */
    public User signIn(String email, String password_string) {

        User user = new User();
        user.setEmail(email);

        User user1 = repoUser.findOne(user);

        if (user1 == null)
            throw new IdentificationException("Invalid email!");


        //check equality between passwords
        Password password = new Password();
        password.setPassword_String(password_string);

        if (!password.checkEquality(user1.getPassword().getHash(), user1.getPassword().getSalt()))
            throw new IdentificationException("Invalid password!");

        return user1;

    }

    public void updateUserData(User user){
        repoUser.update(user);

    }


    @Override
    public void addObserver(Observer e) {
        observers.add(e);

    }

    @Override
    public void removeObserver(Observer e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers() {
        observers.forEach(Observer::update);
    }

    public void addImage(User user, String path) {

        if(imageProfileDBRepo.findOne(user).equals(""))
            imageProfileDBRepo.save(user,path);
        else
            imageProfileDBRepo.update(user,path);

    }

    public String findImageFor(User user) {
        return imageProfileDBRepo.findOne(user);
    }
}