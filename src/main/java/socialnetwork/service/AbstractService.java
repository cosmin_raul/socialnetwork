package socialnetwork.service;

import socialnetwork.domain.User;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.FriendRequestsDBRepo;
import socialnetwork.repository.database.FriendshipDBRepo;
import socialnetwork.repository.database.MessageDBRepo;
import socialnetwork.repository.database.UserDBRepo;

public abstract class AbstractService {
    protected UserDBRepo repoUser;
    protected FriendshipDBRepo repoFriendship;
    protected MessageDBRepo repoMessage;
    protected  FriendRequestsDBRepo repoRequest;


    public AbstractService(UserDBRepo repoUser, FriendshipDBRepo repoFriendship , MessageDBRepo repoMessage , FriendRequestsDBRepo requestsDBRepo) {
        this.repoUser = repoUser;
        this.repoFriendship = repoFriendship;
        this.repoMessage = repoMessage;
        this.repoRequest = requestsDBRepo;


    }


}
