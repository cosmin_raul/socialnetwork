package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.exceptions.FriendRequestException;
import socialnetwork.exceptions.FriendshipException;
import socialnetwork.exceptions.UserException;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.FriendRequestsDBRepo;
import socialnetwork.repository.database.FriendshipDBRepo;
import socialnetwork.repository.database.MessageDBRepo;
import socialnetwork.repository.database.UserDBRepo;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FriendShipService extends AbstractService implements Observable {

    private final int rows = 10;
    public FriendShipService(UserDBRepo repoUser, FriendshipDBRepo repoFriendship, MessageDBRepo repoMessage, FriendRequestsDBRepo requestsDBRepo) {
        super(repoUser, repoFriendship, repoMessage, requestsDBRepo);
    }


   private List<Observer> observers = new ArrayList<>();

    /**
     *
     * @return the number of pages
     */
    public int getNrOfPagesUsers(){
        int nrOfElems = repoUser.getSize();

        if(nrOfElems % rows == 0)
            return Math.max(1,nrOfElems / rows);
        else
            return  nrOfElems / rows + 1;

    }

    /**
     *
     * @param nrPage
     * @param full_name
     * @return the users whose name contains full_name from a specific page
     */
    public Iterable<User> getPagingUsersSearch(int nrPage,String full_name) {
        return  repoUser.getPagingUsersSearch(rows,(nrPage-1) * rows,full_name);
    }

    /**
     *
     * @param full_name
     * @return the number of pages with users who have in their name string full_name
     */
    public int getNrOfSearchPagesUsers(String full_name){
        int nrOfElems = repoUser.getSearchSize(full_name);

        if(nrOfElems % rows == 0)
            return Math.max(1,nrOfElems / rows);
        else
            return  nrOfElems / rows + 1;
    }


    /**
     *
     * @return all users from DB
     */
    public Iterable<User> findAll(){
        return repoUser.findAll();
    }


    /**
     * @param user  :User
     * @param month :String which represent a mont in numeric format : ex: 01,02,..,10,11,12
     * @return map with key LocalDate and value User - represent a friend of user given as arg and their friendship was
     * created in a month that is equal with month from 2nd
     */
    public List<Friendship> setFriendsMonthYear(User user, String month, String year) {

        //TODO
        List<Friendship> list = repoFriendship.findAllFriendshipFromPeriodWith(user, year, month);

        list.forEach(friendship -> {
            friendship.setUser1(repoUser.findOne(new User(friendship.getId().getLeft())));
            friendship.setUser2(repoUser.findOne(new User(friendship.getId().getRight())));
        });


        return list;
    }


    /**
     *
     * @param user
     * @param full_name
     * @return return the number of pages which contains users who are friends of user,given as argument, an that friends contains is their name
     * the string full_name
     */
    public int getNrOfPagesFriends(User user,String full_name){

            int nrOfElems = repoFriendship.getSizeFriendsWith(user,full_name);

            if(nrOfElems % rows == 0)
                return Math.max(1,nrOfElems / rows);
            else
                return  nrOfElems / rows + 1;


    }

    public List<FriendShipDto> getFriendsPageable(User user,int page,String full_name) {

        List<FriendShipDto> friendShipDto = new ArrayList<>();

        for(Friendship f :repoFriendship.findAllFriendshipWhit_Pageable(rows,(page-1)*rows, user,full_name))
        {
            if(f.getId().getLeft().equals(user.getId()))
                friendShipDto.add(new FriendShipDto(
                        repoUser.findOne(new User(f.getId().getRight())),
                        f.getDate()));
            else
                friendShipDto.add(new FriendShipDto(
                        repoUser.findOne(new User(f.getId().getLeft())),
                        f.getDate()));

        }

        return friendShipDto;

    }


    /**
     * @param user : User
     * @return List friends
     * function return a list which contains friends of user given as argument
     */
    public List<FriendShipDto> getFriends(User user) {

        List<FriendShipDto> friendShipDto = new ArrayList<>();

        for(Friendship f :repoFriendship.findAllFriendshipWhit(user))
        {
            if(f.getId().getLeft().equals(user.getId()))
                friendShipDto.add(new FriendShipDto(
                        repoUser.findOne(new User(f.getId().getRight())),
                        f.getDate()));
            else
                friendShipDto.add(new FriendShipDto(
                        repoUser.findOne(new User(f.getId().getLeft())),
                        f.getDate()));

        }

        return friendShipDto;

    }

    /**
     *
     * @param user
     * @param start_period
     * @param end_period
     * @return list with friendships which was made between user given as argument and other user for repo start_period and end_period
     */
    public List<FriendShipDto> getFriendsFromAPeriod(User user,LocalDate start_period, LocalDate end_period){
        Predicate<FriendShipDto> before = f->f.getLocalDate().isBefore(end_period);
        Predicate<FriendShipDto> after = f->f.getLocalDate().isAfter(start_period);

        return getFriends(user)
                .stream()
                .filter(before.and(after))
                .collect(Collectors.toList());

    }

    public Map<String,Integer> getResultForChartDateFriends(User user, LocalDate start_period , LocalDate end_period){

        Map<String,Integer> result = new TreeMap<>();

        for(FriendShipDto f : getFriendsFromAPeriod(user,start_period,end_period)){
            String date = String.valueOf(f.getLocalDate());
           if(result.containsKey(date)){
               result.get(date);
               result.replace(date,result.get(date) + 1 );
           }
           else
               result.put(date,1);

        }

        return result;
    }



    public Map<String,Integer> getFriendRequestReceiveFromAPeriodForChart(User user, LocalDate start_period, LocalDate end_period){

        Map<String,Integer> result = new TreeMap<>();

        Predicate<FriendRequest> before = f->f.getLocalDateTime().toLocalDate().isBefore(end_period);
        Predicate<FriendRequest> after = f->f.getLocalDateTime().toLocalDate().isAfter(start_period);

        List<FriendRequest> list = findFriendRequestFor(user)
                .stream()
                .filter(before.and(after))
                .collect(Collectors.toList());

        return getStringIntegerMap(result, list);

    }

    public Map<String,Integer> getFriendRequestSentFromAPeriodForChart(User user, LocalDate start_period, LocalDate end_period){

        Map<String,Integer> result = new TreeMap<>();

        Predicate<FriendRequest> before = f->f.getLocalDateTime().toLocalDate().isBefore(end_period);
        Predicate<FriendRequest> after = f->f.getLocalDateTime().toLocalDate().isAfter(start_period);

        List<FriendRequest> list = findFriendRequestFrom(user)
                .stream()
                .filter(before.and(after))
                .collect(Collectors.toList());

        return getStringIntegerMap(result, list);

    }

    private Map<String, Integer> getStringIntegerMap(Map<String, Integer> result, List<FriendRequest> list) {
        for(FriendRequest f : list){
            String date = String.valueOf(f.getLocalDateTime().toLocalDate());
            if(result.containsKey(date)){
                result.get(date);
                result.replace(date,result.get(date) + 1 );

            }
            else
                result.put(date,1);
        }
        return result;
    }


    public int getNrOfPagesFriendRequestReceive(User user,String full_name){
        int nrOfElems =repoRequest.getSizeFriendRequestReceive(user,full_name);

        if(nrOfElems % rows == 0)
            return Math.max(1,nrOfElems / rows);
        else
            return  nrOfElems / rows + 1;

    }

    public HashSet<FriendRequest> findFriendRequestReceivePaginate(User user,int page,String full_name){

        int limit = rows;
        int offset = (page -1) * rows;
        HashSet<FriendRequest> list = (HashSet<FriendRequest>) repoRequest.findFriendRequestReceivePaginate(user,limit,offset,full_name);

        list.forEach(friendRequest -> friendRequest.setUserFrom(repoUser.findOne(new User(friendRequest.getIdFrom()))));

        return list;

    }


    public int getNrOfPagesFriendRequestSent(User user,String full_name){
        int nrOfElems =repoRequest.getSizeFriendRequestSent(user,full_name);

        if(nrOfElems % rows == 0)
            return Math.max(1,nrOfElems / rows);
        else
            return  nrOfElems / rows + 1;

    }

    public HashSet<FriendRequest> findFriendRequestSentPaginate(User user,int page,String full_name){

        int limit = rows;
        int offset = (page -1) * rows;
        HashSet<FriendRequest> list = (HashSet<FriendRequest>) repoRequest.findFriendRequestSentPaginate(user,limit,offset,full_name);

        list.forEach(friendRequest -> friendRequest.setUserTo(repoUser.findOne(new User(friendRequest.getIdTo()))));

        return list;

    }

    /**
     * @param user - User
     * @return a list with friend request for a specific user given as argument
     */
    public List<FriendRequest> findFriendRequestFor(User user) {


        HashSet<FriendRequest> list = (HashSet<FriendRequest>) repoRequest.findAll();

        List<FriendRequest> result = list
                .stream()
                .filter(f -> f.getIdTo().equals(user.getId()))
                .collect(Collectors.toList());

        result.forEach(friendRequest -> friendRequest.setUserFrom(repoUser.findOne(new User(friendRequest.getIdFrom()))));

        return result;

    }

    /**
     * @param user - User
     * @return a list with friend request sent for a specific user given as argument
     */
    public List<FriendRequest> findFriendRequestFrom(User user) {

        HashSet<FriendRequest> list = (HashSet<FriendRequest>) repoRequest.findAll();

        List<FriendRequest> result = list
                .stream()
                .filter(f -> f.getIdFrom().equals(user.getId()))
                .collect(Collectors.toList());

        result.forEach(friendRequest -> friendRequest.setUserTo(repoUser.findOne(new User(friendRequest.getIdTo()))));

        return result;

    }



    /**
     *
     * @param IDfrom :user
     * @param IDto:user
     *
     * @throws FriendRequestException if that friend request doesn't exist in DB
     *
     */
    public void removeFriendRequest(Long  IDfrom ,Long IDto){

        if(repoRequest.delete(new FriendRequest(IDfrom,IDto)) == null)
            throw new FriendRequestException("That friend request doesn't exist!");

        notifyObservers();


    }


    /**
     * @param user - User
     * @param friend  - User
     *
     * @throws FriendRequestException -if the user friend who send request is for, doesn't exist in DB
     *                                -if user is already friend with user friend
     *                                -if you try to send a friend request to you
     *                                -if that user friend send a friend request to user which is on pending
     *
     * */
    public void sendFriendRequest(User user, User friend) {

        //case when I want to send a friend request to a friend
        if(repoFriendship.findOne(new Friendship(new Tuple<>(user.getId(), friend.getId()))) != null)
            throw new FriendRequestException("You are already friend whit this user!");


        if(user.getId().equals(friend.getId()))
            throw new FriendRequestException("You can't send a friend request to you!");

        User user_find = repoUser.findOne(friend);

        if (user_find == null)
            throw new FriendRequestException("In DB doesn't exit a user with given id!");

        //check if that friend already send to user a friend request which is in pending
        FriendRequest friendRequest_founded = repoRequest.findOne(new FriendRequest(user_find.getId(),user.getId()));

        if(friendRequest_founded != null && friendRequest_founded.getStatus().equals("pending"))
            throw new FriendRequestException("That user already send a friend request to you, which is on pending!");





        //case when that user that I send before, reject my friend request I can send again another friend request to him

        FriendRequest friendRequestResult = repoRequest.findOne(new FriendRequest(user.getId(),friend.getId()));

        if(friendRequestResult  != null && friendRequestResult .getStatus().equals("rejected"))
            repoRequest.delete(friendRequestResult);


        //add a new friend request which is in pending
        FriendRequest friendRequest = new FriendRequest(user.getId(), user_find.getId(), "pending", LocalDateTime.now());

        if(repoRequest.save(friendRequest) != null)
            throw new FriendRequestException("Your already have a friend request sent to this user, which is on pending!");



    }


    /**
     * @param user     -User
     * @param id       - Long
     * @param response - String
     * @throws FriendRequestException if in DB doesn't exist this friend request
     *                                -if user already respond for that request
     * @throws FriendshipException    if user replied to friend request with "approved" and new friendship couldn't be saved in DB
     *                                <p>
     *                                the function responds for a friend request sent by user with id given to a user given as argument
     */
    public void answerFriendRequest(User user, Long id, String response) {

        FriendRequest friendRequest = repoRequest.findOne(new FriendRequest(id, user.getId()));

        if (friendRequest == null)
            throw new FriendRequestException("This friend request doesn't exit in DB!");


        if (!friendRequest.getStatus().equals("pending"))
            throw new FriendRequestException("For this request you already respond with: " + friendRequest.getStatus());

        friendRequest.setStatus(response);

        repoRequest.update(friendRequest);

        if (response.equals("approved")) //if user accept that request there will be a friendship between them
        {
            Friendship friendship = new Friendship(LocalDate.now());
            friendship.setId(new Tuple<>(id, user.getId()));

            if (repoFriendship.save(friendship) != null)  //save new friendship
                throw new FriendshipException("Friendship couldn't be saved!");

        }


        notifyObservers(); //notify all observers if something has been change

    }

    /**
     * @param id1 : Long
     * @param id2 : Long
     * @return Friendship object , if object couldn't be saved
     * null, otherwise
     * @throws UserException            if the entity is not valid
     * @throws IllegalArgumentException if the given entity is null.
     */
    public Friendship addFriendship(Long id1, Long id2) {


        Friendship friendship1 = new Friendship();
        friendship1.setId(new Tuple(id1, id2));

        if (repoUser.findOne(new User(id1)) == null || repoUser.findOne(new User(id2)) == null)
            throw new UserException("Id1 or id2 doesn't exist in social network!");


        Friendship friendship =  repoFriendship.save(friendship1);

        if(friendship == null)
            notifyObservers();

        return friendship;
    }


    /**
     * @param id1:Long
     * @param id2:Long
     * @return Friendship object : if friendship between id1 and id2 exist and could be deleted
     *                             null-otherwise
     */
    public Friendship removeFriendShip(Long id1, Long id2) {

        Friendship friendship = repoFriendship.delete(new Friendship(new Tuple(id1, id2)));

        //1.implement part when a user send me friend request a accept it and after that i remove it
        //I send to him another request he accept it and after that he block me
        //in this case this two users can't be friends anymore
        //2. when i send a friend request to a user he accept me and then i remove him i can't send another friend request to him
        //==> if I remove a friendship I remove all friend requests between them

        if(friendship != null)
        {
            repoRequest.delete(new FriendRequest(id2,id1));
            repoRequest.delete(new FriendRequest(id1,id2));
            notifyObservers();
        }


        return friendship;
    }


    @Override
    public void addObserver(Observer e) {
        observers.add(e);

    }

    @Override
    public void removeObserver(Observer e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers() {
        observers.forEach(x-> x.update());

    }
}
