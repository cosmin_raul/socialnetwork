package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.exceptions.MessageException;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.FriendRequestsDBRepo;
import socialnetwork.repository.database.FriendshipDBRepo;
import socialnetwork.repository.database.MessageDBRepo;
import socialnetwork.repository.database.UserDBRepo;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class MessageService  extends AbstractService implements Observable {

    private List<Observer> observers = new ArrayList<>();
    private final int rows = 10;

    public MessageService(UserDBRepo repoUser, FriendshipDBRepo repoFriendship, MessageDBRepo repoMessage, FriendRequestsDBRepo requestsDBRepo) {
        super(repoUser, repoFriendship, repoMessage, requestsDBRepo);
    }



    /**
     * @param user   - User
     * @param listId - List of Long
     * @param msg    - String
     *               <p>
     *               throw MessageException if User with id1 doesn't exit in DB or is no friendship between user and user with id1
     *               <p>
     *               function try to send a message from user given as 1st arg to user with id give in 2st argument
     */

    public void sendMessage(User user, List<Long> listId, String msg) {

        for (long id : listId) {
            User user1 = repoUser.findOne(new User(id));

            if (user1 == null)
                throw new MessageException("Person doesn't exit in DB");

            Friendship friendship = repoFriendship.findOne(new Friendship(new Tuple<>(user.getId(), id)));

            if (friendship == null)
                throw new MessageException("You are not friend with a person!");

        }

        Message message = new Message(user.getId(), listId, msg, LocalDateTime.now());

        repoMessage.sendMessage(message);

    }


    /**
     *
     * @param user
     * @param full_name
     * @return the number of pages
     */
    public int getInboxSize(User user,String full_name) {

        int nrOfElems = repoMessage.getSizeMessageTo(user,full_name);

        if(nrOfElems % rows == 0)
            return Math.max(1,nrOfElems / rows);
        else
            return  nrOfElems / rows + 1;


    }


    /**
     *
     * @param user
     * @param page
     * @param full_name
     * @return A HashSet of Messages from a specific page
     */
    public HashSet<Message> getInboxPageable(User user,int page,String full_name) {

        int limit = rows;
        int offset = (page-1) * rows;

        HashSet<Message> list = (HashSet<Message>) repoMessage.findMessageToPageable(user,limit,offset,full_name);

        list
                .stream()
                .forEach(f -> f.setUserFrom(repoUser.findOne(new User(f.getIdFrom()))));

        return list;

    }


    /**
     * @param user -User
     *             <p>
     *             return the inbox messages of an user given as argument
     */
    public HashSet<Message> getInbox(User user) {

        HashSet<Message> list = (HashSet<Message>) repoMessage.findMessageTo(user);

        list
                .stream()
                .forEach(f -> f.setUserFrom(repoUser.findOne(new User(f.getIdFrom()))));

        return list;

    }


    /**
     * @param user - User
     * @param id1  - Long
     *             throw MessageException if User with id1 doesn't exit in DB or is no friendship between user and user with id1
     */
    public List<Message> getMessagesBetween2Users(User user, Long id1) {
        User user1 = repoUser.findOne(new User(id1));

        if (user1 == null)
            throw new MessageException("Person doesn't exit in DB");

        Friendship friendship = repoFriendship.findOne(new Friendship(new Tuple<>(user.getId(), id1)));

        if (friendship == null)
            throw new MessageException("You are not friend whit this person!");

        HashSet<Message> list = (HashSet<Message>) repoMessage.findMessageFrom2Users(user, user1);

        list
                .stream()
                .forEach(f -> {
                    f.setUserFrom(repoUser.findOne(new User(f.getIdFrom())));
                    List<User> lst = new ArrayList<>();
                    lst.add(repoUser.findOne(new User(f.getIdTo().get(0))));
                    f.setUserTo(lst);

                });


        return list.stream().sorted(Comparator.comparing(Message::getDateTime)).collect(Collectors.toList());

    }


    /**
     *
     * @param user
     * @param email_friend
     * @param start
     * @param end
     * @return a List of MessagesDto , message receive from a friend between two dates
     */
    public List<MessageDto> getMessagesReceiveFromFriendFromPeriod(User user,String email_friend , LocalDateTime start,LocalDateTime end )
    {

        return getReceiveMessageFromAPeriodDTO(user,start,end)
                .stream()
                .filter(f->f.getEmailFrom().equals(email_friend))
                .sorted(Comparator.comparing(MessageDto::getLocalDateTime))
                .collect(Collectors.toList());



    }

    /**
     *
     * @param user
     * @param id
     * @param start_date
     * @param end_date
     * @return all messages from two users between two dates
     */

    public Map<String,Integer> getMessagesBetween2UsersFromAPeriodChart(User user , Long id , LocalDate start_date , LocalDate end_date)
    {
        Predicate<Message> before = m->m.getDateTime().toLocalDate().isBefore(end_date);
        Predicate<Message> after = m->m.getDateTime().toLocalDate().isAfter(start_date);

        List<Message> list = getMessagesBetween2Users(user,id)
                .stream()
                .filter(before.and(after))
                .collect(Collectors.toList());

        Map<String,Integer> result = new TreeMap<>();

        for(Message msg : list)
        {
            String date = String.valueOf(msg.getDateTime().toLocalDate());
            if(result.containsKey(date))
                  result.replace(date,result.get(date) + 1);
            else
                  result.put(date,1);
        }

        return result;

    }



    /**
     *
     * @param user - the user that replies to the message
     * @param idMessage - the id of the message the user replies to
     * @param response - the data of the message
     *
     * function reply for a message ,that message has idMessage
     */

    public void replyMessage(User user, Long idMessage, String response) {

        Message message = repoMessage.findOne(new Message(idMessage));

        if (message == null)
            throw new MessageException("No message was found!");

        List<Long> receivers = message.getIdTo();

        boolean ok = false;

        for (Long id : receivers)
            if (id.equals(user.getId())) {
                ok = true;
                break;
            }

        if (!ok)
            throw new MessageException("You are not a receiver for this message!");

        repoMessage.replyMessage(message, user, response);

        notifyObservers();

    }


    /**
     *
     * @param user
     * @param start_period
     * @param end_period
     * @return all messages received by an User from a specific period
     */

    public  List<Message> getReceiveMessagesForAnUserFromAPeriod(User user , LocalDateTime start_period , LocalDateTime end_period)
    {

        Predicate<Message> period_after = msg -> msg.getDateTime().isAfter(start_period);
        Predicate<Message> period_before = msg -> msg.getDateTime().isBefore(end_period);

        return ((HashSet<Message>) repoMessage.findMessageTo(user))
                .stream()
                .filter(period_after.and(period_before))
                .collect(Collectors.toList());
    }

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

    public List<MessageDto> getReceiveMessageFromAPeriodDTO(User user,LocalDateTime start, LocalDateTime end)
    {
       return  getReceiveMessagesForAnUserFromAPeriod(user ,start, end)
                .stream()
                .map(f->{
                    User user_from = repoUser.findOne(new User(f.getIdFrom()));
                    return new MessageDto(f.getId(),user_from.getEmail(),f.getDateTime().format(formatter),f.getMessage(),f.getStatus());
                })
               .collect(Collectors.toList());
    }


    @Override
    public void addObserver(Observer e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer e) {
        observers.remove(e);

    }

    @Override
    public void notifyObservers() {
        observers.forEach(Observer::update);

    }
}
