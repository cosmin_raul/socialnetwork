package socialnetwork.service;

import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.User;
import socialnetwork.exceptions.UserException;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.FriendRequestsDBRepo;
import socialnetwork.repository.database.FriendshipDBRepo;
import socialnetwork.repository.database.MessageDBRepo;
import socialnetwork.repository.database.UserDBRepo;

import java.util.*;


public class AdminService extends AbstractService {

    public AdminService(UserDBRepo repoUser, FriendshipDBRepo repoFriendship , MessageDBRepo repoMessage , FriendRequestsDBRepo repoRequest) {
        super(repoUser, repoFriendship,repoMessage, repoRequest);
    }

    /**
     *
     * @param x :int
     * @param n :int
     * @param nrComp : int
     * @param viz : int[]
     * @param matrix: int[][]
     * @param nodeUser: HasMap<Integer,User>
     *
     *    making a depth first search starting from x and store for all nodes(Users) their connected component
     */
    private void DFS(int x, int n, int nrComp ,int[] viz ,int[][] matrix,HashMap<Integer,User> nodeUser ) {

        User user = nodeUser.get(x); //save connected component for user which is encoded with the node x
        user.setGroup(nrComp);
        repoUser.update(user);


        viz[x] = nrComp;    //mark that we visit this node

        for(int i = 0 ; i < n ; i++) //try to go at neighbors of x
        {
            if(matrix[x][i] == 1 && viz[i] == 0)
                DFS(i,n,nrComp,viz,matrix,nodeUser);

        }

    }




    /**
     *
     * @param n : int , number of nodes
     * @param matrix :int[][] , represent adjacent matrix
     * @param nodeUser : HashMap<Integer,User> , key represent a uniq nod for one user
     *
     *   initialization of the adjacent matrix
     */
   private void initialization_matrix(int n , int[][] matrix,HashMap<Integer,User> nodeUser  ){

            for(Friendship friendship : repoFriendship.findAll())
            {
                Long x = friendship.getId().getLeft();
                Long y = friendship.getId().getRight();

                int find=0,x1=0,y1=0;
               for(int i = 0 ; i < n ;i++)
               {
                       if(nodeUser.get(i).getId() == x)
                       {
                           find++;
                           x1 = i;
                       }

                       if(nodeUser.get(i).getId() == y)
                       {
                           find++;
                           y1 = i;
                       }

                       if(find == 2)
                           break;
               }

                matrix[x1][y1] = matrix[y1][x1] = 1;

            }



    }




    /**
     *
     * @param n:int
     * @param viz:int[]
     * @param nodeUser:  HashMap Integer,User , key represent a uniq nod for one user
     * @return int : number of connected components
     */
    public int calculate(int n,int[] viz,HashMap<Integer,User> nodeUser  ) {

        int[][] matrix = new int[n][n];
        int nrComponents = 0;

        initialization_matrix(n, matrix, nodeUser); //adjacent matrix initialization

        //starting a dfs from each nod that wasn't visited
        for (int i = 0; i < n; i++) {
            if (viz[i] == 0) {
                nrComponents++;
                DFS(i, n, nrComponents, viz, matrix,nodeUser);
            }
        }

        return nrComponents;
    }




    /**
     *
     * @param nodeUser : HashMap Integer,User , for all users I will define a key which will represent the number of nod in out graph for that user
     */
    public void initialization_map(HashMap<Integer,User> nodeUser  ) {

        int vertex = 0;
        for( User user : repoUser.findAll())
        {
            nodeUser.put(vertex,user);
            vertex++;
        }

    }




    /**
     *
     * @param userNode :HashMap-Long,Integer
     * @param nodeUser :HashMap-Integer,User
     * @param friendsList: Vector of users
     *
     *     -function gets 2 maps and each user from map will be encoded with a integer which represent one node
     *     -function gets 2 maps because one map will be use for encoding users with an node ,to be able to calculate the path
     *                   and another one will be use for decoding the result ,that nodes from result represent Users
     *
     */
    private void initialization_maps(HashMap<Long,Integer> userNode , HashMap<Integer,User> nodeUser,Vector<User> friendsList){

        int nod = 0;

        for(User user: friendsList)
        {
            nodeUser.put(nod,user);
            userNode.put(user.getId(),nod);
            nod++;
        }

    }




    /**
     *
     * @param adjacentList :HashMap-Integer,ArrayList-Integer
     * @param friendsList : Vector-Users
     * @param userNode : HashMap:Long ,Integer , Long -represent the id of a user and Integer -represent his node
     *
     *  -this function will compute an adjacent list for a specific connected graph
     *
     */
    private void initialization_adjacentList(HashMap<Integer, ArrayList<Integer>>  adjacentList , Vector<User> friendsList ,  HashMap<Long,Integer> userNode ) {

        for(User user : friendsList)//for all users from connected component I will obtain his list of friends encoded with nodes
        {
            ArrayList<Integer> friendsNode = new ArrayList<Integer>();

            for(User user1 : user.getFriends())
                friendsNode.add(userNode.get(user1.getId()));


            adjacentList.put(userNode.get(user.getId()),friendsNode);

        }

    }




    /**
     *
     * @param x : int
     * @param parent :int[]
     * @param path : Vector-Integer
     *
     *   - x represent the last node form a path
     *   - using vector parent ,recursively I will obtain a path ,users from that path will be stored in Vector path
     */
   private void computePath(int x, int[] parent, Vector<Integer> path) {

        if(x == -1)
            return;

        computePath(parent[x],parent,path);

        path.add(x);

    }


    /**
     *
     * @param x :int
     * @param list : HashMap-integer, arraylist
     * @param dist : int[] ,dist[x] = y, y represent the distance from root to x
     * @param parent : int[], parent[x] = y, y represent the parent of x
     * @param viz:int[] , viz[x] = 1, means that x is already visited
     *                    viz[x] = 0, means that x was not visited
     */
    private void findRecursively(int x, HashMap<Integer,ArrayList<Integer>> list, int[] dist, int[] parent, int[] viz , int[] copyD, int[] copyP) {
            int k = 0;
            for(int val : list.get(x))
            {

                if( dist[x] + 1 > dist[val] && viz[val] == 0)
                {
                    int aux = dist[val];

                    dist[val] = dist[x] + 1;
                    parent[val] = x;
                    viz[val] = 1;
                    findRecursively(val,list,dist,parent,viz,copyD,copyP);
                    viz[val] = 0;
                    dist[val] = aux;
                    parent[val]=0;

                }
                else
                    k++;

            }

            if( k  == list.get(x).size())
            {
                int max1 = 0;
                int max2 = 0;

                for(int i = 0 ; i < dist.length ; i++)
                {
                    if(dist[i] > max1)
                        max1 = dist[i];

                    if(copyD[i] > max2)
                        max2 = copyD[i];

                }

                if(max1 > max2)
                    for(int i = 0 ; i < dist.length ;i++)
                    {
                        copyD[i] = dist[i];
                        copyP[i] = parent[i];
                    }

            }

    }


    /**
     *
     * @param contor -
     * @param list -
     * @param n -
     * @return a path
     *
     * each node from graph will be the root and form there I try to find a max path
     */

    Vector<Integer> tryNodeForStart(int contor, HashMap<Integer,ArrayList<Integer>> list, int n) {

        int[] dist = new int[n];
        int[] copiedD = new int[n];
        int[] copyP = new int[n];
        int[] parent = new int[n];
        int[] viz = new int[n];

        for(int i = 0 ; i < n ;i ++)
        {
            copiedD[i] = -1;
            dist[i] = -1;
            parent[i]=0;
            copyP[i] = 0;
            viz[i] = 0;
        }

        copiedD[contor] = 0;
        dist[contor] = 0;
        parent[contor] = -1;
        copyP[contor] = -1;
        viz[contor] = 1;


        findRecursively(contor,list,dist,parent,viz,copiedD,copyP);

        int max = -1, poz=-1;

        for(int i = 0 ; i < n ;i++)
            if(copiedD[i] > max)
            {
                poz = i;
                max = copiedD[i];

            }


        Vector<Integer> path = new Vector<Integer>();

        computePath(poz, copyP, path);


        return path;

    }



    /**
     *
     * @param list ,HashMap: integer ,arraylist
     * @param n: integer
     * @return vector of integers which represent the node form longest path

     */

    private Vector<Integer> backTrackingSolutionPath(HashMap<Integer,ArrayList<Integer>> list, int n){


        Vector<Integer> result = new Vector<Integer>();

        for(int contor = 0 ; contor < n ;contor++)
        {
            Vector<Integer> path = tryNodeForStart (contor,list,n);

            if(path.size() > result.size())
                result = path;
        }

        return result;


    }




    /**
     *
     * @param friendsList: Vector: User represent a connected component
     *
     * @return a list with Users which represent the longest path in that group
     */
    private Vector<User> findNetworkPath(Vector<User> friendsList) {

        HashMap<Long,Integer> userNode = new HashMap<Long,Integer>(); //this hashmap is used for encode users with a node

        HashMap<Integer,User> nodeUser = new HashMap<Integer, User>(); //this hashmap is use to decode a user from a node

        initialization_maps(userNode,nodeUser,friendsList);

        HashMap<Integer, ArrayList<Integer>>  adjacentList = new HashMap<Integer, ArrayList<Integer>>();

        initialization_adjacentList(adjacentList,friendsList,userNode);

        Vector<Integer> resultNode =  backTrackingSolutionPath(adjacentList,friendsList.size());

        Vector<User> resultUser = new Vector<User>();

        for(int i : resultNode)
            resultUser.add(nodeUser.get(i)); //decoding the nodes in users


        return resultUser;

    }


    /**
     *
     * @return Vector: User -represent the longest path from all connected components
     *   Behavior
     *      function divide all users in connected components, and calculate the longest path in each connected component ,and return the longest one
     *      function takes first components which have more users for example I will take a component which has
     *      7 users and path int this component contain 5 users then if I have a component with only 4 user I will not
     *      calculate a path for that group ,because obviously I can't find there a better solution
     */
    public Vector<User> findMostSociableNetWork(){

        int n =  repoUser.getSize();
        int[] viz = new int[n];//for each node[0..n-1] viz[x]=i , i represent connected component for x

        HashMap<Integer,User> nodeUser = new HashMap<Integer,User>();

        initialization_map(nodeUser);

        int nrConnected = calculate(n,viz,nodeUser);

        HashSet<User> users = (HashSet<User>) upgradeFriendshipList(); //get data from DB an store for each user his friend list


        int[] nrOfUsersGroup = new int[nrConnected +1]; //nrOfUsersGroup[x]=i, x-represent the number of a connected group and i represent the number of users from that group

        for(int i = 0 ; i < n; i++)
            nrOfUsersGroup[viz[i]]++;


        Vector<User> networkPath = new Vector<User>();
        Vector<User> longestNetworkPath = new Vector<User>();



        int rep = 0;
        int size_path = 0;

        while(rep < nrConnected)
        {
            rep++;

            Vector<User> vector = new Vector<User>();

            int group = 0;

            int maxim = 0;
            for(int i = 1; i < nrConnected + 1; i++)
                if(nrOfUsersGroup[i] > maxim)
                {
                    maxim = nrOfUsersGroup[i]; //maxim represent the number of users from a group i
                    group = i;
                }

            if(size_path > maxim)// if we have already found a path that contains more users than the number of maxim users in the remaining related components
                break;


            nrOfUsersGroup[group] = -1;

            for(User user : users)
            {
                if(user.getGroup() == group)
                    vector.add(user);
            }

            networkPath = findNetworkPath(vector);
            size_path = networkPath.size();

            if(size_path > longestNetworkPath.size())
                longestNetworkPath = networkPath;

        }


        return longestNetworkPath;


    }



    /**
     *
     * @return int : the number of connected components
     */
    public int findConnected() {

        int n =  repoUser.getSize();
        int[] viz = new int[n];

        HashMap<Integer,User> nodeUser = new HashMap<Integer,User>();

        initialization_map(nodeUser);

        return calculate(n,viz,nodeUser);

    }



    /**
     *
     * @param user : User
     * @return - null if task could not be saved
     *         - task,otherwise
     *
     * @throws UserException
     *            if the entity is not valid
     * @throws IllegalArgumentException
     *             if the given entity is null.
     */
    public User addUser(User user) {
        return repoUser.save(user);
    }



    /**
     *
     * @return all entities
     * 
     * for each user I store his friends
     */

    private Iterable<User> upgradeFriendshipList() {

        HashSet<User> listUser = (HashSet<User>) repoUser.findAll();

        HashSet<Friendship> listFS = (HashSet<Friendship>) repoFriendship.findAll();

        for(Friendship friendship : listFS)
        {
            User user1 = repoUser.findOne(new User(friendship.getId().getRight()));
            User user2 = repoUser.findOne(new User(friendship.getId().getLeft()));

            for( User user : listUser)
            {
                if(user.getId() == user1.getId())
                    user.addFriend(user2);

                if(user.getId() == user2.getId())
                    user.addFriend(user1);

            }
        }



        return listUser;

    }


    /**
     * 
     * @return a iterable list with users and their friends
     */
    public Iterable<User> getAllUsers(){

        return upgradeFriendshipList();

    }


    /**
     *
     * @param id : Long
     * @return User object  if user exist in repository and can be deleted
     *         null ,otherwise
     * @throws IllegalArgumentException
     *                   if the given entity is null.
     .
     */
    public User removeUser(Long id) {

        //TODO : if I remove a user maybe I have to remove all friend request , messages which include him not only friendships

        if(repoUser.findOne(new User(id)) != null) { //if exist one person with that id

            boolean k = true;


            //for all persons which are friends with person with id given as argument, i will remove the friendship between them
            while (k) {
                k = false;

                for (Friendship friendship : repoFriendship.findAll()) {
                    if (friendship.getId().getLeft() == id) {
                        removeFriendShip(id, friendship.getId().getRight());
                        k = true;
                        break;
                    }
                    if (friendship.getId().getRight() == id) {
                        removeFriendShip(friendship.getId().getLeft(), id);
                        k = true;
                        break;
                    }
                }

            }
        }

        return repoUser.delete(new User(id));

    }



    /**
     *
     * @param id1 : Long
     * @param id2 : Long
     * @return Friendship object , if object couldn't be saved
     *         null, otherwise
     *
     * @throws UserException
     *            if the entity is not valid
     * @throws IllegalArgumentException
     *             if the given entity is null.
     */
    public Friendship addFriendship(Long id1 ,Long id2) {

        Friendship friendship1 = new Friendship();
        friendship1.setId(new Tuple<>(id1,id2));

        if(repoUser.findOne(new User(id1)) == null || repoUser.findOne(new User(id2)) == null)
            throw new UserException("Id1 or id2 doesn't exist in social network!");
        

        return repoFriendship.save(friendship1);

    }



    /**
     *
     * @param id1:Long
     * @param id2:Long
     * @return Friendship object : if friendship between id1 and id2 exist and could be deleted
     *         null-otherwise
     */
    public Friendship removeFriendShip(Long id1 , Long id2) {

        return repoFriendship.delete(new Friendship(new Tuple<>(id1, id2)));
        
    }



    
}
