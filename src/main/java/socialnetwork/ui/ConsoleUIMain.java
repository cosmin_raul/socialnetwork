/*
package socialnetwork.ui;

import socialnetwork.domain.User;
import socialnetwork.exceptions.IdentificationException;
import socialnetwork.exceptions.NumberException;
import socialnetwork.service.AdminService;
import socialnetwork.service.UserService;

import java.util.Scanner;

public class ConsoleUIMain implements UI {
    AdminService adminController;
    UserService userController;
    ConsoleUIAdmin consoleUIAdmin;
    ConsoleUIUser consoleUIUser;


    Scanner in = new Scanner(System.in);

    public ConsoleUIMain(AdminService adminController, UserService userController) {
        this.adminController = adminController;
        this.userController = userController;
    }

    */
/**
     * read a number from stdin
     * @throws NumberException if id is <=0
     * @throws NumberFormatException if the inpunt can't be parase to long
     *//*

    private long readId(){
        Long id;

        System.out.print("id: ");
        String line = in.nextLine();
        id = Long.parseLong(line);
        if( id <= 0 )
            throw new NumberException("Id must be > 0!");
        return id;


    }

    private void runUser() {



        System.out.print("email=");
        String email = in.nextLine();

        System.out.print("password=");
        String password = in.nextLine();

        User user = new User(0l);
        user.setEmail(email);
        user.setPassword(password);

        consoleUIUser.authentication(user);

        consoleUIUser.runApp();


    }

    private void runAdmin() {

        consoleUIAdmin.runApp();

    }

    @Override
    public void mainMenu() {

        System.out.println("===============Welcome to our application!=============");
        System.out.println("Choose your function");
        System.out.println("1)Admin");
        System.out.println("2)User");
        System.out.println("11)Exit");

    }


    @Override
    public void runApp() {

        while (true) {
            mainMenu();
            System.out.print("command:");
            int cmd;

            try {
                String line = in.nextLine();
                cmd = Integer.parseInt(line);

            } catch (NumberFormatException exp) {
                System.out.println("Command must be integer!");
                continue;
            }


            if (cmd == 11) {
                System.out.println("Application is closed!");
                return;
            }
            try {
                switch (cmd) {
                    case 1:
                        consoleUIAdmin = new ConsoleUIAdmin(adminController);
                        runAdmin();
                        break;

                    case 2:
                        consoleUIUser = new ConsoleUIUser(userController);
                        runUser();
                        break;


                    default:
                        System.out.println("Invalid command!");

                }
            }
            catch (NumberException exp)
            {
                System.out.println(exp.getMessage());
            }
            catch (NumberFormatException exp)
            {
                System.out.println(exp.getMessage());
            }
            catch(IdentificationException exp)
            {
                System.out.println(exp.getMessage());
            }
        }


    }

}

*/
