/*
package socialnetwork.ui;

import socialnetwork.domain.User;
import socialnetwork.exceptions.FriendshipException;
import socialnetwork.exceptions.NumberException;
import socialnetwork.exceptions.UserException;
import socialnetwork.service.AdminService;

import java.util.Scanner;
import java.util.Vector;

public class ConsoleUIAdmin implements  UI{
    AdminService adminController;
    Scanner in = new Scanner(System.in);


    public ConsoleUIAdmin(AdminService adminController) {
        this.adminController = adminController;
    }


    private void printUsers() {
        System.out.println("All users:");
        adminController.getAllUsers().forEach(System.out::println);


    }


    /**
     * read a number from stdin
     * @throws NumberException if id is <=0
     * @throws NumberFormatException if the inpunt can't be parase to long
     *//*


*/
/*
    private long readId(){
        Long id;

        System.out.print("id: ");
        String line = in.nextLine();
        id = Long.parseLong(line);
        if( id <= 0 )
            throw new NumberException("Id must be > 0!");
        return id;


    }

    /**
     * add a new user
     *//*



    private void addUser() {

        String firstName, lastName , email,password;
        //Long id = readId();

        System.out.print("Last name: ");
        lastName = in.nextLine();

        System.out.print("First name: ");
        firstName = in.nextLine();

        System.out.print("Email: ");
        email = in.nextLine();

        System.out.print("Password: ");
        password = in.nextLine();

        User user = new User(firstName,lastName,email,password);


        if(adminController.addUser(user) == null)
            System.out.println("The user was successfully added!");
        else
            System.out.println("Id already exist in data base!");



    }


    */
/**
     * add a new friendship
     *//*

    private void addFriendShip() {
        Long id1 = readId();
        Long id2 = readId();

        if( adminController.addFriendship(id1,id2) ==  null)
            System.out.println("Friendship was successfully added!");

        else
            System.out.println("Friendship already exist in data base!");


    }


    */
/**
     * remove a user form DB
     *
     *//*

    private void removeUser() {


        Long id = readId();

        User task = adminController.removeUser(id);

        if(task != null)
        {
            System.out.println("User: " + task.toString() + ", is removed!");
        }
        else
            System.out.println("No user with id: " + id + " was found");


    }


    private void removeFriendShip() {
        Long id1,id2;
        id1 = readId();
        id2 = readId();

        if(adminController.removeFriendShip(id1,id2) != null)
            System.out.println("Friendship was successfully deleted!");
        else
            System.out.println("Can't delete this friendship!");
    }


    private void findNrCommunities(){

        System.out.println("Nr of communities: " +  adminController.findConnected());
    }


    private void findMostSociableNetwork() {

        Vector<User> longhestPath = adminController.findMostSociableNetWork();

        if(longhestPath.size() != 0)
        {
            System.out.println("Path was founded in group "+ longhestPath.get(0).getGroup());
            System.out.println("Users from the longest path are: ");
            longhestPath.forEach(System.out::println);

        }
        else
            System.out.println("Could nod find a path!");


    }


    */
/**
     * Application menu
     *//*

    @Override
    public  void mainMenu(){
        System.out.println("=====================ADMIN MENU========================");
        System.out.println("1)Add user");
        System.out.println("2)Remove user");
        System.out.println("3)Add friendship");
        System.out.println("4)Remove firendship");
        System.out.println("5)Print all users");
        System.out.println("6)Find number of communities");
        System.out.println("7)Find most sociable network");

        System.out.println("11)Exit");

    }


    */
/**
     * Function runs all functionalities form application
     *//*

    @Override
    public void runApp() {

        while(true)
        {
            mainMenu();
            System.out.print("command:");
            int cmd;

            try{
                String line = in.nextLine();
                cmd = Integer.parseInt(line);

            }
            catch(NumberFormatException exp)
            {
                System.out.println("Command must be integer!");
                continue;
            }



            if(cmd == 11)
            {
                System.out.println("Admin is logged out!");
                return;
            }
            try{
                switch(cmd)
                {
                    case 1:
                        addUser();
                        break;

                    case 2:
                        removeUser();
                        break;

                    case 3:
                        addFriendShip();
                        break;
                    case 4:
                        removeFriendShip();
                        break;

                    case 5:
                        printUsers();
                        break;

                    case 6:
                        findNrCommunities();
                        break;

                    case 7:
                        findMostSociableNetwork();
                        break;


                    default:
                        System.out.println("Invalid command!\n");


                }

            }
            catch (FriendshipException exp){
                System.out.println(exp.getMessage());
            }
            catch(NumberException exp){
                System.out.println(exp.getMessage());
            }
            catch(UserException exp){
                System.out.println(exp.getMessage());
            }
            catch(NumberFormatException exp)
            {
                System.out.println("Number must be long");
            }
            catch(IllegalArgumentException exp)
            {
                System.out.println(exp.getMessage());
            }


        }
    }
}*/
