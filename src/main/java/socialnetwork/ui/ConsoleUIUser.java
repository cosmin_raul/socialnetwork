/*
package socialnetwork.ui;

import socialnetwork.domain.User;
import socialnetwork.exceptions.FriendRequestException;
import socialnetwork.exceptions.IdentificationException;
import socialnetwork.exceptions.MessageException;
import socialnetwork.exceptions.NumberException;
import socialnetwork.service.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ConsoleUIUser implements  UI {
    UserService userController;
    Scanner in = new Scanner(System.in);
    User user;


    public ConsoleUIUser(UserService userController) {
        this.userController = userController;
    }


    @Override
    public void mainMenu() {
        System.out.println("===================USER MENU================");
        System.out.println("User logged: ID: " + user.getId() + " | "+user.getFirstName() + " | " + user.getLastName());
        System.out.println("1)Show friends");
        System.out.println("2)Friendship from a period");
        System.out.println("3)Send a message");
        System.out.println("4)Show inbox");
        System.out.println("5)Reply for a message");
        System.out.println("6)Show conversation with a user");
        System.out.println("7)Send a friend request");
        System.out.println("8)Show your friend requests received from other users");
        System.out.println("9)Answer to a friend request from a users");
        System.out.println("11)Exit");

    }

    @Override
    public void runApp() {

        while (true) {
            mainMenu();

            System.out.print("command:");
            int cmd;

            try {
                String line = in.nextLine();
                cmd = Integer.parseInt(line);

            } catch (NumberFormatException exp) {
                System.out.println("Command must be integer!");
                continue;
            }


            if (cmd == 11) {
                System.out.println("User is logged out!");
                return;
            }
            try {
                switch (cmd) {
                    case 1:
                        printAllFriends();
                        break;

                    case 2:
                        printAllFriendshipFromMonthYear();
                        break;

                    case 3:
                        sendMessage();
                        break;

                    case 4:
                        showInbox();
                        break;
                    case 5:
                        replyMessage();
                        break;

                    case 6:
                        showConversationWithAnUser();
                        break;

                    case 7:
                        sendFriendRequest();
                        break;

                    case 8:
                        showFriendRequests();
                        break;

                    case 9:
                        answerFriendRequest();
                        break;

                    default:
                        System.out.println("Invalid command!");

                }
            } catch (NumberException exp) {
                System.out.println(exp.getMessage());
            } catch (NumberFormatException exp) {
                System.out.println(exp.getMessage());
            } catch (IdentificationException exp) {
                System.out.println(exp.getMessage());
            } catch (MessageException exp){
                System.out.println(exp.getMessage());
            } catch (FriendRequestException exp)
            {
                System.out.println(exp.getMessage());
            }

        }

    }

    */
/**
     * function reply to friend request received by user logged
     *//*

    private void  answerFriendRequest() {

        System.out.println("Give the id user of who you want to respond at friend request");
        Long id = readId();

        System.out.print("Your response(approved/rejected): ");
        String response = in.nextLine();

        userController.answerFriendRequest(user,id,response);

        System.out.println("Your reply has been sent");




    }


    */
/**
     * function print all friend request given by another users to user logged
     *//*

    private void showFriendRequests() {

        System.out.println("Friend requests: ");
        userController.findFriendRequestFor(user).forEach(

                f-> {
                    System.out.println("-----------------------------------------------------------------------------------------------------------");
                    System.out.println("From: Id: " + f.getUserFrom().getId()+"  Name: "+ f.getUserFrom().getFirstName() + " | " + f.getUserFrom().getLastName());
                    System.out.println("Status: "+f.getStatus() + " | Date: " +f.getLocalDateTime());
                    System.out.println("-----------------------------------------------------------------------------------------------------------");
                }

        );


    }


    */
/**
     * function try to send a friend request from user logged to another user with id given
     *//*

    private void sendFriendRequest() {

        System.out.println("Give the id of the user who you want to send a friend request");
        Long id1 = readId();

       if(userController.sendFriendRequest(user,id1) == null)
       {
           System.out.println("Friend request was successfully sent!");
       }
       else
           System.out.println("Another friend request to the same user was already sent!");


    }



    */
/**
     * function print all friendship for user logged that was created in a specific month
     *//*

    private void printAllFriendshipFromMonthYear() {

        System.out.print("Month in number format: ");
        String month = in.nextLine();

        System.out.print("Year in number format: ");
        String year = in.nextLine();

        userController.setFriendsMonthYear(user,month,year).forEach(f->{
                    if(user.equals(f.getUser1()))
                        System.out.println("id:"+f.getUser2().getId() +" | "+f.getUser2().getLastName()+" | " + f.getUser2().getFirstName()+ " | " + f.getDate());
                    else
                        System.out.println("id:"+f.getUser1().getId() +" | "+f.getUser1().getLastName()+" | " + f.getUser1().getFirstName()+ " | " + f.getDate());
                }


        );


    }


    */
/**
     * function shows all friends of user logged
     *//*

    private void printAllFriends() {

            userController.setFriends(user).forEach(f->{
                        if(user.equals(f.getUser1()))
                            System.out.println("id:"+f.getUser2().getId() +" | "+f.getUser2().getLastName()+" | " + f.getUser2().getFirstName()+ " | " + f.getDate());
                        else
                            System.out.println("id:"+f.getUser1().getId() +" | "+f.getUser1().getLastName()+" | " + f.getUser1().getFirstName()+ " | " + f.getDate());
                    }


            );




    }


    */
/**
     *
     * @param user
     * @return true if in DB exit that user given as argument
     *         false - otherwise
     *//*


    public void authentication(User user) {

        //User user1 = userController.identification(user);

        //this.user = user1;


    }

    */
/**
     * read a number from stdin
     * @throws NumberException if id is <=0
     * @throws NumberFormatException if the inpunt can't be parase to long
     *//*

    private long readId(){
        Long id;

        System.out.print("id: ");
        String line = in.nextLine();
        id = Long.parseLong(line);
        if( id <= 0 )
            throw new NumberException("Id must be > 0!");
        return id;


    }


    */
/**
     * Function send a message from user to another user with id1
     *//*

    private void sendMessage(){

        List<Long> listId = new ArrayList<Long>();

        System.out.println("Give the number of friends for who you want to write:");
        long nr = 0;
        System.out.print("nr: ");
        String line = in.nextLine();
        nr = Long.parseLong(line);
        if( nr <= 0 )
            throw new NumberException("Nr must be > 0!");

        System.out.println("Give the id of the users who you want to write to ");

        for(int i = 0 ; i < nr ; i++)
        {
            Long id1 = readId();
            listId.add(id1);

        }





        System.out.print("Your message: ");
        String msg = in.nextLine();

        userController.sendMessage(user,listId,msg);

        System.out.println("Message was sent!");

    }


    */
/**
     * function shows a conversation between user logged and user with id given
     *//*

    private void showConversationWithAnUser() {

        System.out.println("Give the id of the friend");
        Long id1 = readId();
        userController.getMessagesBetween2Users(user,id1).forEach(i->{

            System.out.println("----------------------------------------------------------------------------------------------------------------");
            System.out.println("From: " + i.getUserFrom().getFirstName() + " " + i.getUserFrom().getLastName());
            System.out.println("To: "+ i.getUserTo().get(0).getFirstName()+" " + i.getUserTo().get(0).getLastName());
            System.out.println("Date: "+i.getDateTime());
            System.out.println("Message:");
            System.out.println(i.getMessage());
            System.out.println("----------------------------------------------------------------------------------------------------------------");


        });






    }


    */
/**
     * function shows the inbox messages for user logged
     *//*

    private void showInbox() {
        System.out.println("Your inbox: ");

        userController.getInbox(user).forEach(i->{
            System.out.println("----------------------------------------------------------------------------------------------------------------");
            System.out.println("Id message: " + i.getId());
            System.out.println("From: id:" +i.getUserFrom().getId()+" name= "+ i.getUserFrom().getFirstName() + " " + i.getUserFrom().getLastName() + " date:"+ i.getDateTime());
            System.out.println("Message:");
            System.out.println(i.getMessage());
            System.out.print("Status:");
            if(i.getStatus() == false)
                System.out.println("Not replied");
            else
                System.out.println("Replied");

            System.out.println("----------------------------------------------------------------------------------------------------------------");
        });
    }


    */
/**
     * function replies for a message received by user logged from another user
     *//*


    private void replyMessage(){
        System.out.println("Give the id of the message for which you want to reply: ");
        Long idMessage = readId();

        System.out.print("Your response: ");
        String response = in.nextLine();


        userController.replyMessage(user,idMessage,response);

        System.out.println("Your reply has been send!");


    }
}


*/
