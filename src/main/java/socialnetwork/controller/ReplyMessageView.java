package socialnetwork.controller;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import socialnetwork.domain.MessageDto;
import socialnetwork.domain.User;
import socialnetwork.exceptions.MessageException;
import socialnetwork.service.MessageService;

import java.io.IOException;

public class ReplyMessageView {
    public TextField textFieldReplyFor;
    public TextField textFieldReceiveFrom;
    public TextField textFieldReceiveTo;
    public TextField textFieldReceiveDate;
    public TextArea textAreaMessageReceive;
    public TextArea textAreaMessageReply;

    private User user;
    private MessageService messageService;
    private AnchorPane rootAnchorPane;
    private MessageDto messageDtoReceive;


    public void setService(MessageService messageService , MessageDto messageDto ,User user,AnchorPane rootAnchorPane){
        this.messageService = messageService;
        this.messageDtoReceive = messageDto;
        this.rootAnchorPane = rootAnchorPane;
        this.user = user;

        setTextFields();

    }

    private void setTextFields(){
        textFieldReplyFor.setText(messageDtoReceive.getEmailFrom());
        textFieldReceiveFrom.setText(messageDtoReceive.getEmailFrom());

        textFieldReceiveTo.setText(user.getEmail());
        textFieldReceiveDate.setText(String.valueOf(messageDtoReceive.getLocalDateTime()));

        textAreaMessageReceive.setText(messageDtoReceive.getMessage());


    }

    public void handleClickOnSend(MouseEvent mouseEvent) {

        if(textFieldReplyFor.getText().equals(""))
        {
            MessageAlert.showErrorMessage(null,"Invalid email for reply");
            return;
        }


        if(textAreaMessageReply.getText().equals("")) {
            MessageAlert.showErrorMessage(null, "Your message reply is empty!");
            return;
        }

        try{
            messageService.replyMessage(user,messageDtoReceive.getIdMessage(),textAreaMessageReply.getText());
        }
        catch (MessageException exp)
        {
            MessageAlert.showErrorMessage(null,exp.getMessage());
        }

        MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,null,"Your reply has been send!");

    }

    public void handleClickOnBack(MouseEvent mouseEvent) {
        FXMLLoader loader  = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/inboxMessageView.fxml"));

        try {

            rootAnchorPane.getChildren().setAll((Node) loader.load());

        }
        catch (IOException e) {
            e.printStackTrace();
        }

        InboxMessageController friendsController = loader.getController();
        friendsController.setService(messageService,user,rootAnchorPane);

    }
}
