package socialnetwork.controller;

import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.UserValidator;
import socialnetwork.exceptions.UserException;
import socialnetwork.service.UserService;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class SettingsController {



    public TextField imagePath;
    private UserService userService;
    private User user;
    private ImageView imageView;
    private Label userLabelName;


    public void setService(UserService userService, User user, ImageView imageViewUser, Label userLabelName) {
        this.userService = userService;
        this.user= user;
        this.imageView = imageViewUser;
        this.userLabelName = userLabelName;

    }

    public void handleOnBrowse(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("File Dialog");

        File file = fileChooser.showOpenDialog(new Stage());

        if(file != null)
        {
            String path = file.getAbsolutePath();

            if(!path.endsWith(".png") && !path.endsWith(".jpg"))
                MessageAlert.showErrorMessage(null,"Profile image must end with .png or .jpg");
            else
                imagePath.setText(path);
        }

    }

    public void handleOnSaveImage(ActionEvent actionEvent) {

        String path = imagePath.getText();
        if(path.equals("") || (!path.endsWith(".jpg") && !path.endsWith(".png"))) {
            MessageAlert.showErrorMessage(null, "Profile image must end with .png or .jpg");
            return;
        }

        File tempFile = new File(path);
        boolean exists = tempFile.exists();

        if(!exists)
        {
            MessageAlert.showErrorMessage(null, "That image profile doesn't exist!");
            return;

        }
        try {
            imageView.setImage(new Image(new FileInputStream(path)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        userService.addImage(user,path);


    }


}
