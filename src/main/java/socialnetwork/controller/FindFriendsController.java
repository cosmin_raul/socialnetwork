package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import socialnetwork.domain.User;
import socialnetwork.exceptions.FriendRequestException;
import socialnetwork.service.FriendShipService;

import java.util.Collection;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class FindFriendsController   {

    private int maxPage;

    public TextField textFieldSearchFriend;

    public TableView<User> tableUsers;
    public TableColumn<User, Long> tableColumnID;
    public TableColumn<User, String> tableColumnFirstName;
    public TableColumn<User, String> tableColumnLastName;
    public TableColumn<User, String> tableColumnEmail;
    public TextField textFieldCurrentPage;
    public TextField textFieldPages;


    private User user;

    private FriendShipService friendShipService;

    private ObservableList<User> modelView = FXCollections.observableArrayList();

    @FXML
    public void initialize(){

        tableColumnID.setCellValueFactory(new PropertyValueFactory<User,Long>("id"));
        tableColumnFirstName.setCellValueFactory(new PropertyValueFactory<User,String>("firstName"));
        tableColumnLastName.setCellValueFactory(new PropertyValueFactory<User,String>("lastName"));
        tableColumnEmail.setCellValueFactory(new PropertyValueFactory<User,String>("email"));

        tableUsers.setItems(modelView);


       // textFieldSearchFriend.textProperty().addListener(x->handleFilter());

    }


    private void handleFilter() {

        String full_name = textFieldSearchFriend.getText();
        Predicate<User> byFullName = user->user.getFullName().contains(full_name);

        modelView.setAll(StreamSupport.stream(friendShipService.findAll().spliterator(),false)
                .filter(byFullName)
                .collect(Collectors.toList())
        );

    }


    public void setService(FriendShipService friendShipService,User user){
        this.friendShipService = friendShipService;
        this.user = user;

        maxPage = friendShipService.getNrOfPagesUsers();
        textFieldCurrentPage.setText("1");
        textFieldPages.setText("/"+String.valueOf(maxPage));

        initModel(1,"");
    }



    public void handleClickOnAddFriend(MouseEvent mouseEvent) {

         User friend = tableUsers.getSelectionModel().getSelectedItem();

         if(friend != null)
         {
             try {
                 friendShipService.sendFriendRequest(user, friend);
                 MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,null,"Your friend request has been send!");
             }
             catch (FriendRequestException exp)
             {
                 MessageAlert.showErrorMessage(null,exp.getMessage());
             }
         }
         else
             MessageAlert.showErrorMessage(null,"Nothing from table was selected!");


    }


    public void handleOnEnter(KeyEvent keyEvent){

        if(keyEvent.getCode().equals(KeyCode.ENTER)) {
            try {
                int pageNr = Integer.parseInt(textFieldCurrentPage.getText());
                if (pageNr < 1 || pageNr > maxPage)
                    MessageAlert.showErrorMessage(null, "Invalid page number!");
                else {
                    initModel(pageNr,textFieldSearchFriend.getText());
                }

            } catch (NumberFormatException exp) {
                MessageAlert.showErrorMessage(null, "Invalid page format!");
            }
        }

    }

    public void handleOnPrevious(ActionEvent actionEvent) {

      int pageNr = 0;
       try {
            pageNr = Integer.parseInt(textFieldCurrentPage.getText());
       }
       catch (NumberFormatException exp)
       {
           MessageAlert.showErrorMessage(null,"Current page is invalid!");
           return;
       }
        if(pageNr == 1 ) {
            MessageAlert.showErrorMessage(null, "This is first page!");
            return;

        }
        if (pageNr < 1 || pageNr > maxPage)
            MessageAlert.showErrorMessage(null, "Invalid page number!");
        else
        {
            String full_name = textFieldSearchFriend.getText();

            initModel(pageNr - 1,full_name);

            textFieldCurrentPage.setText(String.valueOf(pageNr - 1));
        }


    }

    public void handleOnNext(ActionEvent actionEvent) {
        int pageNr = 0;
        try {
            pageNr = Integer.parseInt(textFieldCurrentPage.getText());
        }
        catch (NumberFormatException exp)
        {
            MessageAlert.showErrorMessage(null,"Current page is invalid!");
            return;
        }
        if(pageNr == maxPage ) {
            MessageAlert.showErrorMessage(null, "This is last page!");
            return;
        }
        if (pageNr < 1 || pageNr > maxPage)
            MessageAlert.showErrorMessage(null, "Invalid page number!");
        else
        {
            String full_name = textFieldSearchFriend.getText();

            initModel(pageNr +1 ,full_name);

            textFieldCurrentPage.setText(String.valueOf(pageNr + 1));
        }
    }

    public void initModel(int page, String full_name){
        modelView.setAll((Collection<? extends User>) friendShipService.getPagingUsersSearch(page,full_name));
    }


    public void handleOnEnterPressed(KeyEvent keyEvent) {

        if(keyEvent.getCode().equals(KeyCode.ENTER)){
            String search_full_name = textFieldSearchFriend.getText();

            initModel(1,search_full_name);

            textFieldCurrentPage.setText("1");
            maxPage = friendShipService.getNrOfSearchPagesUsers(search_full_name);
            textFieldPages.setText("/"+maxPage);

        }
    }

    public void handleOnSearch(MouseEvent mouseEvent) {
        String search_full_name = textFieldSearchFriend.getText();

        initModel(1,search_full_name);

        textFieldCurrentPage.setText("1");
        maxPage = friendShipService.getNrOfSearchPagesUsers(search_full_name);
        textFieldPages.setText("/"+maxPage);
    }
}
