package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import socialnetwork.domain.FriendShipDto;
import socialnetwork.domain.User;
import socialnetwork.exceptions.FriendRequestException;
import socialnetwork.exceptions.FriendshipException;
import socialnetwork.service.FriendShipService;
import socialnetwork.service.MessageService;
import socialnetwork.utils.observer.Observer;

import java.io.IOException;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ComposeMessageController implements Observer {

    public TextField textFieldCurrentPage;
    public TextField textFieldPages;
    @FXML TableView<FriendShipDto> tableFriends;
    @FXML TableColumn<FriendShipDto,Long> tableColumnID;
    @FXML TableColumn<FriendShipDto,String> tableColumnFirstName;
    @FXML TableColumn<FriendShipDto,String> tableColumnLastName;
    @FXML TableColumn<FriendShipDto,String> tableColumnEmail;
    @FXML TableColumn<FriendShipDto,LocalDate> tableColumnFriendShipDate;
    @FXML TextField textFieldSearchFriend;
    @FXML TextField textFieldReceivers;

    private MessageService messageService;
    private FriendShipService friendShipService;
    private User user;
    private AnchorPane rootAnchorPane;
    private ObservableList<FriendShipDto> friendships = FXCollections.observableArrayList();

    private double xOffset,yOffset;

    private int maxPage;


    public void setService(MessageService messageService, FriendShipService friendShipService, User user,AnchorPane rootAnchorPane){
        this.messageService = messageService;
        this.friendShipService = friendShipService;
        this.user = user;
        this.rootAnchorPane=rootAnchorPane;
        friendShipService.addObserver(this);
        innitConnections();

        initModel(1,"");

        maxPage = messageService.getInboxSize(user,"");
        textFieldCurrentPage.setText("1");
        textFieldPages.setText("/"+maxPage);


    }

    @FXML
    public void initialize(){
        tableColumnID.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableColumnFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        tableColumnLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        tableColumnEmail.setCellValueFactory(new PropertyValueFactory<>("email"));
        tableColumnFriendShipDate.setCellValueFactory(new PropertyValueFactory<>("localDate"));
        tableFriends.setItems(friendships);
    }

    private void innitConnections(){
        tableFriends.setRowFactory( tv -> {
            TableRow<FriendShipDto> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    FriendShipDto friendShipDto = tv.getSelectionModel().getSelectedItem();
                    ArrayList<String> mails = new ArrayList<>();
                    ArrayList<Long> IDs = new ArrayList<>();
                    mails.add(friendShipDto.getEmail());
                    IDs.add(friendShipDto.getId());
                    showSendMessage(mails,IDs);
                }
            });
            return row ;
        });
      //  textFieldSearchFriend.textProperty().addListener(x->handleFilter());
    }

    private void handleFilter() {
        Predicate<FriendShipDto> filterMailName = f->f.getEmail().contains(textFieldSearchFriend.getText())
                || f.getFullName().contains(textFieldSearchFriend.getText());
        friendships.setAll(friendShipService.getFriends(user)
                .stream()
                .filter(filterMailName)
                .collect(Collectors.toList())
        );
    }

    private void initModel(int page, String full_name){
        friendships.setAll(friendShipService.getFriendsPageable(user,page,full_name));
    }

    @Override
    public void update() {
        initModel(1, textFieldSearchFriend.getText());
        maxPage = messageService.getInboxSize(user,"");
        textFieldCurrentPage.setText("1");
        textFieldPages.setText("/"+maxPage);

    }

    public void handleClickOnCompose(MouseEvent mouseEvent) {
        if(textFieldReceivers.getText().equals(""))
        {
            MessageAlert.showErrorMessage(null,"If you want to compose a message please first choose receivers!");
            return;
        }

        String[] IdAndMails = textFieldReceivers.getText().split(",");
        ArrayList<String> mails = new ArrayList<>();
        ArrayList<Long> IDs = new ArrayList<>();
        String fails = "";
        for(String data : IdAndMails){
            try {
                if(data.equals("") || data.equals(" "))
                    continue;
                String[] split = data.split(";");
                mails.add(split[1]);
                IDs.add(Long.parseLong(split[0]));

            }catch (Exception e){
                System.out.println(e.getMessage());
                fails+=fails+data+"\n";
            }
        }
        if(fails.length()>0)
        {
            Alert alert = new Alert(Alert.AlertType.ERROR,
                    "Following format errors encountered:\n" + fails + "Do you still wish to continue?",
                    ButtonType.YES,ButtonType.NO,ButtonType.CLOSE);
            alert.setHeaderText(null);
            alert.setTitle("Format errors");
            alert.showAndWait();
            if(alert.getResult().equals(ButtonType.YES))
                showSendMessage(mails,IDs);
        }
        else
            showSendMessage(mails,IDs);
    }

    private void showSendMessage(List<String> mails, List<Long> IDs){
        try{

            Stage stage = new Stage();
            stage.initStyle(StageStyle.TRANSPARENT);

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/view/sendMessageView.fxml"));

            AnchorPane layout;
            try {
                layout = loader.load();
                Scene scene = new Scene(layout);
                scene.setFill(Color.TRANSPARENT);

                stage.setScene(scene);

                layout.setOnMousePressed(event -> {
                    xOffset = event.getSceneX();
                    yOffset = event.getSceneY();
                });

                layout.setOnMouseDragged(event -> {
                    stage.setX(event.getScreenX() - xOffset);
                    stage.setY(event.getScreenY() - yOffset);
                });

                SendMessageController controller = loader.getController();
                controller.setService(messageService, user,mails,IDs,stage);
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Error: + " + e.getMessage());
            }

        }
        catch (FriendshipException | IllegalArgumentException  | FriendRequestException exp) {
            MessageAlert.showErrorMessage(null, exp.getMessage());
        }

    }

    public void handleOnClickAddReceiver(MouseEvent mouseEvent) {
        FriendShipDto friendShipDto = tableFriends.getSelectionModel().getSelectedItem();
        if(friendShipDto==null)
        {
            MessageAlert.showErrorMessage(null,"Please select a friend from the table!");
            return;
        }

       if(textFieldReceivers.getText().contains(friendShipDto.getId() +";"))
           MessageAlert.showErrorMessage(null,"Please select a friend from the table which is not already in receiver list!");
       else
           textFieldReceivers.setText(textFieldReceivers.getText()+ friendShipDto.getId()+ ";" + friendShipDto.getEmail() + ",");
    }

    public void handleOnClickSearch(MouseEvent mouseEvent) {
        String search_full_name = textFieldSearchFriend.getText();

        initModel(1,search_full_name);

        textFieldCurrentPage.setText("1");
        maxPage = friendShipService.getNrOfPagesFriends(user,search_full_name);
        textFieldPages.setText("/"+maxPage);
    }

    public void handleOnPrevious(ActionEvent actionEvent) {
        int pageNr = 0;
        try {
            pageNr = Integer.parseInt(textFieldCurrentPage.getText());
        }
        catch (NumberFormatException exp)
        {
            MessageAlert.showErrorMessage(null,"Current page is invalid!");
            return;
        }
        if(pageNr == 1 ) {
            MessageAlert.showErrorMessage(null, "This is first page!");
            return;

        }
        if (pageNr < 1 || pageNr > maxPage)
            MessageAlert.showErrorMessage(null, "Invalid page number!");
        else
        {
            String full_name = textFieldSearchFriend.getText();

            initModel(pageNr - 1,full_name);

            textFieldCurrentPage.setText(String.valueOf(pageNr - 1));
        }

    }

    public void handleOnEnter(KeyEvent keyEvent) {
        if(keyEvent.getCode().equals(KeyCode.ENTER)) {
            try {
                int pageNr = Integer.parseInt(textFieldCurrentPage.getText());
                if (pageNr < 1 || pageNr > maxPage)
                    MessageAlert.showErrorMessage(null, "Invalid page number!");
                else {
                    initModel(pageNr,textFieldSearchFriend.getText());
                }

            } catch (NumberFormatException exp) {
                MessageAlert.showErrorMessage(null, "Invalid page format!");
            }
        }
    }

    public void handleOnNext(ActionEvent actionEvent) {
        int pageNr = 0;
        try {
            pageNr = Integer.parseInt(textFieldCurrentPage.getText());
        }
        catch (NumberFormatException exp)
        {
            MessageAlert.showErrorMessage(null,"Current page is invalid!");
            return;
        }
        if(pageNr == maxPage ) {
            MessageAlert.showErrorMessage(null, "This is last page!");
            return;
        }
        if (pageNr < 1 || pageNr > maxPage)
            MessageAlert.showErrorMessage(null, "Invalid page number!");
        else
        {
            String full_name = textFieldSearchFriend.getText();

            initModel(pageNr +1 ,full_name);

            textFieldCurrentPage.setText(String.valueOf(pageNr + 1));
        }
    }

    public void handleOnEnterSearch(KeyEvent keyEvent) {
        if(keyEvent.getCode().equals(KeyCode.ENTER)){
            String search_full_name = textFieldSearchFriend.getText();

            initModel(1,search_full_name);

            textFieldCurrentPage.setText("1");
            maxPage = friendShipService.getNrOfPagesFriends(user,search_full_name);
            textFieldPages.setText("/"+maxPage);

        }
    }
}
