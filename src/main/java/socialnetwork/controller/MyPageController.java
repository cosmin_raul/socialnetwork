package socialnetwork.controller;

import com.jfoenix.assets.JFoenixResources;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import jdk.vm.ci.meta.Local;
import socialnetwork.domain.Event;
import socialnetwork.domain.EventsDTO;
import socialnetwork.domain.User;
import socialnetwork.exceptions.EventsException;
import socialnetwork.exceptions.FriendRequestException;
import socialnetwork.exceptions.FriendshipException;
import socialnetwork.service.EventsService;
import socialnetwork.utils.observer.Observer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class MyPageController implements Initializable, Observer {

    public ImageView notificationImage;
    public Label nrNotification;
    private double xOffset;
    private double yOffset;


    private EventsService eventsService;
    private User user;
    private int maxPage;
    private  Boolean notification;

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
    public TextField textFieldSearchEvent;
    public TextField textFieldPages;
    public TextField textFieldCurrentPage;

    public TableColumn<EventsDTO,String> eventName;
    public TableColumn<EventsDTO,String> user_name;
    public TableColumn<EventsDTO, String> date;
    public TableColumn<EventsDTO,Long> subscribers;
    public TableColumn<EventsDTO,Boolean> subscribed;
    public TableView<EventsDTO> tableEvents;

    public TableColumn<EventsDTO,Long> idEvent;

    private ObservableList<EventsDTO> modelView = FXCollections.observableArrayList();

    public void setService(EventsService eventsService , User user){
        this.eventsService = eventsService;
        eventsService.addObserver(this);
        this.user = user;
        initModel(1,"");
        textFieldCurrentPage.setText("1");
        maxPage = eventsService.getNrOfPagesEvents("");
        textFieldPages.setText("/"+maxPage);

        setNotificationImage();

    }

    private void setNotificationImage(){
        List<Event> events = eventsService.getAllEvents(user);

        notification = false;

        int nr = 0;

        for(Event event1 : events)
        {
            if(event1.getSubscribed().equals(true) && event1.getLocalDateTime().getYear() == LocalDateTime.now().getYear() && event1.getLocalDateTime().getMonth() == LocalDateTime.now().getMonth()) {

                if (event1.getLocalDateTime().getDayOfMonth() - LocalDateTime.now().getDayOfMonth() == 1)
                {       notification = true;
                        nr++;
                }

                int minutesDifference1 = (event1.getLocalDateTime().getHour()*60 + event1.getLocalDateTime().getMinute()) -( LocalDateTime.now().getHour() * 60 + LocalDateTime.now().getMinute()) ;

                if (event1.getLocalDateTime().getDayOfMonth() == LocalDateTime.now().getDayOfMonth() && minutesDifference1 >= 0 && minutesDifference1 <= 30) {
                    notification = true;
                    nr++;
                }


                int minutesDifference = LocalDateTime.now().getHour() * 60 + LocalDateTime.now().getMinute() - (event1.getLocalDateTime().getHour()*60 + event1.getLocalDateTime().getMinute());

                if (event1.getLocalDateTime().getDayOfMonth() == LocalDateTime.now().getDayOfMonth()
                        && minutesDifference >= 0 && minutesDifference <= 15) {
                    notification = true;
                    nr++;
                }

            }
        }

        if(notification) {
            try {
                nrNotification.setText(String.valueOf(nr));
                notificationImage.setImage(new Image(new FileInputStream("resources/images/redbell.png")));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        else
            try {
                nrNotification.setText("");
                notificationImage.setImage(new Image(new FileInputStream("resources/images/bell.png")));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

    }

    /**
     * show pop up messages for - 1 day
     *                          - 30 minutes before
     *                          -event starts (max 15 minutes ago)
     */
    private void showEventStarts() {
        if(notification) {


            List<Event> events = eventsService.getAllEvents(user);


            for (Event event1 : events) {
                if (event1.getSubscribed().equals(true) && event1.getLocalDateTime().getYear() == LocalDateTime.now().getYear() && event1.getLocalDateTime().getMonth() == LocalDateTime.now().getMonth()) {

                    if (event1.getLocalDateTime().getDayOfMonth() - LocalDateTime.now().getDayOfMonth() == 1)
                        MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Events", "Next day event: " + event1.getEventName() + " will take place at: " + event1.getLocalDateTime().format(formatter));

                    int minutesDifference1 = (event1.getLocalDateTime().getHour()*60 + event1.getLocalDateTime().getMinute()) - (LocalDateTime.now().getHour() * 60 + LocalDateTime.now().getMinute()) ;

                    if (event1.getLocalDateTime().getDayOfMonth() == LocalDateTime.now().getDayOfMonth() && minutesDifference1 >= 0 && minutesDifference1 <= 30)
                        MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Events", "This day event: " + event1.getEventName() + " will take place at: " + event1.getLocalDateTime().format(formatter) + ", there are more " + minutesDifference1 + " minutes until event begins!");


                    int minutesDifference = LocalDateTime.now().getHour() * 60 +LocalDateTime.now().getMinute() - (event1.getLocalDateTime().getHour()*60 + event1.getLocalDateTime().getMinute());

                    if (event1.getLocalDateTime().getDayOfMonth() == LocalDateTime.now().getDayOfMonth()
                        && minutesDifference >= 0 && minutesDifference <= 15)
                        MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Events", "Event: " + event1.getEventName() + " started  " +minutesDifference + " minutes ago!");


                }
            }
            try {
                nrNotification.setText("");
                notificationImage.setImage(new Image(new FileInputStream("resources/images/bell.png")));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            notification = false;

        }

    }


    public void handleOnEnterPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode().equals(KeyCode.ENTER)){
            String search_full_name = textFieldSearchEvent.getText();

            initModel(1,search_full_name);

            textFieldCurrentPage.setText("1");
            maxPage = eventsService.getNrOfPagesEvents(search_full_name);
            textFieldPages.setText("/"+maxPage);

        }
    }

    public void handleOnSearch(MouseEvent mouseEvent) {
        String search_full_name = textFieldSearchEvent.getText();

        initModel(1,search_full_name);

        textFieldCurrentPage.setText("1");
        maxPage = eventsService.getNrOfPagesEvents(search_full_name);
        textFieldPages.setText("/"+maxPage);
    }

    public void handleOnUnsubscribe(MouseEvent mouseEvent) {
        EventsDTO event = tableEvents.getSelectionModel().getSelectedItem();


           if (event != null) {
               if(MessageAlert.showMessageConfirmation("Do you want to unsubscribe?")) {

                   try {
                       eventsService.unSubscribeForEvent(user, event, formatter);
                       MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, null, "You unsubscribed from that event");
                   } catch (RuntimeException exp) {

                       MessageAlert.showErrorMessage(null, exp.getMessage());
                   }
               }
           } else
               MessageAlert.showErrorMessage(null, "Nothing from table was selected!");

    }

    public void handleOnSubscribe(MouseEvent mouseEvent) {
        EventsDTO event = tableEvents.getSelectionModel().getSelectedItem();

        if(event != null)
        {
            try {
                eventsService.subscribeForEvent(user,event,formatter);
                MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,null,"Now you are a subscriber for that event!");
            }
            catch (EventsException exp)
            {
                MessageAlert.showErrorMessage(null,exp.getMessage());
            }
        }
        else
            MessageAlert.showErrorMessage(null,"Nothing from table was selected!");
    }



    public void handleOnNext(ActionEvent actionEvent) {
        int pageNr = 0;
        try {
            pageNr = Integer.parseInt(textFieldCurrentPage.getText());
        }
        catch (NumberFormatException exp)
        {
            MessageAlert.showErrorMessage(null,"Current page is invalid!");
            return;
        }
        if(pageNr == maxPage ) {
            MessageAlert.showErrorMessage(null, "This is last page!");
            return;
        }
        if (pageNr < 1 || pageNr > maxPage)
            MessageAlert.showErrorMessage(null, "Invalid page number!");
        else
        {
            String full_name = textFieldSearchEvent.getText();

            initModel(pageNr +1 ,full_name);

            textFieldCurrentPage.setText(String.valueOf(pageNr + 1));
        }
    }

    public void handleOnEnter(KeyEvent keyEvent) {

        if(keyEvent.getCode().equals(KeyCode.ENTER)) {
            try {
                int pageNr = Integer.parseInt(textFieldCurrentPage.getText());
                if (pageNr < 1 || pageNr > maxPage)
                    MessageAlert.showErrorMessage(null, "Invalid page number!");
                else {
                    initModel(pageNr,textFieldSearchEvent.getText());
                }

            } catch (NumberFormatException exp) {
                MessageAlert.showErrorMessage(null, "Invalid page format!");
            }
        }
    }

    public void handleOnPrevious(ActionEvent actionEvent) {
        int pageNr = 0;
        try {
            pageNr = Integer.parseInt(textFieldCurrentPage.getText());
        }
        catch (NumberFormatException exp)
        {
            MessageAlert.showErrorMessage(null,"Current page is invalid!");
            return;
        }
        if(pageNr == 1 ) {
            MessageAlert.showErrorMessage(null, "This is first page!");
            return;

        }
        if (pageNr < 1 || pageNr > maxPage)
            MessageAlert.showErrorMessage(null, "Invalid page number!");
        else
        {
            String full_name = textFieldSearchEvent.getText();

            initModel(pageNr - 1,full_name);

            textFieldCurrentPage.setText(String.valueOf(pageNr - 1));
        }

    }

    public List<EventsDTO> getDTOEvents(int page, String full_name){

        return eventsService.getAllEventsPageable(user,page,full_name)
                .stream()
                .map(event -> new EventsDTO(
                        event.getId(),
                        event.getEventName(),
                        event.getUser().getFirstName() + " " + event.getUser().getLastName(),
                        event.getLocalDateTime().format(formatter),
                        event.getSubscribers(),
                        event.getSubscribed()


                ))
                .collect(Collectors.toList());
    }

    public void initModel(int page, String full_name){
        modelView.setAll(getDTOEvents(page,full_name));

      //  setNotificationImage();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        idEvent.setCellValueFactory(new PropertyValueFactory<>("idEvent"));
        eventName.setCellValueFactory(new PropertyValueFactory<>("eventName"));
        user_name.setCellValueFactory(new PropertyValueFactory<>("user_name"));
        date.setCellValueFactory(new PropertyValueFactory<>("date"));
        subscribed.setCellValueFactory(new PropertyValueFactory<>("status"));
        subscribers.setCellValueFactory(new PropertyValueFactory<>("subscribers"));

        tableEvents.setItems(modelView);




    }

    @Override
    public void update() {
        initModel(1,textFieldSearchEvent.getText());
        textFieldCurrentPage.setText("1");
        maxPage = eventsService.getNrOfPagesEvents(textFieldSearchEvent.getText());
        textFieldPages.setText("/"+maxPage);
        setNotificationImage();
    }

    public void handleOnCreateEvent(MouseEvent mouseEvent) {


            Stage stage = new Stage();
            stage.initStyle(StageStyle.TRANSPARENT);

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/view/createEventView.fxml"));

            AnchorPane layout;
            try {
                layout = loader.load();
                Scene scene = new Scene(layout);
                scene.setFill(Color.TRANSPARENT);

                stage.setScene(scene);

                layout.setOnMousePressed(event -> {
                    xOffset = event.getSceneX();
                    yOffset = event.getSceneY();
                });

                layout.setOnMouseDragged(event -> {
                    stage.setX(event.getScreenX() - xOffset);
                    stage.setY(event.getScreenY() - yOffset);
                });

                CreateEventController createEventController = loader.getController();
                createEventController.setService(eventsService,user,stage);


                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Error: + " + e.getMessage());
            }



    }

    public void handleOnNotification(MouseEvent mouseEvent) {
        showEventStarts();
    }
}
