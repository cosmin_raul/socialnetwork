package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.MouseEvent;
import socialnetwork.domain.MessageDto;
import socialnetwork.domain.User;
import socialnetwork.service.UserService;

import java.net.URL;
import java.util.Collection;
import java.util.ResourceBundle;
import java.util.stream.StreamSupport;

public class PageableTestController implements Initializable {

    public UserService service;
    public TableColumn<User,Long> idColumn;
    public TableColumn<User,String> firstNameCol;
    public TableColumn<User,String> lastNameCol;
    public TableColumn<User,String> emailCol;
    public TableView<User> tableUser;

    public TextField page;

    private int totalPages;


    private ObservableList<User> modelView = FXCollections.observableArrayList();

    public void setService(UserService service){
        this.service = service;
        initModel(1);
        totalPages = service.getNrOfPages();

        page.setText(1+"/" + totalPages);



    }

    private void initModel(){
       modelView.setAll((Collection<? extends User>) service.findAll());
    }

    private void initModel(int pageNr){
        modelView.setAll((Collection<? extends User>) service.getPagingUsers(pageNr));
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        idColumn.setCellValueFactory(new PropertyValueFactory<User, Long>("id"));
        firstNameCol.setCellValueFactory(new PropertyValueFactory<User, String>("firstName"));
        lastNameCol.setCellValueFactory(new PropertyValueFactory<User, String>("lastName"));
        emailCol.setCellValueFactory(new PropertyValueFactory<User, String>("email"));
        tableUser.setItems(modelView);

    }


    public void handleOnPrevious(ActionEvent actionEvent) {

        String pages = page.getText();
        String rez[] = pages.split("/");

        int pageNr = Integer.parseInt(rez[0]);

        if( pageNr == 1 )
            MessageAlert.showErrorMessage(null,"This is first page!");
        else {

            initModel(pageNr - 1 );
            page.setText((pageNr - 1)+ "/" + totalPages);


        }
    }

    public void handleOnNext(ActionEvent actionEvent) {
        String pages = page.getText();
        String rez[] = pages.split("/");

        int pageNr = Integer.parseInt(rez[0]);

        if(pageNr == totalPages)
            MessageAlert.showErrorMessage(null,"This is last page!");
        else {
            initModel(pageNr + 1);
            page.setText((pageNr + 1) + "/" + totalPages);
        }
    }
}
