package socialnetwork.controller;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

import java.util.Optional;

public class MessageAlert {
    static boolean showMessageConfirmation( String header){
        Alert message=new Alert(Alert.AlertType.CONFIRMATION);
        message.setHeaderText(header);

        message.initOwner(null);
        message.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        Optional<ButtonType> result =  message.showAndWait();

        if(result.get().getText().equals("OK")) {
           return true;
        }
      return false;
    }

    static void showMessage(Stage owner, Alert.AlertType type, String header, String text){
        Alert message=new Alert(type);
        message.setHeaderText(header);
        message.setContentText(text);
        message.initOwner(owner);
        message.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        message.showAndWait();
    }

    static void showErrorMessage(Stage owner, String text){


        Alert message=new Alert(Alert.AlertType.ERROR);
        message.setHeaderText(null);
        message.initOwner(owner);
        message.setTitle("Error message");
        message.setContentText(text);
        message.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        message.showAndWait();
    }
}

