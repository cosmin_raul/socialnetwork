package socialnetwork.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import socialnetwork.domain.Password;
import socialnetwork.domain.User;
import socialnetwork.exceptions.IdentificationException;
import socialnetwork.exceptions.UserException;
import socialnetwork.service.*;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;


enum LABEL{
    FIRST,LAST,EMAIL,PASSWORD
}



public class RootSignPartController implements Initializable {

    private double xOffset,yOffset;

   //sing in
    public AnchorPane signInAnchor;

    public TextField textEmail;
    public PasswordField textPassword;

    public Label errorTextSignIn;


    //----------------------------

    //sing up

    public AnchorPane signUpAnchor;

    public Label labelErrorLastName;
    public Label labelErrorEmail;
    public Label labelErrorPassword;
    public Label labelErrorFirstName;

    public TextField textFirstNameCreate;
    public TextField textLastNameCreate;
    public TextField textEmailCreate;
    public PasswordField textPasswordCreate;
    public Label labelCreateAccount;


    //---------------------------
    private Stage stage;
    private UserService userService;
    private AdminService adminService;
    private FriendShipService friendShipService;
    private MessageService messageService;
    private EventsService eventsService;


    private void clearTextFieldsSignIn(){

            textEmail.clear();
            textPassword.clear();
    }

    private void clearLabelErrorSingIn(){
        errorTextSignIn.setText("");
    }

    private void clearLabelErrorSingUp(){

        labelErrorFirstName.setText("");
        labelErrorLastName.setText("");
        labelErrorEmail.setText("");
        labelErrorPassword.setText("");
        labelCreateAccount.setText("");

    }

    private void clearTextFieldsSignUp(){
            textFirstNameCreate.clear();
            textLastNameCreate.clear();
            textEmailCreate.clear();
            textPasswordCreate.clear();
    }

    public void setService(UserService userService, AdminService adminService,FriendShipService friendShipService,MessageService messageService,EventsService eventsService, Stage stage){
        this.userService = userService;
        this.adminService = adminService;
        this.eventsService = eventsService;
        this.stage = stage;
        this.friendShipService = friendShipService;
        this.messageService = messageService;

    }


    public void handleBtnSignIn() {

        String email = textEmail.getText();
        String password = textPassword.getText();

        if(email.isEmpty() || password.isEmpty() )
            errorTextSignIn.setText("Email and password can't be empty!");
        else{

            try {
                clearLabelErrorSingIn();
                clearTextFieldsSignIn();

                User user = userService.signIn(email, password);
                FXMLLoader loader  = new FXMLLoader();
                loader.setLocation(getClass().getResource("/view/mainInterface.fxml"));

                AnchorPane root = loader.load();
                Stage dialogStage = new Stage();

                root.setOnMousePressed(event -> {
                    xOffset = event.getSceneX();
                    yOffset = event.getSceneY();

                });

                root.setOnMouseDragged(event -> {
                    dialogStage.setX(event.getScreenX() - xOffset);
                    dialogStage.setY(event.getScreenY() - yOffset);

                });

                dialogStage.initStyle(StageStyle.TRANSPARENT);
                Scene scene = new Scene(root);
                scene.setFill(Color.TRANSPARENT);
                dialogStage.setScene(scene);
                MainInterfaceController mainInterfaceController = loader.getController();
                mainInterfaceController.setService(userService,friendShipService,messageService,eventsService,dialogStage,this.stage,user);
                dialogStage.show();
                this.stage.close();

            }
            catch (IdentificationException exp){
                errorTextSignIn.setText(exp.getMessage());
            }
            catch (IOException e) {
                e.printStackTrace();
            }


        }


    }


    public void handleClose() {stage.close();}
    public void handleMinimize() {
        stage.setIconified(true);
    }

    public void handleBtnSignUp() {
        signInAnchor.toBack();
        clearTextFieldsSignIn();
        clearLabelErrorSingIn();
    }


    private LABEL findLabelError(String error) {
        switch (error) {
            case "First name is invalid!":
                return LABEL.FIRST;
            case "Last name is invalid!":
                return LABEL.LAST;
            case "Email is invalid(format eg: name@addres.domain)!":
                return LABEL.EMAIL;
            case "Password is to short!":
                return LABEL.PASSWORD;

        }
        return null;
    }

    public void handleCreateProfile() {
        clearLabelErrorSingUp();

        String firstName = textFirstNameCreate.getText();
        String lastName = textLastNameCreate.getText();
        String email = textEmailCreate.getText();
        String password_string = textPasswordCreate.getText();
        Password password = new Password();

        password.setPassword_String(password_string);


        try{
            User user = new User(firstName,lastName,email,password);
            User result = adminService.addUser(user);

            if(result != null) {
                clearLabelErrorSingUp();
                clearTextFieldsSignUp();

                labelErrorEmail.setText("Email is already taken");
            }
            else
                labelCreateAccount.setText("Your account is created!");


        }catch (UserException | IllegalArgumentException exp) {

            List<String> errors = Arrays.asList(exp.getMessage().split("\\n"));

            errors.forEach(error->{
                switch (findLabelError(error))
                {
                    case FIRST:
                         labelErrorFirstName.setText(error);
                         break;

                    case LAST:
                        labelErrorLastName.setText(error);
                        break;

                    case EMAIL:
                        labelErrorEmail.setText(error);
                        break;

                    case PASSWORD:
                        labelErrorPassword.setText(error);
                        break;

                    default:
                        System.out.println("INVALID MESSAGE ERROR");
                }

            });





        }

    }


    public void handleBtnSignInChange() {
        signUpAnchor.toBack();
        clearTextFieldsSignUp();
        clearLabelErrorSingUp();

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        signUpAnchor.toBack();
    }


    public void setService(UserService userService, AdminService adminService, FriendShipService friendShipService, MessageService messageService, Stage dialogStage) {
    }
}
