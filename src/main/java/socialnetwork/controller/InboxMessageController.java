package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import socialnetwork.domain.MessageDto;
import socialnetwork.domain.User;
import socialnetwork.exceptions.FriendRequestException;
import socialnetwork.exceptions.FriendshipException;
import socialnetwork.service.MessageService;
import socialnetwork.utils.observer.Observer;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

public class InboxMessageController implements Observer {

    public TableColumn<MessageDto, Long> tableColumnID;
    public TableColumn<MessageDto, String> tableColumnEmail;
    public TableColumn<MessageDto,String> tableColumnDate;
    public TableColumn<MessageDto, String> tableColumnMessage;
    public TableColumn<MessageDto, Boolean> tableColumnStatus;

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

    public TableView<MessageDto> tableMessages;
    public TextField textFieldSearchMessage;
    public TextField textFieldCurrentPage;
    public TextField textFieldPages;

    private AnchorPane rootAnchorPane;
    private MessageService messageService;
    private User user;

    private int maxPage;

    public void setService(MessageService messageService, User user,AnchorPane rootAnchorPane){

        this.messageService = messageService;
        this.user = user;
        this.rootAnchorPane = rootAnchorPane;

        this.messageService.addObserver(this);

        maxPage = messageService.getInboxSize(user,"");
        textFieldCurrentPage.setText("1");
        textFieldPages.setText("/"+maxPage);

        initModel(1,"");


    }

    private ObservableList<MessageDto> modelView = FXCollections.observableArrayList();


    @FXML
    public void initialize(){

        tableColumnID.setCellValueFactory(new PropertyValueFactory<MessageDto,Long>("idMessage"));
        tableColumnEmail.setCellValueFactory(new PropertyValueFactory<MessageDto,String>("emailFrom"));
        tableColumnDate.setCellValueFactory(new PropertyValueFactory<MessageDto,String>("localDateTime"));
        tableColumnMessage.setCellValueFactory(new PropertyValueFactory<MessageDto,String>("message"));
        tableColumnStatus.setCellValueFactory(new PropertyValueFactory<MessageDto,Boolean>("reply"));

        tableMessages.setItems(modelView);

        //textFieldSearchMessage.textProperty().addListener(f->handleFindMessage());

    }

   /* private void handleFindMessage() {

        modelView.setAll(getMessageDto()
                .stream()
                .filter(msg->msg.getEmailFrom().startsWith(textFieldSearchMessage.getText()))
                .collect(Collectors.toList())

        );

    }*/

    private void initModel(int page, String full_name){
        modelView.setAll(getMessageDto(page,full_name));

    }

    public List<MessageDto> getMessageDto(int page,String full_name){
        return messageService.getInboxPageable(user,page,full_name)
                .stream()
                .map(message -> new MessageDto(
                        message.getId(),
                        message.getUserFrom().getEmail(),
                        message.getDateTime().format(formatter),
                        message.getMessage(),
                        message.getStatus()
                ))
                .collect(Collectors.toList());


    }

    @Override
    public void update() {
        textFieldCurrentPage.setText("1");
        initModel(1,"");
        maxPage = messageService.getInboxSize(user,"");
        textFieldPages.setText("/"+maxPage);

    }


    public void handleOnClickReply(MouseEvent mouseEvent) {
       MessageDto messageDto = tableMessages.getSelectionModel().getSelectedItem();

        if(messageDto != null)
        {
            try{
                FXMLLoader loader  = new FXMLLoader();
                loader.setLocation(getClass().getResource("/view/replyMessageView.fxml"));
                try {

                    rootAnchorPane.getChildren().setAll((Node) loader.load());

                }
                catch (IOException e) {
                    e.printStackTrace();
                }

                ReplyMessageView replyMessageView = loader.getController();
                replyMessageView.setService(messageService,messageDto,user,rootAnchorPane);

             //   MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,null,"Good");

            }
            catch (FriendshipException | IllegalArgumentException  | FriendRequestException exp) {
                MessageAlert.showErrorMessage(null, exp.getMessage());
            }

        }
        else
            MessageAlert.showErrorMessage(null,"Nothing was selected from the table !");


    }




    public void handleOnPrevious(ActionEvent actionEvent) {
        int pageNr = 0;
        try {
            pageNr = Integer.parseInt(textFieldCurrentPage.getText());
        }
        catch (NumberFormatException exp)
        {
            MessageAlert.showErrorMessage(null,"Current page is invalid!");
            return;
        }
        if(pageNr == 1 ) {
            MessageAlert.showErrorMessage(null, "This is first page!");
            return;

        }
        if (pageNr < 1 || pageNr > maxPage)
            MessageAlert.showErrorMessage(null, "Invalid page number!");
        else
        {
            String full_name = textFieldSearchMessage.getText();

            initModel(pageNr - 1,full_name);

            textFieldCurrentPage.setText(String.valueOf(pageNr - 1));
        }

    }


    public void handleOnNext(ActionEvent actionEvent) {
        int pageNr = 0;
        try {
            pageNr = Integer.parseInt(textFieldCurrentPage.getText());
        }
        catch (NumberFormatException exp)
        {
            MessageAlert.showErrorMessage(null,"Current page is invalid!");
            return;
        }
        if(pageNr == maxPage ) {
            MessageAlert.showErrorMessage(null, "This is last page!");
            return;
        }
        if (pageNr < 1 || pageNr > maxPage)
            MessageAlert.showErrorMessage(null, "Invalid page number!");
        else
        {
            String full_name = textFieldSearchMessage.getText();

            initModel(pageNr +1 ,full_name);

            textFieldCurrentPage.setText(String.valueOf(pageNr + 1));
        }
    }

    public void handleOnEnter(KeyEvent keyEvent) {
        if(keyEvent.getCode().equals(KeyCode.ENTER)) {
            try {
                int pageNr = Integer.parseInt(textFieldCurrentPage.getText());
                if (pageNr < 1 || pageNr > maxPage)
                    MessageAlert.showErrorMessage(null, "Invalid page number!");
                else {
                    initModel(pageNr,textFieldSearchMessage.getText());
                }

            } catch (NumberFormatException exp) {
                MessageAlert.showErrorMessage(null, "Invalid page format!");
            }
        }
    }

    public void handleOnMessageSearch(MouseEvent mouseEvent) {

        String search_full_name = textFieldSearchMessage.getText();

        initModel(1, search_full_name);

        textFieldCurrentPage.setText("1");
        maxPage = messageService.getInboxSize(user, search_full_name);
        textFieldPages.setText("/" + maxPage);
    }
    public void handleEnterSearch(KeyEvent actionEvent) {
        if(actionEvent.getCode().equals(KeyCode.ENTER)) {
            String search_full_name = textFieldSearchMessage.getText();

            initModel(1, search_full_name);

            textFieldCurrentPage.setText("1");
            maxPage = messageService.getInboxSize(user, search_full_name);
            textFieldPages.setText("/" + maxPage);
        }
    }

}
