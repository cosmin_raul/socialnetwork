package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import socialnetwork.domain.FriendShipDto;
import socialnetwork.domain.User;
import socialnetwork.service.FriendShipService;
import socialnetwork.utils.observer.Observer;

import java.time.LocalDate;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FriendsController implements Observer {


    public TextField textFieldPages;
    public TextField textFieldCurrentPage;
    private FriendShipService friendShipService;
    private User user;

    private int maxPage;
    public TextField textFieldSearchFriend;

    public TableView<FriendShipDto> tableFriends;
    public TableColumn<FriendShipDto, Long> tableColumnID;
    public TableColumn<FriendShipDto, String> tableColumnFirstName;
    public TableColumn<FriendShipDto, String> tableColumnLastName;
    public TableColumn<FriendShipDto, String> tableColumnEmail;
    public TableColumn<FriendShipDto,LocalDate> tableColumnFriendShipDate;

    private ObservableList<FriendShipDto> modelFriendShipDtos = FXCollections.observableArrayList();



    @FXML
    public void initialize(){


        tableColumnID.setCellValueFactory(new PropertyValueFactory<FriendShipDto,Long>("id"));
        tableColumnFirstName.setCellValueFactory(new PropertyValueFactory<FriendShipDto,String>("firstName"));
        tableColumnLastName.setCellValueFactory(new PropertyValueFactory<FriendShipDto,String>("lastName"));
        tableColumnEmail.setCellValueFactory(new PropertyValueFactory<FriendShipDto,String>("email"));
        tableColumnFriendShipDate.setCellValueFactory(new PropertyValueFactory<FriendShipDto,LocalDate>("localDate"));
        tableFriends.setItems(modelFriendShipDtos);


      //  textFieldSearchFriend.textProperty().addListener(x->handleFilter());

    }

    private void handleFilter() {

        Predicate<FriendShipDto> byFullName = f->f.getFullName().contains(textFieldSearchFriend.getText());
        modelFriendShipDtos.setAll(friendShipService.getFriends(user)
                .stream()
                .filter(byFullName)
                .collect(Collectors.toList())
        );


    }


    private void initModel(int page, String full_name) {
        modelFriendShipDtos.setAll(friendShipService.getFriendsPageable(user,page,full_name));

    }


    public void setService(FriendShipService friendShipService,User user){
        this.friendShipService = friendShipService;
        this.user = user;
        friendShipService.addObserver(this);
        initModel(1,textFieldSearchFriend.getText());

        maxPage = friendShipService.getNrOfPagesFriends(user,textFieldSearchFriend.getText());
        textFieldCurrentPage.setText("1");
        textFieldPages.setText("/"+maxPage);


    }



    public void handleClickOnRemoveFriend(MouseEvent mouseEvent) {

        FriendShipDto friendShipDto = tableFriends.getSelectionModel().getSelectedItem();


            if (friendShipDto != null) {
                if(MessageAlert.showMessageConfirmation("Do you want to remove " + friendShipDto.getFullName() + " from your friend list? " )) {

                    if (friendShipService.removeFriendShip(user.getId(), friendShipDto.getId()) != null)
                            MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, null, "Friend was removed from your friendship list!");
                        else
                            MessageAlert.showErrorMessage(null, "That friend could not be removed!");
                }
            } else
                MessageAlert.showErrorMessage(null, "Nothing from table was selected!");


    }

    @Override
    public void update() {

        initModel(1,textFieldSearchFriend.getText());
        textFieldCurrentPage.setText("1");
        maxPage = friendShipService.getNrOfPagesFriends(user,textFieldSearchFriend.getText());
        textFieldPages.setText("/"+maxPage);
    }

    public void handleOnPrevious(ActionEvent actionEvent) {
        int pageNr = 0;
        try {
            pageNr = Integer.parseInt(textFieldCurrentPage.getText());
        }
        catch (NumberFormatException exp)
        {
            MessageAlert.showErrorMessage(null,"Current page is invalid!");
            return;
        }
        if(pageNr == 1 ) {
            MessageAlert.showErrorMessage(null, "This is first page!");
            return;

        }
        if (pageNr < 1 || pageNr > maxPage)
            MessageAlert.showErrorMessage(null, "Invalid page number!");
        else
        {
            String full_name = textFieldSearchFriend.getText();

            initModel(pageNr - 1,full_name);

            textFieldCurrentPage.setText(String.valueOf(pageNr - 1));
        }

    }

    public void handleOnEnter(KeyEvent keyEvent) {
        if(keyEvent.getCode().equals(KeyCode.ENTER)) {
            try {
                int pageNr = Integer.parseInt(textFieldCurrentPage.getText());
                if (pageNr < 1 || pageNr > maxPage)
                    MessageAlert.showErrorMessage(null, "Invalid page number!");
                else {
                    initModel(pageNr,textFieldSearchFriend.getText());
                }

            } catch (NumberFormatException exp) {
                MessageAlert.showErrorMessage(null, "Invalid page format!");
            }
        }
    }

    public void handleOnNext(ActionEvent actionEvent) {
        int pageNr = 0;
        try {
            pageNr = Integer.parseInt(textFieldCurrentPage.getText());
        }
        catch (NumberFormatException exp)
        {
            MessageAlert.showErrorMessage(null,"Current page is invalid!");
            return;
        }
        if(pageNr == maxPage ) {
            MessageAlert.showErrorMessage(null, "This is last page!");
            return;
        }
        if (pageNr < 1 || pageNr > maxPage)
            MessageAlert.showErrorMessage(null, "Invalid page number!");
        else
        {
            String full_name = textFieldSearchFriend.getText();

            initModel(pageNr +1 ,full_name);

            textFieldCurrentPage.setText(String.valueOf(pageNr + 1));
        }
    }

    public void handleOnEnterPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode().equals(KeyCode.ENTER)){
            String search_full_name = textFieldSearchFriend.getText();

            initModel(1,search_full_name);

            textFieldCurrentPage.setText("1");
            maxPage = friendShipService.getNrOfPagesFriends(user,search_full_name);
            textFieldPages.setText("/"+maxPage);

        }
    }

    public void handleOnSearch(MouseEvent mouseEvent) {
        String search_full_name = textFieldSearchFriend.getText();

        initModel(1,search_full_name);

        textFieldCurrentPage.setText("1");
        maxPage = friendShipService.getNrOfPagesFriends(user,search_full_name);
        textFieldPages.setText("/"+maxPage);
    }
}
