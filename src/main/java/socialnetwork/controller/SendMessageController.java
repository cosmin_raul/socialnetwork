package socialnetwork.controller;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import socialnetwork.domain.User;
import socialnetwork.service.MessageService;

import java.util.List;

public class SendMessageController {

    public Label ReceiversLabel;
    public TextArea MessageData;
    private MessageService service;
    private User user;
    private List<String> mails;
    private List<Long> IDs;
    private Stage stage;
    public void setService(MessageService service, User user,
                           List<String> mails, List<Long> IDs, Stage stage){
        this.service=service;
        this.user=user;
        this.mails = mails;
        this.IDs = IDs;
        this.stage = stage;
        setReceiversLabel();
    }

    public void setReceiversLabel(){
        ReceiversLabel.setText("");
        mails.forEach(x-> ReceiversLabel.setText(ReceiversLabel.getText() + x + ", "));
        ReceiversLabel.setText(ReceiversLabel.getText().substring(0,ReceiversLabel.getText().length()-2));
    }

    public void sendMessage(){
        try {
            service.sendMessage(this.user,IDs,MessageData.getText());
            MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,"Message sent","Message was successfully sent!");
        }catch (Exception e){
            MessageAlert.showErrorMessage(null,e.getMessage());
        }

    }



    public void cancel(){
//        FXMLLoader loader  = new FXMLLoader();
//        loader.setLocation(getClass().getResource("/view/inboxMessageView.fxml"));
//        try {
//            rootAnchorPane.getChildren().setAll((Node) loader.load());
//        }
//        catch (IOException e) {
//            e.printStackTrace();
//        }
//        ComposeMessageController controller = loader.getController();
//        controller.setService(service,friendShipService,user,rootAnchorPane);
        this.stage.close();
    }

    public void handleOnClickMinimize(MouseEvent mouseEvent) {
        stage.setIconified(true);
    }

    public void handleOnClickClose(MouseEvent mouseEvent) {
        this.stage.close();
    }
}
