package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import socialnetwork.domain.User;
import socialnetwork.service.EventsService;
import socialnetwork.service.FriendShipService;
import socialnetwork.service.MessageService;
import socialnetwork.service.UserService;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainInterfaceController implements Initializable {

    @FXML
    public ImageView imageViewUser;
    public AnchorPane rootAnchorPane;
    public Label labelId;
    public Label labelEmail;
    public AnchorPane mainAnchor;

    private EventsService eventsService;
    private MessageService messageService;
    private FriendShipService friendShipService;
    private UserService userService;
    private Stage dialogStage;
    private Stage signInStage;

    private User user;

    private final ObservableList<User> model_users = FXCollections.observableArrayList();

    public Label labelTopTitle;
    public Label userLabelName;


    public void handleClickOnUsers(MouseEvent mouseEvent) {

        FXMLLoader loader  = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/findFirendsView.fxml"));

        try {
            rootAnchorPane.getChildren().setAll((Node) loader.load());
        } catch (IOException e) {
            e.printStackTrace();
        }

        FindFriendsController findFriendsController = loader.getController();
        findFriendsController.setService(friendShipService,user);

        labelTopTitle.setText("Users");


    }

    public void handleClickOnFriends(MouseEvent mouseEvent) {

        FXMLLoader loader  = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/friendsView.fxml"));

        try {

            rootAnchorPane.getChildren().setAll((Node) loader.load());

        }
        catch (IOException e) {
            e.printStackTrace();
        }

        FriendsController friendsController = loader.getController();
        friendsController.setService(friendShipService,user);

        labelTopTitle.setText("Friends");

    }

    public void handleClickOnFriendRequest(MouseEvent mouseEvent) {
        FXMLLoader loader  = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/friendRequestView.fxml"));

        try {

            rootAnchorPane.getChildren().setAll((Node) loader.load());

        }
        catch (IOException e) {
            e.printStackTrace();
        }

        FriendRequestController friendRequestController = loader.getController();
        friendRequestController.setService(friendShipService,user, labelTopTitle);

        labelTopTitle.setText("Friend Requests: Receive");

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setService(UserService userService, FriendShipService friendShipService, MessageService messageService , EventsService eventsService, Stage dialogStage, Stage signInStage, User user) {
        this.userService = userService;
        this.messageService = messageService;
        this.friendShipService = friendShipService;
        this.eventsService = eventsService;

        this.dialogStage = dialogStage;
        this.signInStage = signInStage;
        this.user = user;

        userLabelName.setText(user.getFirstName() + " " + user.getLastName());
        labelId.setText("ID: "+ user.getId());
        labelEmail.setText("Email: "+ user.getEmail());

        String pathImage = userService.findImageFor(user);

        if(!pathImage.equals("")) {
            File tempFile = new File(pathImage);
            boolean exists = tempFile.exists();

            if (!exists) {
                MessageAlert.showErrorMessage(null, "Your image profile doesn't exist!");
            } else
                try {
                    imageViewUser.setImage(new Image(new FileInputStream(pathImage)));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
        }
        handleClickOnMyPage();

    }


    public void handleClickOnSignOut(MouseEvent mouseEvent) {
        dialogStage.close();
        signInStage.show();
    }

    public void handleOnMinimize(MouseEvent mouseEvent) {
        dialogStage.setIconified(true);
    }

    public void handleOnClose(MouseEvent mouseEvent) {
        dialogStage.close();
    }



    public void handleClickOnInbox(MouseEvent mouseEvent) {

        labelTopTitle.setText("Inbox");

        FXMLLoader loader  = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/inboxMessageView.fxml"));
        try {
            rootAnchorPane.getChildren().setAll((Node) loader.load());
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        InboxMessageController friendsController = loader.getController();
        friendsController.setService(messageService,user,rootAnchorPane);


    }

    public void handleClickOnComposeMessage(MouseEvent mouseEvent) {
        labelTopTitle.setText("Compose message");

        FXMLLoader loader  = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/composeMessageView.fxml"));
        try {
            rootAnchorPane.getChildren().setAll((Node) loader.load());
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        ComposeMessageController composeMessageController = loader.getController();
        composeMessageController.setService(messageService,friendShipService,user,rootAnchorPane);
    }

    public void handleClickOnReports(MouseEvent mouseEvent) {
        labelTopTitle.setText("Reports");

        FXMLLoader loader  = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/reportFriendsMessages.fxml"));
        try {
            rootAnchorPane.getChildren().setAll((Node) loader.load());
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        ReportFriendsMessagesController reportFriendsMessagesController = loader.getController();

        reportFriendsMessagesController.setService(friendShipService,messageService,user);

    }

    public void handleClickOnMyPage() {
        labelTopTitle.setText("My Page: Events");
        FXMLLoader loader  = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/myPageView.fxml"));
        try {
            rootAnchorPane.getChildren().setAll((Node) loader.load());
        }
        catch (IOException e) {
            e.printStackTrace();
        }

       MyPageController myPageController = loader.getController();
        myPageController.setService(eventsService,user);

    }

    public void handleOnSettings(MouseEvent mouseEvent) {
        labelTopTitle.setText("Settings");
        FXMLLoader loader  = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/settingsView.fxml"));
        try {
            rootAnchorPane.getChildren().setAll((Node) loader.load());
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        SettingsController settingsController = loader.getController();
        settingsController.setService(userService,user,imageViewUser,userLabelName);

    }
}
