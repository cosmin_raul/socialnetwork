package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import socialnetwork.domain.FriendRequestDto;
import socialnetwork.domain.User;
import socialnetwork.exceptions.FriendRequestException;
import socialnetwork.exceptions.FriendshipException;
import socialnetwork.service.FriendShipService;
import socialnetwork.utils.observer.Observer;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

enum REQUEST{
    RECEIVE,SENT
}
public class FriendRequestController implements Observer {


    int maxPagesReceive;
    int maxPagesSent;

    public TextField textFieldCurrentPage;
    public TextField textFieldPages;
    REQUEST requestType;

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
    public TableView<FriendRequestDto> tableFriendRequest;
    public TableColumn<FriendRequestDto,String> tableColumnFirstName;
    public TableColumn<FriendRequestDto,Long> tableColumnID;
    public TableColumn<FriendRequestDto,String> tableColumnLastName;
    public TableColumn<FriendRequestDto,String> tableColumnEmail;
    public TableColumn<FriendRequestDto, String> tableColumnDate;
    public TableColumn<FriendRequestDto,String> tableColumnStatus;


    private Label labelTopTitle;

    public AnchorPane idAnchorRequestSent;
    public TextField textFieldSearchRequestSent;

    public AnchorPane idAnchorRequestReceive;
    public TextField textFieldSearchRequestsReceive;

    public MenuButton idMenuButton;


    private FriendShipService friendShipService;
    private User user;

    private ObservableList<FriendRequestDto> modelView = FXCollections.observableArrayList();

    @FXML

    public void initialize(){

        tableColumnID.setCellValueFactory(new PropertyValueFactory<FriendRequestDto,Long>("id"));
        tableColumnFirstName.setCellValueFactory(new PropertyValueFactory<FriendRequestDto,String>("firstName"));
        tableColumnLastName.setCellValueFactory(new PropertyValueFactory<FriendRequestDto,String>("lastName"));
        tableColumnEmail.setCellValueFactory(new PropertyValueFactory<FriendRequestDto,String>("email"));
        tableColumnDate.setCellValueFactory(new PropertyValueFactory<FriendRequestDto,String>("localDateTime"));
        tableColumnStatus.setCellValueFactory(new PropertyValueFactory<FriendRequestDto,String>("status"));

        tableFriendRequest.setItems(modelView);

       // textFieldSearchRequestsReceive.textProperty().addListener(x->handleFilterRequestReceive());

       // textFieldSearchRequestSent.textProperty().addListener(x->handleFilterRequestSent());

    }

    public void setService(FriendShipService friendShipService, User user,Label labelTopTitle) {
        this.friendShipService = friendShipService;
        this.user = user;
        this.labelTopTitle = labelTopTitle;

        this.labelTopTitle.setText("Friend Requests: Receive");

        friendShipService.addObserver(this);

        requestType=REQUEST.RECEIVE;

        idAnchorRequestReceive.toFront();


        maxPagesReceive = friendShipService.getNrOfPagesFriendRequestReceive(user,"");
        textFieldCurrentPage.setText("1");
        textFieldPages.setText("/"+maxPagesReceive);
        initModelForRequestReceive(1,"");
    }


    private void initModelForRequestReceive(int page, String full_name) {
        modelView.setAll(getFriendRequestReceiveDto(page,full_name));

    }

    private void initModelForRequestSent(int page, String full_name) {
        modelView.setAll(getFriendRequestSentDto(page,full_name));

    }

  /*  private void handleFilterRequestReceive(){

        Predicate<FriendRequestDto> byFullRequest = f->f.getFullStringRequest().contains(textFieldSearchRequestsReceive.getText());
        modelView.setAll(getFriendRequestReceiveDto()
                .stream()
                .filter(byFullRequest)
                .collect(Collectors.toList())

        );

    }*/

  /*  private void handleFilterRequestSent(){

        Predicate<FriendRequestDto> byFullRequest = f->f.getFullStringRequest().contains(textFieldSearchRequestSent.getText());
        modelView.setAll(getFriendRequestSentDto()
                .stream()
                .filter(byFullRequest)
                .collect(Collectors.toList())

        );

    }*/

    private List<FriendRequestDto> getFriendRequestSentDto(int page,String full_name){

        return  friendShipService.findFriendRequestSentPaginate(user,page,full_name)
                .stream()
                .map(f-> new FriendRequestDto(f.getIdTo(),
                        f.getUserTo().getFirstName(),
                        f.getUserTo().getLastName(),
                        f.getUserTo().getEmail(),
                        f.getLocalDateTime().format(formatter),
                        f.getStatus()))
                .collect(Collectors.toList());

    }




    private List<FriendRequestDto> getFriendRequestReceiveDto(int page,String full_name){

       return  friendShipService.findFriendRequestReceivePaginate(user,page,full_name)
               .stream()
               .map(f-> new FriendRequestDto(f.getIdFrom(),
                       f.getUserFrom().getFirstName(),
                       f.getUserFrom().getLastName(),
                       f.getUserFrom().getEmail(),
                       f.getLocalDateTime().format(formatter),
                       f.getStatus()))
               .collect(Collectors.toList());
    }

    public void handleClickOnAcceptUser(MouseEvent mouseEvent) {
        FriendRequestDto friendRequestDto = tableFriendRequest.getSelectionModel().getSelectedItem();

        if(friendRequestDto != null)
        {
           try{
               friendShipService.answerFriendRequest(user,friendRequestDto.getId(),"approved");

               MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,null,"Friendship was create!");

           }
           catch (FriendshipException  | IllegalArgumentException  | FriendRequestException exp)
           {
               MessageAlert.showErrorMessage(null,exp.getMessage());
           }

        }
        else
            MessageAlert.showErrorMessage(null,"Nothing form table was selected!");



    }

    public void handleClickOnRejectUser(MouseEvent mouseEvent) {
        FriendRequestDto friendRequestDto = tableFriendRequest.getSelectionModel().getSelectedItem();



        if(friendRequestDto != null)
        {
            if(MessageAlert.showMessageConfirmation("Do you want to reject friend request from " + friendRequestDto.getFirstName() + " " +friendRequestDto.getLastName() )) {

                try {
                    friendShipService.answerFriendRequest(user, friendRequestDto.getId(), "rejected");

                    MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, null, "Your reject has been send!");

                } catch (FriendshipException | IllegalArgumentException | FriendRequestException exp) {
                    MessageAlert.showErrorMessage(null, exp.getMessage());
                }
            }
        }
        else
            MessageAlert.showErrorMessage(null,"Nothing form table was selected!");


    }



    @Override
    public void update() {

        if(requestType == REQUEST.RECEIVE) {

            initModelForRequestReceive(1,"");
            textFieldCurrentPage.setText("1");
            maxPagesReceive = friendShipService.getNrOfPagesFriendRequestReceive(user,textFieldSearchRequestsReceive.getText());
            textFieldPages.setText("/"+maxPagesReceive);

        }
        else {

            initModelForRequestSent(1,"");
            textFieldCurrentPage.setText("1");
            maxPagesSent = friendShipService.getNrOfPagesFriendRequestSent(user,textFieldSearchRequestSent.getText());
            textFieldPages.setText("/"+maxPagesSent);
        }


    }

    public void handleOnRequestSent(ActionEvent actionEvent) {


        labelTopTitle.setText("Friend Requests: Sent");
        requestType = REQUEST.SENT;

        idAnchorRequestSent.toFront();

        initModelForRequestSent(1,"");
        textFieldCurrentPage.setText("1");
        maxPagesSent = friendShipService.getNrOfPagesFriendRequestSent(user,textFieldSearchRequestSent.getText());
        textFieldPages.setText("/"+maxPagesSent);


    }

    public void handleOnRequestReceive(ActionEvent actionEvent) {

        labelTopTitle.setText("Friend Requests: Receive");
        requestType=REQUEST.RECEIVE;

        idAnchorRequestReceive.toFront();

        initModelForRequestReceive(1,"");
        textFieldCurrentPage.setText("1");
        maxPagesReceive = friendShipService.getNrOfPagesFriendRequestReceive(user,textFieldSearchRequestsReceive.getText());
        textFieldPages.setText("/"+maxPagesReceive);

    }

    public void handleClickRemoveRequestSent(MouseEvent mouseEvent) {

        FriendRequestDto friendRequestDto = tableFriendRequest.getSelectionModel().getSelectedItem();


            if (friendRequestDto != null) {

                if(MessageAlert.showMessageConfirmation("Do you want to remove friend request for " + friendRequestDto.getFirstName() + " " + friendRequestDto.getLastName()))
                    {
                        try {

                            textFieldSearchRequestSent.setText("");
                            friendShipService.removeFriendRequest(this.user.getId(), friendRequestDto.getId());


                            MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, null, "Your friend request was removed!");

                        } catch (FriendshipException | IllegalArgumentException | FriendRequestException exp) {
                            MessageAlert.showErrorMessage(null, exp.getMessage());
                        }
                    }

            } else
                MessageAlert.showErrorMessage(null, "Nothing form table was selected!");


    }



    public void handleOnPrevious(ActionEvent actionEvent) {

        if(requestType == REQUEST.RECEIVE) {
            int pageNr = 0;

            try {
                pageNr = Integer.parseInt(textFieldCurrentPage.getText());
            }
            catch (NumberFormatException exp)
            {
                MessageAlert.showErrorMessage(null,"Current page is invalid!");
                return;
            }
            if(pageNr == 1 ) {
                MessageAlert.showErrorMessage(null, "This is first page!");
                return;

            }
            if (pageNr < 1 || pageNr > maxPagesReceive)
                MessageAlert.showErrorMessage(null, "Invalid page number!");
            else
            {
                String full_name = textFieldSearchRequestsReceive.getText();

                initModelForRequestReceive(pageNr - 1,full_name);

                textFieldCurrentPage.setText(String.valueOf(pageNr - 1));
            }


        }
        else {
            int pageNr = 0;
            try {
                pageNr = Integer.parseInt(textFieldCurrentPage.getText());
            }
            catch (NumberFormatException exp)
            {
                MessageAlert.showErrorMessage(null,"Current page is invalid!");
                return;
            }
            if(pageNr == 1 ) {
                MessageAlert.showErrorMessage(null, "This is first page!");
                return;

            }
            if (pageNr < 1 || pageNr > maxPagesSent)
                MessageAlert.showErrorMessage(null, "Invalid page number!");
            else
            {
                String full_name = textFieldSearchRequestSent.getText();

                initModelForRequestSent(pageNr - 1,full_name);

                textFieldCurrentPage.setText(String.valueOf(pageNr - 1));
            }

        }
    }





    public void handleOnNext(ActionEvent actionEvent) {

        if(requestType == REQUEST.RECEIVE) {
            int pageNr = 0;
            try {
                pageNr = Integer.parseInt(textFieldCurrentPage.getText());
            } catch (NumberFormatException exp) {
                MessageAlert.showErrorMessage(null, "Current page is invalid!");
                return;
            }
            if (pageNr == maxPagesReceive) {
                MessageAlert.showErrorMessage(null, "This is last page!");
                return;
            }
            if (pageNr < 1 || pageNr > maxPagesReceive)
                MessageAlert.showErrorMessage(null, "Invalid page number!");
            else {
                String full_name = textFieldSearchRequestsReceive.getText();

                initModelForRequestReceive(pageNr + 1, full_name);

                textFieldCurrentPage.setText(String.valueOf(pageNr + 1));
            }
        }
        else
        {
            int pageNr = 0;
            try {
                pageNr = Integer.parseInt(textFieldCurrentPage.getText());
            } catch (NumberFormatException exp) {
                MessageAlert.showErrorMessage(null, "Current page is invalid!");
                return;
            }
            if (pageNr == maxPagesSent) {
                MessageAlert.showErrorMessage(null, "This is last page!");
                return;
            }
            if (pageNr < 1 || pageNr > maxPagesSent)
                MessageAlert.showErrorMessage(null, "Invalid page number!");
            else {
                String full_name = textFieldSearchRequestSent.getText();

                initModelForRequestSent(pageNr + 1, full_name);

                textFieldCurrentPage.setText(String.valueOf(pageNr + 1));
            }
        }
    }

    public void handleOnSearchRequestReceive(MouseEvent mouseEvent) {
        String search_full_name = textFieldSearchRequestsReceive.getText();

        initModelForRequestReceive(1,search_full_name);

        textFieldCurrentPage.setText("1");
        maxPagesReceive = friendShipService.getNrOfPagesFriendRequestReceive(user,search_full_name);
        textFieldPages.setText("/"+maxPagesReceive);

    }

    public void handleOnSearchRequestSent(MouseEvent mouseEvent) {

        String search_full_name = textFieldSearchRequestSent.getText();

        initModelForRequestSent(1,search_full_name);

        textFieldCurrentPage.setText("1");
        maxPagesSent = friendShipService.getNrOfPagesFriendRequestSent(user,search_full_name);
        textFieldPages.setText("/"+maxPagesSent);

    }

    public void handleOnEnterReceive(KeyEvent keyEvent) {
      if(keyEvent.getCode().equals(KeyCode.ENTER)) {
          String search_full_name = textFieldSearchRequestsReceive.getText();

          initModelForRequestReceive(1,search_full_name);

          textFieldCurrentPage.setText("1");
          maxPagesReceive = friendShipService.getNrOfPagesFriendRequestReceive(user,search_full_name);
          textFieldPages.setText("/"+maxPagesReceive);

      }
    }

    public void handleOnEnterSent(KeyEvent keyEvent) {

        if(keyEvent.getCode().equals(KeyCode.ENTER)) {
            String search_full_name = textFieldSearchRequestSent.getText();

            initModelForRequestSent(1,search_full_name);

            textFieldCurrentPage.setText("1");
            maxPagesSent = friendShipService.getNrOfPagesFriendRequestSent(user,search_full_name);
            textFieldPages.setText("/"+maxPagesSent);
        }
    }

    public void handleOnEnter(KeyEvent keyEvent) {
        if(requestType == REQUEST.RECEIVE) {
            if (keyEvent.getCode().equals(KeyCode.ENTER)) {
                try {
                    int pageNr = Integer.parseInt(textFieldCurrentPage.getText());
                    if (pageNr < 1 || pageNr > maxPagesReceive)
                        MessageAlert.showErrorMessage(null, "Invalid page number!");
                    else {
                        initModelForRequestReceive(pageNr, textFieldSearchRequestsReceive.getText());
                    }

                } catch (NumberFormatException exp) {
                    MessageAlert.showErrorMessage(null, "Invalid page format!");
                }
            }
        }
        else {
            if (keyEvent.getCode().equals(KeyCode.ENTER)) {
                try {
                    int pageNr = Integer.parseInt(textFieldCurrentPage.getText());
                    if (pageNr < 1 || pageNr > maxPagesSent)
                        MessageAlert.showErrorMessage(null, "Invalid page number!");
                    else {
                        initModelForRequestReceive(pageNr, textFieldSearchRequestSent.getText());
                    }

                } catch (NumberFormatException exp) {
                    MessageAlert.showErrorMessage(null, "Invalid page format!");
                }
            }

        }

    }

}
