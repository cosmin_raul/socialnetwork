package socialnetwork.controller;

import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTimePicker;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import socialnetwork.domain.Event;
import socialnetwork.domain.User;
import socialnetwork.exceptions.EventsException;
import socialnetwork.exceptions.FriendRequestException;
import socialnetwork.exceptions.FriendshipException;
import socialnetwork.service.EventsService;

import javax.swing.text.DateFormatter;
import java.io.IOException;
import java.text.DateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class CreateEventController {

 //   public JFXDatePicker dateField;
 //   public JFXTimePicker timeField;
    public TextField titleTextField;
    public DatePicker eventDate;
    public TextField eventTime;
    private Stage stage;
    private EventsService eventsService;
    private User user;


    private boolean checkTimeForEvent(String valueTime) {
        if(valueTime == null || valueTime.equals("") || valueTime.length() != 5)
            return false;

        try{
            int h1 = Integer.parseInt(String.valueOf(valueTime.charAt(0)));

            int h2 = Integer.parseInt(String.valueOf(valueTime.charAt(1)));

            if(h1 < 0 || h2 < 0 || h1 > 2 || h2 > 9)
                throw new NumberFormatException();

            if(h1 == 2 && h2 > 3)
                throw new NumberFormatException();

            if(!String.valueOf(valueTime.charAt(2)).equals(":"))
                throw new NumberFormatException();

            int m1 = Integer.parseInt(String.valueOf(valueTime.charAt(3)));
            int m2 = Integer.parseInt(String.valueOf(valueTime.charAt(4)));

            if( m1 < 0 || m2 < 0 || m1 > 5 || m2 > 9)
                throw new NumberFormatException();

        }
        catch(NumberFormatException exp){

            return false;
        }

        return true;

    }

    public void createEvent(ActionEvent actionEvent) {
     /* String valueTime = (timeField.getValue() != null ? timeField.getValue().toString() : "");
      String valueDate = (dateField.getValue() != null ? dateField.getValue().toString() : "");


      String title = titleTextField.getText();

      if(valueDate.equals("") || valueTime.equals("") || title.equals("")){
          MessageAlert.showErrorMessage(null,"Invalid input for event creation!");
          return;

      }*/

        String title = titleTextField.getText();
     String valueDate = eventDate.getValue().toString();

     String valueTime = eventTime.getText();

     if(!checkTimeForEvent(valueTime))
     {
         MessageAlert.showErrorMessage(null,"Invalid time format, correct format is: hh:mm");
         return;
     }



      String str = valueDate + " " + valueTime;


      DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
      LocalDateTime dateTime = LocalDateTime.parse(str, formatter);


      try{
          Event event1 = new Event(0l,user,dateTime,title,1l,true);
          eventsService.saveEvent(event1);
          MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,null,"Your event is created!");

      }
      catch (EventsException exception)
      {
          MessageAlert.showErrorMessage(null,exception.getMessage());
      }




    }


    public void cancel(ActionEvent actionEvent) {
        stage.close();
    }

    public void handleOnClickMinimize(MouseEvent mouseEvent) {
        stage.setIconified(true);
    }

    public void handleOnClickClose(MouseEvent mouseEvent) {
        stage.close();
    }

    public void setService(EventsService eventsService, User user,Stage stage) {
        this.eventsService = eventsService;
        this.user = user;
        this.stage = stage;
    }
}
