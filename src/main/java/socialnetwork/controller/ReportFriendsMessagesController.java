package socialnetwork.controller;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.*;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;
import socialnetwork.domain.FriendShipDto;
import socialnetwork.domain.MessageDto;
import socialnetwork.domain.User;
import socialnetwork.service.FriendShipService;
import socialnetwork.service.MessageService;

import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.List;
import java.util.stream.Stream;

public class ReportFriendsMessagesController {

    public BarChart<String,Integer> barChart;

    public TextField textFieldCurrentPage;
    public TextField textFieldPages;
    public TextField textFieldSearchFriend;
    public TableColumn<FriendShipDto,String> columnFirstName;
    public TableColumn<FriendShipDto,String>  columnLastName;


    @FXML
    private TableView<FriendShipDto> tableFriends;

    @FXML
    private TableColumn<FriendShipDto, String> columnEmail;



    @FXML
    private Label labelFriend;

    public PieChart pieChart;

    @FXML
    private DatePicker date_picker_start;

    @FXML
    private DatePicker date_picker_end;


    private List<FriendShipDto> listFriends;


    ObservableList<XYChart.Series<String,Integer>> seriesModelBar = FXCollections.observableArrayList();

    ObservableList<PieChart.Data> modelPie = FXCollections.observableArrayList();

    ObservableList<FriendShipDto> modelTable = FXCollections.observableArrayList();


    private User user;

    private FriendShipService friendShipService;
    private MessageService messageService;

    private int maxPage;


    public void setService(FriendShipService friendShipService , MessageService messageService,User user)
    {
        this.friendShipService = friendShipService;
        this.messageService = messageService;
        this.user = user;
        listFriends = friendShipService.getFriends(user);

        LocalDate start = LocalDate.of(2020,1,1);
        LocalDate end = LocalDate.now();

        date_picker_start.setValue(start);
        date_picker_end.setValue(end);

        initModelBar(start,end);

        initModel(1,"");

        maxPage = friendShipService.getNrOfPagesFriends(user,"");
        textFieldCurrentPage.setText("1");
        textFieldPages.setText("/"+maxPage);

        initModelPie(null,start,end);

    }

    private void initModel(int page, String full_name) {
        modelTable.setAll(friendShipService.getFriendsPageable(user,page,full_name));

    }



    private void initModelPie(FriendShipDto friend ,LocalDate date_start, LocalDate date_end){
       if(listFriends == null || listFriends.isEmpty())
           return;

        if(friend == null)
        {
            FriendShipDto aux = null;
            Map<String,Integer> result = null;

            List<PieChart.Data> lst = new ArrayList<>();

            for(FriendShipDto f : listFriends){
                 result = messageService.getMessagesBetween2UsersFromAPeriodChart(user,f.getId(),date_start,date_end);
                if(!result.entrySet().isEmpty())
                {
                    aux = f;
                    break;
                }
            }

            if(!result.entrySet().isEmpty())
            {
                labelFriend.setText(aux.toString());

                result.entrySet().forEach(f->lst.add(new PieChart.Data(f.getKey(),f.getValue())));


                for(PieChart.Data p : lst){
                    p.setName(p.getName() + "->" + Math.round(p.getPieValue()));
                }

                pieChart.setTitle("Messages with " + aux.getEmail());
                modelPie.setAll(lst);

            }else
            {

                modelPie.setAll(new PieChart.Data("No messages!",1));
                pieChart.setTitle("You don't have messages");
            }



        }
        else {
            List<PieChart.Data> lst1 = new ArrayList<>();
            Map<String, Integer> messages = messageService.getMessagesBetween2UsersFromAPeriodChart(user, friend.getId(), date_start, date_end);

            if(!messages.entrySet().isEmpty()) {
                messages.entrySet().forEach(f -> lst1.add(new PieChart.Data(f.getKey(), f.getValue())));

                for(PieChart.Data p : lst1){
                    p.setName(p.getName() + "->" + Math.round(p.getPieValue()));
                }

                pieChart.setTitle("Messages with " + friend.getEmail());
                modelPie.setAll(lst1);


            }
            else
            {
                modelPie.setAll(new PieChart.Data("No messages!",1));
                pieChart.setTitle("You don't have messages with "+ friend.getEmail());

            }

        }

    }

    private void initModelBar(LocalDate date_start, LocalDate date_end){


        TreeMap<String,Integer> receive_request = getRequestReceive(date_start,date_end);
        TreeMap<String,Integer> sent_request = getRequestSent(date_start,date_end);


        XYChart.Series<String, Integer> series_request_receive = new XYChart.Series<>();
        XYChart.Series<String, Integer> series_request_sent = new XYChart.Series<>();

        series_request_receive.setName("request receive");

        receive_request.entrySet().forEach(f -> series_request_receive.getData().add(new XYChart.Data(f.getKey(), f.getValue())));

        series_request_sent.setName("request sent");

        sent_request.entrySet().forEach(f -> series_request_sent.getData().add(new XYChart.Data(f.getKey(), f.getValue())));



        seriesModelBar.setAll(series_request_sent, series_request_receive);


    }

    private void handleDatePiker() {

        if(date_picker_start.getValue() != null  && date_picker_end.getValue() != null )

            if(date_picker_start.getValue().isBefore(date_picker_end.getValue())) {
                initModelBar(date_picker_start.getValue(), date_picker_end.getValue());

                String text = labelFriend.getText();

                if(!text.isEmpty() && text.contains(";"))
                {
                    String[] arguments = labelFriend.getText().split(";");

                    try{

                        User user1 = new User(Long.parseLong(arguments[0]));
                        user1.setEmail(arguments[1]);
                        FriendShipDto friendShipDto = new FriendShipDto(user1,LocalDate.parse(arguments[2]));

                        initModelPie(friendShipDto,date_picker_start.getValue(),date_picker_end.getValue());

                    }catch ( NumberFormatException exp)
                    {
                        MessageAlert.showErrorMessage(null,"Error at pie chart messages!");
                    }

                }

            }

            else
                MessageAlert.showErrorMessage(null,"Start date must be before end date!");


    }

    public TreeMap<String,Integer> getRequestSent(LocalDate date_start, LocalDate date_end){
        return (TreeMap<String,Integer>)friendShipService.getFriendRequestSentFromAPeriodForChart(user,date_start,date_end);

    }

    public TreeMap<String,Integer> getRequestReceive(LocalDate date_start, LocalDate date_end){
        return (TreeMap<String,Integer>)friendShipService.getFriendRequestReceiveFromAPeriodForChart(user,date_start,date_end);

    }

    @FXML
    public void initialize() {

        columnFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        columnLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        columnEmail.setCellValueFactory(new PropertyValueFactory<>("email"));

        tableFriends.setItems(modelTable);

        barChart.setData(seriesModelBar);

        pieChart.setData(modelPie);

        date_picker_start.valueProperty().addListener(f->handleDatePiker());
        date_picker_end.valueProperty().addListener(f->handleDatePiker());



    }


    @FXML
    void handleOnClickTable() {
        FriendShipDto friendShipDto = tableFriends.getSelectionModel().getSelectedItem();

        if(friendShipDto != null) {
            labelFriend.setText(friendShipDto.toString());
            if(date_picker_start.getValue() != null && date_picker_end.getValue() != null)
                if(date_picker_start.getValue().isBefore(date_picker_end.getValue()))
                     initModelPie(friendShipDto,date_picker_start.getValue(),date_picker_end.getValue());
                else
                    MessageAlert.showErrorMessage(null,"Start date must be before end date!");

        }
    }

    /**
     *
     * @param actionEvent
     * generate a pdf file which contains 2 tables : 1) contains new friends from a period of time
     *                                               2) all messages receive by user from a period of time
     */
    public void handleClickOnReport1(ActionEvent actionEvent) {

        if(date_picker_start.getValue() != null && date_picker_end.getValue() != null)
            if(date_picker_start.getValue().isBefore(date_picker_end.getValue()))
            {
                LocalDate start = date_picker_start.getValue();
                LocalDate end = date_picker_end.getValue();


                String path = "data\\Activities.pdf";
                Document document = new Document();
                try{

                    PdfWriter writer = PdfWriter.getInstance(document,new FileOutputStream(path));
                    document.open();
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");


                    document.addTitle("Activities");


                    Text text = new Text("This report is created by user: "+ user.getEmail() + " at date: " + LocalDateTime.now().format(formatter) +".\nThis report " +
                            "contains 2 tables, first table represent all new friendships created between " + start + " and " + end +" " +
                            "and second table contains all messages receive by user between " + start + " and " + end + ".\n\n\n"

                            );


                    document.add(new Paragraph(text.getText()));


                    document.add(new Paragraph("Table 1: New friendships created between "+ start + " and " + end +":"));
                    document.add(new Paragraph("\n"));



                    PdfPTable table_friends = new PdfPTable(5);
                    addTableHeaderFriends(table_friends);
                    addCostumeRowsFriends(table_friends,start,end);

                    document.add(table_friends);

                    document.add(new Paragraph("\n\n\n"));

                    document.add(new Paragraph("Table 2: All messages receive between "+ start + " and " + end +":\n"));
                    document.add(new Paragraph("\n"));


                    PdfPTable table_messages = new PdfPTable(3);

                    addTableHeaderMessages(table_messages);
                    addCostumeRowsMessages(table_messages,start,end);

                    document.add(table_messages);


                    document.close();

                    MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,"Activities", "Report Activities is successfully saved at " + path );
                    openPdfFile(path);

                } catch (FileNotFoundException | DocumentException e) {
                    e.printStackTrace();
                }


            }
        else
            MessageAlert.showErrorMessage(null,"Start date must be before end date!");



    }

    private void addCostumeRowsMessages(PdfPTable table_messages, LocalDate start, LocalDate end){

        LocalDateTime localDateTime_start = start.atTime(0,0);
        LocalDateTime localDateTime_end = end.atTime(0,0);


        List<MessageDto> list = messageService.getReceiveMessageFromAPeriodDTO(user,localDateTime_start,localDateTime_end);

       // DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        list.sort(Comparator.comparing(MessageDto::getLocalDateTime));

        list.forEach(msg->{
            table_messages.addCell(msg.getEmailFrom());
            table_messages.addCell(msg.getMessage());
            table_messages.addCell(msg.getLocalDateTime());
        });


    }
    private void addTableHeaderMessages(PdfPTable table_messages) {
        Stream.of("EmailFrom","Message","Date")
                .forEach(columnTitle-> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table_messages.addCell(header);
                });
    }

    private void addCostumeRowsFriends(PdfPTable table, LocalDate start , LocalDate end){
        List<FriendShipDto> list = friendShipService.getFriendsFromAPeriod(user,start,end);
        list.sort(Comparator.comparing(FriendShipDto::getLocalDate));

        list
                .forEach(friendShipDto -> {
                    table.addCell(String.valueOf(friendShipDto.getId()));
                    table.addCell(friendShipDto.getEmail());
                    table.addCell(friendShipDto.getFirstName());
                    table.addCell(friendShipDto.getLastName());
                    table.addCell(String.valueOf(friendShipDto.getLocalDate()));
                });

    }

    private void addTableHeaderFriends(PdfPTable table) {
        Stream.of("IdFriend","Email","First Name","Last Name","Friendship Date")
                .forEach(columnTitle-> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }


    private void openPdfFile(String path)
    {
        try
        {
        //constructor of file class having file as argument
            File file = new File(path);
            if(!Desktop.isDesktopSupported())//check if Desktop is supported by Platform or not
            {
                System.out.println("not supported");
                return;
            }
            Desktop desktop = Desktop.getDesktop();
            if(file.exists())         //checks file exists or not
                desktop.open(file);              //opens the specified file
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }


    /**
     *
     * @param actionEvent
     * generate a pdf file which contains 1 table that represent all messages receive by user from a firend in a
     * period of time
     */

    public void handleClickOnReport2(ActionEvent actionEvent) {
        if(date_picker_start.getValue() != null && date_picker_end.getValue() != null)
            if(date_picker_start.getValue().isBefore(date_picker_end.getValue()))
            {

                LocalDate start = date_picker_start.getValue();
                LocalDate end = date_picker_end.getValue();

                String str_friend = labelFriend.getText();

                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

                String[] arguments = str_friend.split(";");

                Long id_friend = Long.parseLong(arguments[0]);
                String email_friend = arguments[1];


                String path = "data\\MessagesReceive.pdf";

                Document document = new Document();
                try{
                    PdfWriter writer = PdfWriter.getInstance(document,new FileOutputStream(path));
                    document.open();

                    document.addTitle("MessagesReceive");


                    Text text = new Text("This report is created by user: "+ user.getEmail() + " at date: " + LocalDateTime.now().format(formatter) +".\nThis report " +
                            "contains one table, which contains all messages receive by user from friend: " + email_friend + ", between " +
                            " " + start + " and " + end +".\n\n\n"

                    );


                    document.add(new Paragraph(text.getText()));


                    document.add(new Paragraph("Table 1: all messages receive by user from: " + email_friend + ", between " + " " + start + " and " + end +":"));
                    document.add(new Paragraph("\n"));


                    PdfPTable table_messages_from = new PdfPTable(2);
                    addTableHeaderMessagesFrom(table_messages_from);
                    addCostumeRowsMessagesFrom(table_messages_from,start,end,email_friend);

                    document.add(table_messages_from);

                    document.close();

                    MessageAlert.showMessage(null, Alert.AlertType.INFORMATION,"MessagesReceive", "Report MessagesReceive is successfully saved at " + path );

                    openPdfFile(path);



                } catch (FileNotFoundException | DocumentException e) {
                    e.printStackTrace();
                }

            }
            else
                MessageAlert.showErrorMessage(null,"Start date must be before end date!");


    }

    private  void addTableHeaderMessagesFrom(PdfPTable table_messages_from){
        Stream.of("Message","Date")
                .forEach(columnTitle-> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table_messages_from.addCell(header);
                });

    }

    private void addCostumeRowsMessagesFrom(PdfPTable table_messages_from, LocalDate start, LocalDate end, String email_friend) {
        LocalDateTime localDateTime_start = start.atTime(0,0);
        LocalDateTime localDateTime_end = end.atTime(0,0);
      //  DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        messageService.getMessagesReceiveFromFriendFromPeriod(user,email_friend,localDateTime_start,localDateTime_end)
                .forEach(msg->{
                    table_messages_from.addCell(msg.getMessage());
                    table_messages_from.addCell(msg.getLocalDateTime());

                });

    }

    public void handleOnPrevious(ActionEvent actionEvent) {
        int pageNr = 0;
        try {
            pageNr = Integer.parseInt(textFieldCurrentPage.getText());
        }
        catch (NumberFormatException exp)
        {
            MessageAlert.showErrorMessage(null,"Current page is invalid!");
            return;
        }
        if(pageNr == 1 ) {
            MessageAlert.showErrorMessage(null, "This is first page!");
            return;

        }
        if (pageNr < 1 || pageNr > maxPage)
            MessageAlert.showErrorMessage(null, "Invalid page number!");
        else
        {
            String full_name = textFieldSearchFriend.getText();

            initModel(pageNr - 1,full_name);

            textFieldCurrentPage.setText(String.valueOf(pageNr - 1));
        }
    }

    public void handleOnEnter(KeyEvent keyEvent) {
        if(keyEvent.getCode().equals(KeyCode.ENTER)) {
            try {
                int pageNr = Integer.parseInt(textFieldCurrentPage.getText());
                if (pageNr < 1 || pageNr > maxPage)
                    MessageAlert.showErrorMessage(null, "Invalid page number!");
                else {
                    initModel(pageNr,textFieldSearchFriend.getText());
                }

            } catch (NumberFormatException exp) {
                MessageAlert.showErrorMessage(null, "Invalid page format!");
            }
        }
    }

    public void handleOnEnterSearch(KeyEvent keyEvent) {
        if(keyEvent.getCode().equals(KeyCode.ENTER)){
            String search_full_name = textFieldSearchFriend.getText();

            initModel(1,search_full_name);

            textFieldCurrentPage.setText("1");
            maxPage = friendShipService.getNrOfPagesFriends(user,search_full_name);
            textFieldPages.setText("/"+maxPage);

        }
    }

    public void handleOnNext(ActionEvent actionEvent) {
        int pageNr = 0;
        try {
            pageNr = Integer.parseInt(textFieldCurrentPage.getText());
        }
        catch (NumberFormatException exp)
        {
            MessageAlert.showErrorMessage(null,"Current page is invalid!");
            return;
        }
        if(pageNr == maxPage ) {
            MessageAlert.showErrorMessage(null, "This is last page!");
            return;
        }
        if (pageNr < 1 || pageNr > maxPage)
            MessageAlert.showErrorMessage(null, "Invalid page number!");
        else
        {
            String full_name = textFieldSearchFriend.getText();

            initModel(pageNr +1 ,full_name);

            textFieldCurrentPage.setText(String.valueOf(pageNr + 1));
        }
    }
}
