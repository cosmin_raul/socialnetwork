package socialnetwork;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import socialnetwork.config.ApplicationContext;

import socialnetwork.controller.MainInterfaceController;
import socialnetwork.controller.RootSignPartController;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.*;
import socialnetwork.repository.database.*;
import socialnetwork.service.*;

public class MainFx  extends Application {
    double xOffset,yOffset;
    Stage dialogStage = new Stage();
    @Override
    public void start(Stage primaryStage) throws Exception{

        final String file_password = ApplicationContext.getPROPERTIES().getProperty("datafile.password");

        final String url = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url");
        final String username= ApplicationContext.getPROPERTIES().getProperty("databse.socialnetwork.username");
        final String password= ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.pasword");

        EventsDBRepo eventsDBRepo = new EventsDBRepo(url,username,password,new EventValidator());

        UserDBRepo userDBRepo = new UserDBRepo(url,username, password,  new UserValidator());

        FriendshipDBRepo friendshipDBRepo = new FriendshipDBRepo(url,username,password,new FriendshipValidator());

        MessageDBRepo messageDBRepo = new MessageDBRepo(url,username,password,new MessageValidator());

        FriendRequestsDBRepo requestsDBRepo = new FriendRequestsDBRepo(url,username,password,new FriendRequestValidator());

        ImageProfileDBRepo imageProfileDBRepo = new ImageProfileDBRepo(url,username,password);

        EventsService eventsService = new EventsService(eventsDBRepo,userDBRepo);

        AdminService adminService = new AdminService(userDBRepo,friendshipDBRepo,messageDBRepo,requestsDBRepo);

        UserService userService = new UserService(userDBRepo,friendshipDBRepo,messageDBRepo,requestsDBRepo,imageProfileDBRepo);

        FriendShipService friendShipService = new FriendShipService(userDBRepo,friendshipDBRepo,messageDBRepo,requestsDBRepo);

        MessageService messageService = new MessageService(userDBRepo,friendshipDBRepo,messageDBRepo,requestsDBRepo);

       /* Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/mainInterface.fxml"));

        AnchorPane root = loader.load();

        Scene scene = new Scene(root);

        stage.setScene(scene);

        MainInterfaceController mainInterfaceController = loader.getController();
        mainInterfaceController.setService(userService,friendShipService,messageService,eventsService,stage,null,new User(24l));


        stage.show();


        */

     /*   Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/pageabletest.fxml"));

        AnchorPane root = loader.load();

        Scene scene = new Scene(root);

        stage.setScene(scene);

        PageableTestController pageableTestController = loader.getController();

        pageableTestController.setService(userService);


        stage.show();*/


        FXMLLoader loader=new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/rootSignPart.fxml"));

        AnchorPane root=loader.load();

        root.setOnMousePressed(event -> {
            xOffset = event.getSceneX();
            yOffset = event.getSceneY();

        });

        root.setOnMouseDragged(event -> {
            dialogStage.setX(event.getScreenX() - xOffset);
            dialogStage.setY(event.getScreenY() - yOffset);

        });

        Scene scene = new Scene(root, 850, 600);
        scene.setFill(Paint.valueOf("transparent"));
        //scene.setFill(Color.TRANSPARENT);

        dialogStage.setScene(scene);

        dialogStage.initStyle(StageStyle.TRANSPARENT);

        RootSignPartController rootSignPartController = loader.getController();

        rootSignPartController.setService(userService,adminService,friendShipService,messageService,eventsService,dialogStage);

        dialogStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }


}
