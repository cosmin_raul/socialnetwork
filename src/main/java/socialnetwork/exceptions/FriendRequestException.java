package socialnetwork.exceptions;

public class FriendRequestException extends RuntimeException{

    public FriendRequestException() {
        super();
    }

    public FriendRequestException(String message) {
        super(message);
    }

    public FriendRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public FriendRequestException(Throwable cause) {
        super(cause);
    }
}
