package socialnetwork.exceptions;

public class IdentificationException extends  RuntimeException {

    public IdentificationException() {
        super();
    }

    public IdentificationException(String message) {
        super(message);
    }

    public IdentificationException(String message, Throwable cause) {
        super(message, cause);
    }

    public IdentificationException(Throwable cause) {
        super(cause);
    }
}
