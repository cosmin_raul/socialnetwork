package socialnetwork.exceptions;

public class EventsException extends RuntimeException{
    public EventsException() {
        super();
    }

    public EventsException(String message) {
        super(message);
    }

    public EventsException(String message, Throwable cause) {
        super(message, cause);
    }

    public EventsException(Throwable cause) {
        super(cause);
    }
}
