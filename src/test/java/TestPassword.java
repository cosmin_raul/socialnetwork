import socialnetwork.domain.Password;
import socialnetwork.domain.User;
import socialnetwork.repository.database.UserDBRepo;

import java.util.Arrays;

public class TestPassword {
    public static void main(String[] args) {
        test_password();
        test_covertFunctions();
    }

    private static void test_covertFunctions() {
        UserDBRepo userDBRepo = new UserDBRepo();

        Password password = new Password();
        password.setPassword_String("salut");

        password.encodePassword();

        String hash_string = userDBRepo.getStringFromBytes(password.getHash());

        byte[] hash_test = userDBRepo.getBytesFromString(hash_string);

        assert Arrays.equals(hash_test,password.getHash());


        String salt_string = userDBRepo.getStringFromBytes(password.getSalt());
        System.out.println(salt_string);

        byte[] salt_test = userDBRepo.getBytesFromString(salt_string);

        assert Arrays.equals(salt_test,password.getSalt());


        Password password1 = new Password();
        password1.setPassword_String("Salut12321JH$%^&");
        password1.encodePassword();

        String hash_string1 = userDBRepo.getStringFromBytes(password1.getHash());

        byte[] hash_test1 = userDBRepo.getBytesFromString(hash_string1);

        assert Arrays.equals(hash_test1,password1.getHash());


        String salt_string1 = userDBRepo.getStringFromBytes(password1.getSalt());
        System.out.println(salt_string1);

        byte[] salt_test1 = userDBRepo.getBytesFromString(salt_string1);

        assert Arrays.equals(salt_test1,password1.getSalt());




    }

    private static void test_password() {
        Password password = new Password();
        password.setPassword_String("salut");

        password.encodePassword();

        Password password2 = new Password();
        password2.setPassword_String("salut");

        Password password3 = new Password();
        password3.setPassword_String("salut1");

        Password password4 = new Password();
        password4.setPassword_String("Salut");

        Password password5 = new Password();
        password5.setPassword_String("SALUT");



        System.out.println(password2.checkEquality(password.getHash(), password.getSalt()));

        assert password2.checkEquality(password.getHash(), password.getSalt());

        assert !password3.checkEquality(password.getHash(), password.getSalt());
        assert !password4.checkEquality(password.getHash(), password.getSalt());

        assert !password5.checkEquality(password.getHash(), password.getSalt());



        Password password6 = new Password();
        password6.setPassword_String("CoSmin_12*");

        password6.encodePassword();

        Password password7 = new Password();
        password7.setPassword_String("CoSmin_12*");

        Password password8 = new Password();
        password8.setPassword_String("Cosmin_12*");

        Password password9 = new Password();
        password9.setPassword_String("Cosmin_1");

        assert password7.checkEquality(password6.getHash(),password6.getSalt());
        assert !password8.checkEquality(password6.getHash(),password6.getSalt());

        assert !password9.checkEquality(password6.getHash(),password6.getSalt());




        Password password10 = new Password();
        password10.setPassword_String("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

        password10.encodePassword();

        Password password11 = new Password();
        password11.setPassword_String("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

        Password password12 = new Password();
        password12.setPassword_String("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

        assert password11.checkEquality(password10.getHash(),password10.getSalt());
        assert !password12.checkEquality(password10.getHash(),password10.getSalt());


        Password password13 = new Password();
        password13.setPassword_String("!@!#@$#$^&^*&*())*)(*><.,=-");

        password13.encodePassword();

        Password password14 = new Password();
        password14.setPassword_String("!@!#@$#$^&^*&*())*)(*><.,=-");


        Password password15 = new Password();
        password15.setPassword_String("!@!#@$#$^&^*&*())*)(><.,=-");

        assert password14.checkEquality(password13.getHash(),password13.getSalt());
        assert !password15.checkEquality(password13.getHash(),password13.getSalt());



        Password password16 = new Password();
        password16.setPassword_String("123  ASCV  %^");

        password16.encodePassword();

        Password password17 = new Password();
        password17.setPassword_String("123  ASCV  %^");

        Password password18 = new Password();
        password18.setPassword_String("123  ASCV %^");

        assert password17.checkEquality(password16.getHash(),password16.getSalt());
        assert !password18.checkEquality(password16.getHash(),password16.getSalt());



    }


}
